import axios from 'axios'
import {Fragment, useState } from 'react';
import Link from 'next/link'
import Head from 'next/head'
import {
  ChevronUpIcon,
  StatusOnlineIcon, StatusOfflineIcon,
  DocumentAddIcon, DocumentIcon, DocumentTextIcon, DocumentRemoveIcon, DocumentDuplicateIcon,
  CogIcon,
  EyeIcon,
  LinkIcon,
  HomeIcon,
  CubeIcon,
  TagIcon,
  UsersIcon,
  BeakerIcon,
  StarIcon,
  InboxIcon,
  FolderIcon,
  PencilIcon,
  ClockIcon,
  MinusIcon,
  PlusIcon,
  KeyIcon,
  MapIcon,
  BellIcon,
  SaveIcon,
  ChatIcon,
  ReplyIcon,
  FolderRemoveIcon,
  ShareIcon,
  TableIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  TrashIcon,
  SearchIcon,
  CodeIcon,
  ArchiveIcon,
  LibraryIcon,
  InboxInIcon,
  FolderOpenIcon,
  FolderDownloadIcon,
  InformationCircleIcon,
  DuplicateIcon,
  DotsCircleHorizontalIcon,
  BookOpenIcon,
  BookmarkIcon,
  CashIcon,
  CheckIcon,
  CalendarIcon,
  ChartBarIcon,
  CollectionIcon,
  CalculatorIcon,
  ColorSwatchIcon, CameraIcon, FilterIcon, FolderAddIcon, UserGroupIcon, HeartIcon, HashtagIcon,
  LockClosedIcon, LocationMarkerIcon, MenuIcon, MicrophoneIcon, PaperClipIcon, PhotographIcon, RefreshIcon, UserAddIcon, UserRemoveIcon, UploadIcon, UserIcon,
  SortAscendingIcon, SortDescendingIcon, SelectorIcon, TrendingUpIcon, TrendingDownIcon, TerminalIcon, TemplateIcon, TicketIcon,
  ChevronDownIcon, DotsVerticalIcon, AtSymbolIcon,  AnnotationIcon, AdjustmentsIcon, CakeIcon, CreditCardIcon,
  PlayIcon, PauseIcon, RewindIcon, SaveAsIcon, StopIcon,
  LockOpenIcon, ExternalLinkIcon, ViewGridIcon, ViewListIcon, ViewGridAddIcon, ViewBoardsIcon
} from '@heroicons/react/solid'
import { Popover, Transition, Disclosure } from '@headlessui/react'
import DashLayout from '../../components/layout/dashboard'
import Slide from '../../components/ui/slide/slide'
import Switch from '../../components/ui/switch'
import Input from '../../components/ui/input/input'
import Dropdown from '../../components/dropdown'
import Listbox from '../../components/ui/listbox'
import Tooltip from '../../components/ui/tooltip/tooltip'
import Divider from '../../components/ui/divider'
import Dialog from '../../components/ui/dialog/dialog'
import UserDropdown from '../../components/user/statusDropdown'
import Radio from '../../components/ui/radio/radio'
import Select from '../../components/ui/dropdown/select'
import PriceInput from '../../components/ui/input/price'
import DropdownBtn from '../../components/ui/dropdown/button'
// import Popover from '../../components/ui/popover/popover'
import DDisclosure from '../../components/ui/disclosure/disclosure'
import { useSession } from 'next-auth/client'

const solutions = [
  {
    name: "Insights",
    description: "Measure actions your users take",
    href: "##",
    icon: IconOne,
  },
  {
    name: "Automations",
    description: "Create your own targeted content",
    href: "##",
    icon: IconTwo,
  },
  {
    name: "Reports",
    description: "Keep track of your growth",
    href: "##",
    icon: IconThree,
  },
];

export function EditPopover() {
  return (
    <div className="fixed w-full max-w-sm px-4 top-16">
      <Popover className="relative">
        {({ open }) => (
          <>
            <Popover.Button
              className={`
                ${open ? "" : "text-opacity-90"}
                text-white group bg-orange-700 px-3 py-2 rounded-md inline-flex items-center text-base font-medium hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75`}
            >
              <span>Solutions</span>
              <ChevronDownIcon
                className={`${open ? "" : "text-opacity-70"}
                  ml-2 h-5 w-5 text-orange-300 group-hover:text-opacity-80 transition ease-in-out duration-150`}
                aria-hidden="true"
              />
            </Popover.Button>
            <Transition
              show={open}
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel
                static
                className="absolute z-10 w-screen max-w-sm px-4 mt-3 transform -translate-x-1/2 left-1/2 sm:px-0 lg:max-w-3xl"
              >
                <div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                  <div className="relative bg-white grid gap-8 p-7 lg:grid-cols-2">
                    {solutions.map((item) => (
                      <a
                        key={item.name}
                        href={item.href}
                        className="flex items-center p-2 -m-3 rounded-lg transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50"
                      >
                        <div className="flex items-center justify-center flex-shrink-0 w-10 h-10 text-white sm:h-12 sm:w-12">
                          <item.icon aria-hidden="true" />
                        </div>
                        <div className="ml-4">
                          <p className="text-sm font-medium text-gray-900">
                            {item.name}
                          </p>
                          <p className="text-sm text-gray-500">
                            {item.description}
                          </p>
                        </div>
                      </a>
                    ))}
                  </div>
                  <div className="p-4 bg-gray-50">
                    <a
                      href="##"
                      className="px-2 py-2 flow-root transition duration-150 ease-in-out rounded-md hover:bg-gray-100 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50"
                    >
                      <span className="flex items-center">
                        <span className="text-sm font-medium text-gray-900">
                          Documentation
                        </span>
                      </span>
                      <span className="block text-sm text-gray-500">
                        Start integrating products and tools
                      </span>
                    </a>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </div>
  );
}

function IconOne() {
  return (
    <svg
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="48" height="48" rx="8" fill="#FFEDD5" />
      <path
        d="M24 11L35.2583 17.5V30.5L24 37L12.7417 30.5V17.5L24 11Z"
        stroke="#FB923C"
        strokeWidth="2"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.7417 19.8094V28.1906L24 32.3812L31.2584 28.1906V19.8094L24 15.6188L16.7417 19.8094Z"
        stroke="#FDBA74"
        strokeWidth="2"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M20.7417 22.1196V25.882L24 27.7632L27.2584 25.882V22.1196L24 20.2384L20.7417 22.1196Z"
        stroke="#FDBA74"
        strokeWidth="2"
      />
    </svg>
  );
}

function IconTwo() {
  return (
    <svg
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="48" height="48" rx="8" fill="#FFEDD5" />
      <path
        d="M28.0413 20L23.9998 13L19.9585 20M32.0828 27.0001L36.1242 34H28.0415M19.9585 34H11.8755L15.9171 27"
        stroke="#FB923C"
        strokeWidth="2"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.804 30H29.1963L24.0001 21L18.804 30Z"
        stroke="#FDBA74"
        strokeWidth="2"
      />
    </svg>
  );
}

function IconThree() {
  return (
    <svg
      width="48"
      height="48"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="48" height="48" rx="8" fill="#FFEDD5" />
      <rect x="13" y="32" width="2" height="4" fill="#FDBA74" />
      <rect x="17" y="28" width="2" height="8" fill="#FDBA74" />
      <rect x="21" y="24" width="2" height="12" fill="#FDBA74" />
      <rect x="25" y="20" width="2" height="16" fill="#FDBA74" />
      <rect x="29" y="16" width="2" height="20" fill="#FB923C" />
      <rect x="33" y="12" width="2" height="24" fill="#FB923C" />
    </svg>
  );
}


export function EntityBadge(props) {
  const name = props.name? props.name : "Quantity";
  const tooltip = props.tooltip? props.name : "The amount in your library";
  return (
    <div className={` flex ml-3  float-left px-2 py-1 ${ props.bg } text-md ${ props.color } ${ props.hoverBg } ${ props.hoverColor } ${ props.ridge3d? props.ridge3d : "" } items-center rounded-xl text-sm`}>
      <props.icon class="w-4 h-4"/>
      <span class="ml-1">{ props.value }</span>
  </div>
  )
}
export function EntityPanel(props) {
  return (
      <Disclosure>
        {({ open }) => (
            <>
          <Disclosure.Button className="justify-around w-full p-2 px-8 py-2 mx-8 mx-auto my-2 font-medium bg-gray-100 border-b-2 border-gray-200 rounded-lg hover:bg-gray-200 transition duration-200">
            <div
              className={`
                 flex flex-shrink float-left items-center items-start
                ${ open? "" : "" }
              `}>
              <div class="rounded-full bg-white text-gray-400 p-1 border-b-4 border-b-gray-400">
                <props.icon class="   w-6 h-6 "/>
              </div>
              <span className={` text-gray-800 ml-3  float-left text-gray-700 text-md items-center ${ open? "text-black" : "" }`}>{ props.entity }</span>
              {  props.quantity &&
                <span className={`  ml-3  float-left text-gray-400 text-md items-center ${ open? "text-gray-400" : "" }`}>{ props.quantity  } in library</span>
              }
            </div>
            <div class="items-center place-items-end float-right justify-end flex-shrink flex mt-1">
              {  props.badge1 &&
              <EntityBadge
                value = { props.badge1 }
                icon = { CheckIcon }
                name = "" tooltip = ""
                bg = "bg-gray-200" hoverBg = "bg-gray-300"
                color = "text-gray-300" hoverColor = "text-gray-400"
              />
              }
              {  props.badge2 &&
              <EntityBadge
                value = { props.badge2 }
                icon = { BookmarkIcon }
                name = "" tooltip = ""
                bg = "bg-gray-400" hoverBg = "bg-gray-300"
                color = "text-gray-200" hoverColor = "text-gray-100"
              />
              }
              {  props.due &&
                <span class="items-center text-sm text-gray-400 ">Due by { props.due }</span>
              }
            <ChevronDownIcon
              className={` ${
                open ? "transform rotate-180" : ""
              } w-5 h-5 text-gray-500 ml-3`}/>
            </div>
          </Disclosure.Button>
          <Transition
            show={open}
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0"
          >
              <Disclosure.Panel static className="px-4 pt-4 pb-2 text-sm text-gray-500 bg-white rounded-lg">
                { props.children }
                <br/><br/><hr/><br/><br/>
              </Disclosure.Panel>
          </Transition>
            </>
          )}
        </Disclosure>
  )
}
export function TextBtn(props) {

  return (
    <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200 text-gray-700 focus:shadow-inner">
      <props.icon className="w-4 h-4 mr-1 item-center"/>
      <Link href="#">{ props.children }</Link>
    </button>

  )
}
export function EditorBtnBox(props) {
  return (
    <div class="flex flex-row justify-around space-x-2 float-left px-3 py-2    ">
      {props.buttons.map((button) => (

        <EditorBtn bg="white" icon = { button.icon } onClick = { button.onClick }> { button.label }</EditorBtn>
      ))}
    </div>
  )
}
export function EditToolbar(props) {
  const bclassPrimary = "flex justify-around px-3 py-2 mx-auto text-sm bg-white  shadow text-gray-800 hover:shadow-inner hover:bg-gray-200 transition duration-75";
  const bclass = "flex justify-around px-3 py-2 mx-auto text-sm bg-green-600  shadow text-white hover:shadow-inner hover:bg-gray-200 transition duration-75";
  const toButton = (button, idx) => {
    const buttonClass = button.primary ? bclass : bclassPrimary;
    if (idx == 0) {
      return (
      <button class = { buttonClass + "rounded-l-lg rounded-r-none" } onClick = { button.onClick }>
        <button.icon  className="items-center float-left w-4 h-4 mr-1 clear-both"/>
        { button.label }
      </button>
      )
    } else if (idx == props.buttons.length) {
      return (
      <button class = { buttonClass + "rounded-r-lg rounded-l-none" } onClick = { button.onClick }>
        <button.icon  className="items-center float-left w-4 h-4 mr-1 clear-both"/>
        { button.label }
      </button>
      )

    } else {
      return (
      <button class = { buttonClass + "rounded-none" } onClick = { button.onClick }>
        <button.icon  className="items-center float-left w-4 h-4 mr-1 clear-both"/>
        { button.label }
      </button>
      )

    }
  }
  return (
    <div class="flex flex-row   float-left px-3 py-2 -mx-2 rounded-lg  ">
      {props.buttons.map((button, index) => (
        toButton(button, index)
      ))}
    </div>
  )
}
export function EditorBtn(props) {
  const label = props.label;
  const buttonClass = () => {
    const color = props.bg? props.bg : "white";
    return "px-4 py-3 rounded-md shadow hover:shadow-inner bg-white text-sm hover:bg-gray-200 justify-around flex mx-auto transition duration-75 " + color;
  }
  return (
    <button
      className= { buttonClass() }
      onClick = { props.onClick }
    >
      <props.icon  className="items-center float-left w-5 h-5 mr-1 clear-both"/>
      { props.children }
    </button>
  )
}

export function Posts() {
  return (
    <>
    </>
  )
}
export function EditorHome() {
  //const [openTab, setTab] = useState(0);
  // const [ connection, setConnection ] = useState(false);
  var rec = []
  const [ tab, setTab ] = useState(0)
  const [ session, loading ] = useSession()
  const connection = false;
  var posts =  []
  const records = [
    { text: "Records", value: "#", onClick: () => {}, disabled: false  },
    { text: "Items", value: "#", onClick: () => {}, disabled: false  },
    { text: "Fields", value: "#", onClick: () => {}, disabled: false  }
  ];
  const record_btns = [
    { label: "Create", icon: DocumentAddIcon, onClick: () => {} },
    { label: "Delete", icon: TrashIcon, onClick: () => {} },
    { label: "Update", icon: DocumentTextIcon, onClick: () => {} },
    { label: "Duplicate", icon: DuplicateIcon, onClick: () => {} }
  ]
  const connection_ind = () => {
    return (
      <>
          { connection? <>
              <p class="flex text-xs p-1 my-auto ml-12 border-2 border-green-400  opacity-80 rounded-xl   ml-2">
                <StatusOnlineIcon class="text-green-500 w-5 h-5"/>
                Connected to the Devisa database.
              </p>
              <p class="flex text-xs py-1 my-auto ml-0 border-2 border-yellow-400  opacity-80 rounded-full   mr-2">
                <StatusOnlineIcon class="text-yellow-400 w-4 h-4 mr-4 items-center justify-center ml-4"/>
              </p>
              <p class="flex ml-12 text-xs py-1 my-auto ml-0 border-2 border-red-500  opacity-80 rounded-full   mr-12">
                <StatusOnlineIcon class="text-red-500 w-4 h-4 mr-4 items-center justify-center ml-4"/>
              </p>
          </>
                :
                <>
              <p class="flex ml-12 text-xs py-1 my-auto ml-10 border-2 border-green-400  opacity-80 rounded-full   mr-2">
                <StatusOnlineIcon class="text-green-400 w-4 h-4 mr-4 items-center justify-center ml-4"/>
              </p>
              <p class="flex text-xs py-1 my-auto ml-0 border-2 border-yellow-400  opacity-80 rounded-full   mr-2">
                <StatusOnlineIcon class="text-yellow-400 w-4 h-4 mr-4 items-center justify-center ml-4"/>
              </p>
                  <Link href="#"><p class="flex text-xs p-1 my-auto ml-0 border-2 border-red-500 hover:border-red-700 hover:bg-red-700 transition duration-200 hover:shadow bg-red-500 text-white  opacity-100 rounded-xl   mr-12">
                <StatusOfflineIcon class="text-white w-4 h-4 mr-4 items-center "/>
                No connection to the Devisa database.
                  </p></Link>
          <EditorBtn icon={RefreshIcon} bg="white">Try Again</EditorBtn>
                </>
            }
      </>
    )
  }
  const item_btns = [
    { label: "Create", icon: DocumentAddIcon, onClick: () => {} },
    { label: "Delete", icon: TrashIcon, onClick: () => {} },
    { label: "Update", icon: DocumentTextIcon, onClick: () => {} },
    { label: "Duplicate", icon: DuplicateIcon, onClick: () => {} }
  ]
  const toolbarFile = [
    { label: "New", icon: DocumentAddIcon, onClick: () => {}, primary: true },
    { label: "Open", icon: TrashIcon, onClick: () => {}, primary: false },
    { label: "Save", icon: DocumentTextIcon, onClick: () => {}, primary: false },
    { label: "Save As", icon: DuplicateIcon, onClick: () => {}, primary: false },
    { label: "", icon: MinusIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "", icon: PlusIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
  ]
  const likeToolbar = [
    { label: "More", icon: ThumbUpIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "Less", icon: ThumbDownIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
  ]
  const editToolbar = [
    { label: "File", icon: DocumentIcon, onClick: () => {}, primary: true },
    { label: "Folder", icon: FolderIcon, onClick: () => {}, primary: false },
    { label: "", icon: FolderAddIcon, onClick: () => {}, primary: false },
    { label: "", icon: FolderRemoveIcon, onClick: () => {}, primary: false },
    { label: "", icon: FolderOpenIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "", icon: FolderDownloadIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "", icon: KeyIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "", icon: ReplyIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
  ]
  const miscToolbar = [
    { label: "More", icon: ClockIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
    { label: "Less", icon: SearchIcon, onClick: () => {}, primary: false, sep: true, dropdown: null },
  ]
  const items = [
      { text: "Dashboard", value: "/dashboard", disabled: false },
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Recipes", value: "/user/chris/recipes", disabled: false },
      { text: "Items", value: "/user/chris/items", disabled: false },
      { text: "Feed", value: "/community/feed", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  const tabs = (
    <div class="h-8 lg:flex w-full  border-b shadow border-gray-200 dark:border-gray-800 hidden px-10">
      <div class="flex h-full text-gray-600 text-black text-sm dark:text-gray-400">
        <button
          onClick={e => {
            e.preventDefault();
            setTab(0);
          }}
          href="#" className={"text-sm text-black cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8" + ( tab === 0 ? "cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center" : "cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8")}>
        Overview</button>
        <button
          onClick={e => {
            e.preventDefault();
            setTab(1);
          }}
        href="#" className={"text-sm text-black cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8" + ( tab === 1 ? "cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center" : "cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8")}>Build</button>
        <button
          onClick={e => {
            e.preventDefault();
            setTab(2);
          }}
        href="#" className={"text-sm text-black cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8" + ( tab === 2 ? "cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center" : "cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8")}>Review</button>
        <button
          onClick={e => {
            e.preventDefault();
            setTab(3);
          }}
        href="#" className={"text-sm text-black cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8" + ( tab === 3 ? "cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center" : "cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8")}>Deploy</button>
      </div>
      <div class="ml-auto flex items-center space-x-7">

    </div>
  </div>

  )
  const header = (
    <>
      <Head>
        <title>Editor Home - devisa.io</title>
      </Head>
      <div class="grid-cols-2   divide-gray-400 mb-4 ">
        <div class="flex flex-row ">
          <h1 class="font-medium pl-8 text-2xl -mt-2 flex">
          <div class="rounded-full border-4 border-gray-200 bg-green-300  ">
          <HomeIcon className = "items-center w-10 h-10 pr-0 text-white"/>
          </div>
            <span class="ml-4  items-center mt-2 text-md">Editor Home</span>
        </h1>
        </div>
        <div>
        <ul class="flex flex-row space-x-4 mr-16 text-sm font-light   float-right text-black  items-center justify-around">
          {
          // <li class="items-start -mt-10">
            // <EditorBtn bg="white" icon={InformationCircleIcon}>Info</EditorBtn>
          // </li>
          // <li class="items-start -mt-10">
            // <EditorBtn bg="white" icon={EyeIcon}>Your Automata</EditorBtn>
          // </li>
          // <li class="items-start -mt-10">
            // <EditorBtn bg="white" icon={EyeIcon}>Guides</EditorBtn>
          // </li>
          }
          <li class="items-start -mt-10 ml-8">
            { session?
                    <Dropdown
                      button={<UserDropdown image = "/lightart.jpg" user={ session.user? session.user : { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io" } }/>}
                      items={items}
                      heading={
                      <div class="px-4 py-3 hover:bg-green-100">
                        <p class="text-sm leading-5">Create a new...</p>
                      </div>
                      }
                    />
                    :
                    <Dropdown
                      button={<UserDropdown image="/lightart.jpg" user={    { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>}
                      items={items}
                      heading={
                      <div class="px-4 py-3 hover:bg-green-100">
                        <p class="text-sm leading-5">Create a new...</p>
                      </div>
                      }
                    />
            }
          </li>
        </ul>
        </div>
    </div>



      <div class="flex-grow overflow-hidden max-h-full min-h-full flex flex-col ">
        { tabs }
  </div>
    </>
  );

  if (tab == 0) {
  return (

    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
      { header }
      <div class="bg-gray-50 grid grid-cols-2 -mx-4 p-4 flex-row">



        <Disclosure as="div" class="relative">
            {({ open }) =>  (
            <>
              <div class={`bg-white rounded-md font-light m-2 shadow-sm p-4 flex-shrink ${open? "bg-white": "bg-transparent"}`}>

                <Disclosure.Button className={`grid grid-cols-2 w-full pt-2 outline-none focus:outline-none ring-transparent outline-transparent ${open? "hover:bg-gray-100 transition duration-200" : "hover:bg-gray-100 transition duration-200"}`}>
            <h1 class="text-xl font-medium pb-2 pl-6 flex items-center">
              <TerminalIcon class="w-6 h-6 mr-4" />
              Run
            </h1>
            <div>
              <div class="float-right items-center mt-0 ml-2 bg-gray-100 rounded-full p-2 mb-4">
                  { open? <ChevronUpIcon class="w-5 h-5 items-center"/> : <ChevronDownIcon class="w-5 h-5 items-center"/> }
              </div>
            </div>

          </Disclosure.Button>
                { open && <><hr/><br/></>}

          { open &&
          <Transition
            show={open}
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0"
          >
          <Disclosure.Panel static className={`${open? "bg-white" : " bg-gray-100 hidden"}`}>



            {/* <div class="flex flex-row"> */}
          {/* <EditToolbar buttons= { toolbarFile }/> */}
          {/* <EditToolbar buttons= { likeToolbar }/> */}
            {/* </div> */}
            <div class="float-left ml-4">
                <TextBtn onClick={() => {}} icon={UsersIcon}>Shared</TextBtn>
                <TextBtn onClick={() => {}} icon={CogIcon}>Editor</TextBtn>
                <TextBtn onClick={() => {}} icon={TerminalIcon}>IDE</TextBtn>
                <TextBtn onClick={() => {}} icon={SaveIcon}>Save</TextBtn>
                <TextBtn onClick={() => {}} icon={TrashIcon}>Discard</TextBtn>
                <TextBtn onClick={() => {}} icon={ArchiveIcon}>Archive</TextBtn>
              <button class="flex text-gray-100 bg-gray-500 font-medium float-left justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200  focus:shadow-inner">
                <PlusIcon className="w-4 h-4 mr-1 item-center"/>
                <Link href="#">New</Link>
              </button>
            </div>
<div>
  <br/>

  <div class="mt-0 relative rounded-md shadow-sm">
    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
      <span class="text-gray-500 sm:text-sm items-center mt-6">
        Auto
      </span>
    </div>
    <input type="text" name="price" id="price" class="rounded-t-lg focus:ring-green-500 focus:border-green-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300 rounded-b-none pl-12" placeholder="0.00"/>
    <div class="absolute inset-y-0 right-0 flex items-center">
      <label for="currency" class="sr-only">Currency</label>
      <select id="currency" name="currency" class=" rounded-t-lg focus:ring-green-500 focus:border-green-500 h-full py-0 pl-2 pr-7 border-transparent bg-transparent text-gray-500 sm:text-sm rounded-none-b items-center mt-6 rounded-t-3xl">
        <option>USD</option>
        <option>CAD</option>
        <option>EUR</option>
      </select>
    </div>
  </div>
</div>
          <textarea
            class="min-h-2/3 border-none outline-none rounded-md shadow-inner bg-gradient-to-br from-gray-50 to-gray-100 border-gray-300 border-4 focus:ring-2 focus:ring-green-200 mx-auto min-w-full p-4 border-r border-l border-gray-400 mb-0 pb-0 border-l-2 border-r-2 border-gray-400 rounded-none min-h-2/3 h-64">

          </textarea>
<div class="-mt-2">
  <div class="mt-0 pt-0 relative rounded-md shadow-sm">
    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
      <span class="text-gray-500 sm:text-sm">
        Auto
      </span>
    </div>
    <input type="text" name="other2" id="other2" class="rounded-b-lg focus:ring-green-500 focus:border-green-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300 rounded-t-none pl-12" placeholder="0.00"/>
    <div class="absolute inset-y-0 right-0 flex items-center">
      <label for="currency" class="sr-only">Currency</label>
      <select id="other" name="other" class=" rounded-b-lg focus:ring-green-500 focus:border-green-500 h-full py-0 pl-2 pr-7 border-transparent bg-transparent text-gray-500 sm:text-sm rounded-none-t">
        <option>USD</option>
        <option>CAD</option>
        <option>EUR</option>
      </select>
    </div>

    <input id="itemName" placeholder="item name" class="rounded-lg p-2"/>
    <button class="bg-gray-100 px-3 py-2 text-gray-500 text-sm"
      onClick={(e) => {
        e.preventDefault();
        alert(e.target.value);
      }}
    >add</button>

  </div>
</div>


            {/* <div class="mt-4 mx-8  rounded-lg px-6 py-4 shadow justify-start place-items-start "> */}

            {/* <label class="pt-8 text-sm text-gray-500 -mb-2 py-2 pt-8">Name</label> */}
          {/* <input */}
            {/* class=" border-none outline-none rounded-md shadow-inner bg-gradient-to-br from-gray-50 to-gray-100 border-gray-300 border-4 focus:ring-2 focus:ring-green-200 mx-auto min-w-full p-4 border-r border-l border-gray-400 mb-0 pb-0 border-l-2 border-r-2 border-gray-400 rounded-none mb-4"/> */}
            {/* <label class="pt-8 text-sm text-gray-500 -mb-2 py-2 pt-8">Name</label> */}
          {/* <input */}
            {/* class=" border-none outline-none rounded-md shadow-inner bg-gradient-to-br from-gray-50 to-gray-100 border-gray-300 border-4 focus:ring-2 focus:ring-green-200 mx-auto min-w-full p-4 border-r border-l border-gray-400 mb-0 pb-0 border-l-2 border-r-2 border-gray-400 rounded-none"/> */}
            {/* </div> */}

          <div class="grid grid-cols-2">
            <div class=" flex flex-shrink-0 place-self-start relative justify-around space-x-2  px-3 py-2       mt-2 rounded-md     hover:bg-gray-200 transition duration-300  float-left  ">
              <DropdownBtn/>
            </div>

            <div class=" flex flex-shrink-0 place-self-end relative justify-around space-x-2  px-3 py-2       mt-2 rounded-md      transition duration-300  float-right  ">
              <TextBtn icon={SaveIcon} >Save</TextBtn>
                <TextBtn icon={StopIcon}> Pause</TextBtn>
              <TextBtn icon={PlayIcon}>Run</TextBtn>
            </div>
          </div>

            </Disclosure.Panel>
            </Transition>


            }

            </div>
              </>)}
              </Disclosure>

        <Disclosure as="div" class="relative">
            {({ open }) =>  (
            <>
              <div class={`bg-white rounded-md font-light m-2 shadow-sm p-4 flex-shrink ${open? "bg-white": "bg-transparent"}`}>

                <Disclosure.Button className={`grid grid-cols-2 w-full pt-2 outline-transparent ring-transparent focus:outline-none ${open? "hover:bg-gray-100 transition duration-200" : "hover:bg-gray-100 transition duration-200"}`}>
            <h1 class="text-xl font-medium pb-2 pl-6 flex items-center">
              <CubeIcon class="w-6 h-6 mr-4" />
              Entities
            </h1>
            <div>
              <div class="float-right items-center mt-0 ml-2 bg-gray-100 rounded-full p-2 mb-4">
                  { !open? <ChevronDownIcon class="w-5 h-5 items-center"/> : <ChevronUpIcon class="w-5 h-5 items-center"/> }
              </div>
            </div>

          </Disclosure.Button>
                { open && <><hr/><br/></>}








          { open &&
          <Transition
            show={open}
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0"
          >
          <Disclosure.Panel static className={`${open? "bg-white" : " bg-gray-100 hidden"}`}>

            <h1 class="text-lg font-medium pb-2 pl-6 flex items-center  " >
              <LibraryIcon class="w-5 h-5 mr-4 text-gray-800 ml-2 "/>
              Your Library
            </h1>
          <EntityPanel
            entity = "Records"
            icon = { BookOpenIcon }
            quantity = {3}
            badge1={8}
            badge2={11}
          >
            <div class="float-left pl-2 block">
                <TextBtn onClick={() => {}} icon={UsersIcon}>Shared</TextBtn>
                <TextBtn onClick={() => {}} icon={CogIcon}>Editor</TextBtn>
                <TextBtn onClick={() => {}} icon={SaveIcon}>Save</TextBtn>
                <TextBtn onClick={() => {}} icon={TrashIcon}>Discard</TextBtn>
                <TextBtn onClick={() => {}} icon={ArchiveIcon}>Archive</TextBtn>
              <button class="flex text-gray-100 bg-indigo-500 font-medium float-left justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200  focus:shadow-inner">
                <PlusIcon className="w-4 h-4 mr-1 item-center"/>
                <Link href="#">New</Link>
              </button>
            </div>
            <br/><br/><br/>
            <p>If you're unhappy with your purchase for any reason, email us
              within 90 days and we'll refund you in full, no questions asked.</p>
            </EntityPanel>

          <EntityPanel
            entity = "Items"
            icon = { ArchiveIcon }
            quantity = {7}
            badge1={3}
            badge2={33}
          >
            <div class="float-left pl-2 block">
                <TextBtn onClick={() => {}} icon={UsersIcon}>Shared</TextBtn>
                <TextBtn onClick={() => {}} icon={CogIcon}>Editor</TextBtn>
                <TextBtn onClick={() => {}} icon={SaveIcon}>Save</TextBtn>
                <TextBtn onClick={() => {}} icon={TrashIcon}>Discard</TextBtn>
                <TextBtn onClick={() => {}} icon={ArchiveIcon}>Archive</TextBtn>
              <button class="flex text-gray-100 bg-indigo-500 font-medium float-left justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200  focus:shadow-inner">
                <PlusIcon className="w-4 h-4 mr-1 item-center"/>
                <Link href="#">New</Link>
              </button>
            </div>
            <br/><br/><br/>
            <p>If you're unhappy with your purchase for any reason, email us
              within 90 days and we'll refund you in full, no questions asked.</p>
            </EntityPanel>

          <EntityPanel
            entity = "Fields"
            icon = { TableIcon }
            quantity = {1}
            badge1={3}
            badge2={33}
          >
            <div class="float-left pl-2 block">
                <TextBtn onClick={() => {}} icon={UsersIcon}>Shared</TextBtn>
                <TextBtn onClick={() => {}} icon={CogIcon}>Editor</TextBtn>
                <TextBtn onClick={() => {}} icon={SaveIcon}>Save</TextBtn>
                <TextBtn onClick={() => {}} icon={TrashIcon}>Discard</TextBtn>
                <TextBtn onClick={() => {}} icon={ArchiveIcon}>Archive</TextBtn>
              <button class="flex text-gray-100 bg-indigo-500 font-medium float-left justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200  focus:shadow-inner">
                <PlusIcon className="w-4 h-4 mr-1 item-center"/>
                <Link href="#">New</Link>
              </button>
            </div>
            <br/><br/><br/>
            <p>If you're unhappy with your purchase for any reason, email us
              within 90 days and we'll refund you in full, no questions asked.</p>
            </EntityPanel>

          <EntityPanel
            entity = "Links"
            icon = { TagIcon }
            quantity = {5}
            badge1={3}
            badge2={33}
          >
            <div class="float-left pl-2 block">
                <TextBtn onClick={() => {}} icon={UsersIcon}>Shared</TextBtn>
                <TextBtn onClick={() => {}} icon={CogIcon}>Editor</TextBtn>
                <TextBtn onClick={() => {}} icon={SaveIcon}>Save</TextBtn>
                <TextBtn onClick={() => {}} icon={TrashIcon}>Discard</TextBtn>
                <TextBtn onClick={() => {}} icon={ArchiveIcon}>Archive</TextBtn>
              <button class="flex text-gray-100 bg-indigo-500 font-medium float-left justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200  focus:shadow-inner">
                <PlusIcon className="w-4 h-4 mr-1 item-center"/>
                <Link href="#">New</Link>
              </button>
            </div>
            <br/><br/><br/>
            <p>If you're unhappy with your purchase for any reason, email us
              within 90 days and we'll refund you in full, no questions asked.</p>
            </EntityPanel>

          <EntityPanel
            entity = "Topics"
            icon = { HashtagIcon }
            quantity = {22}
            badge1={3}
            badge2={33}
          >
                If you're unhappy with your purchase for any reason, email us
                within 90 days and we'll refund you in full, no questions asked.
            </EntityPanel>
          <br/><br/>

            <h1 class="text-lg font-medium pb-2 pl-6 flex items-center">
              <KeyIcon class="w-5 h-5 mr-4 text-gray-800 ml-2" />
              Actions
            </h1>
          <br/>



            <div class=" pt-2 bg-gray-100 rounded-lg p-2 px-2 mx-auto  border-b-2 border-gray-200 text-gray-800">
              <div class="grid grid-cols-2">
                <h1 class="  mb-2 text-md font-medium pb-2 pl-2 flex items-center">
              <div class="rounded-full bg-white text-gray-400 p-2 border-b-4 border-b-gray-400 mr-3">
                  <DocumentAddIcon class="w-5 h-5  text-gray-400 " />
              </div>
                  Create a new entity
                </h1>
                <div class=" flex flex-shrink-0 place-self-end relative justify-around space-x-2           rounded-md      transition duration-300  float-right  ">
                  <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-200 transition duration-200 text-gray-700 focus:shadow-inner">
                    <CogIcon className="w-4 h-4 mr-1 item-center"/>
                    <Link href="#">Settings</Link>
                  </button>
                  <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-200 transition duration-200 text-gray-700 focus:shadow-inner">
                    <BellIcon className="w-4 h-4 mr-1 item-center"/>
                    <Link href="#">Advanced</Link>
                  </button>
                </div>
              </div>


              <div class="p-4 bg-white rounded-lg shadow h-12">

              </div>
            <div>
            </div>
          </div>

            <div class=" pt-2 bg-gray-100 rounded-lg p-2 px-2 mx-auto mt-4 border-b-2 border-gray-200 text-gray-800">
              <div class="grid grid-cols-2">
                <h1 class="  mb-2 text-md font-medium pb-2 pl-2 flex items-center">
              <div class="rounded-full bg-white text-gray-400 p-2 border-b-4 border-b-gray-400 mr-3">
                  <CalendarIcon class="w-5 h-5  text-gray-400" />
              </div>
                  Schedule a new item
                </h1>
                <div class=" flex flex-shrink-0 place-self-end relative justify-around space-x-2           rounded-md      transition duration-300  float-right  text-gray-400">
                  <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-200 transition duration-200 text-gray-700 focus:shadow-inner">
                    <CogIcon className="w-4 h-4 mr-1 item-center"/>
                    <Link href="#">Settings</Link>
                  </button>
                  <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-200 transition duration-200 text-gray-700 focus:shadow-inner">
                    <BellIcon className="w-4 h-4 mr-1 item-center"/>
                    <Link href="#">Advanced</Link>
                  </button>
                </div>
              </div>


              <div class="p-4 bg-white rounded-lg shadow h-12">

              </div>
            <div>
            </div>
          </div>
          <EntityPanel
            entity = "Synthesize others' automations"
            icon = { DocumentAddIcon }
            due="April 20"
          />
          <EntityPanel
            entity = "Link your accounts"
            icon = { LinkIcon }
            due="May 2"
          />


        </Disclosure.Panel>
          </Transition>
          }
              </div>
          </>
            )
        }
        </Disclosure>
      </div>

      </div>



  )

  } else if (tab == 1) {
    return  (
    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
        { header }
      </div>
    )
  } else if (tab == 2) {
    return  (
    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
        { header }
      </div>
    )
  } else if (tab == 3) {
    return  (
    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
        { header }
      </div>
    )
  } else {
    return  (
    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
        { header }
      </div>
    )
  }
}

const getRecords = async () => {
    const res = await axios.get("/api/record")
      .then(res => res.json());
    return res;
}

const getApiRecords = async () => {
  const res = await axios.get("dvapi.onrender.com/record")
      .then(res => res.json());
  return res;
}

export default function Editor() {
  const home = <EditorHome/>;
  const visual = <EditorVisual/>;
  const Source = <EditorSource/>;
  const userDashboard = (
    <DashLayout
      title = "Edit Automata"
      tab1 = "Home"
      tab2 = "Visual"
      tab3 = "Source"
      tab4 = "User"
      child1 = {<EditorHome/>}
      child2 = {<EditorVisual/>}
      child3 = {<EditorSource/>}
      child4 = {<EditorUser/>}
      mainBtn = "New"
      secondaryBtn = "Edit"
      breadcrumb = "Home / Edit Automata"
      bypassLogin = { true }
      >
    </DashLayout>
  )
    return userDashboard
}


export function EditorVisual() {
  const [ session, loading  ] = useSession()
  return (
    <>
    <div class="mt-4 mb-0 rounded-lg shadow-md pt-8 p-4 justify-center bg-white text-black mx-8">
      <Head>
        <title>Editor Home - devisa.io</title>
      </Head>
      <div class="grid-cols-2   divide-gray-400 mb-8">
        <div class="flex flex-row">
          <h1 class="font-light pl-8 text-4xl flex">
          <div class="rounded-full border-4 border-gray-200 bg-green-300  ">
          <ViewBoardsIcon className = "items-center w-10 h-10 pr-0 text-white"/>
          </div>
            <span class="ml-4  items-center mt-2 text-md">Visual Editor</span>
        </h1>
        </div>
        <div>
        <ul class="flex flex-row space-x-4 mr-16 text-sm font-light   float-right text-black  items-center justify-around">
          {/* <li class="items-start -mt-10"> */}
          {/*   <EditorBtn bg="white" icon={InformationCircleIcon}>Info</EditorBtn> */}
          {/* </li> */}
          {/* <li class="items-start -mt-10"> */}
          {/*   <EditorBtn bg="white" icon={EyeIcon}>Your Automata</EditorBtn> */}
          {/* </li> */}
          {/* <li class="items-start -mt-10"> */}
          {/*   <EditorBtn bg="white" icon={EyeIcon}>Guides</EditorBtn> */}
          {/* </li> */}
          <li class="items-start -mt-10 ml-8">
            <UserDropdown user={ session.user? session.user : { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>
          </li>
        </ul>
        </div>
    </div>

  <div class="flex-grow overflow-hidden h-full flex flex-col">

    <div class="h-16 lg:flex w-full border-b border-gray-200 dark:border-gray-800 hidden px-10">
      <div class="flex h-full text-gray-600 dark:text-gray-400">
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Assemble</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center">Explore</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Lab</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center">Deploy</a>
      </div>
      <div class="ml-auto flex items-center space-x-7">
        <button class="flex items-center">
          <span class="relative flex-shrink-0">
            <img class="w-7 h-7 rounded-full" src="/images/lightart.jpg" alt="profile" />
            <span class="absolute right-0 -mb-0.5 bottom-0 w-2 h-2 rounded-full bg-green-500 border border-white dark:border-gray-900"></span>
          </span>
          <span class="ml-2">James Smith</span>
          <svg viewBox="0 0 24 24" class="w-4 ml-1 flex-shrink-0" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <polyline points="6 9 12 15 18 9"></polyline>
          </svg>
        </button>

    </div>
  </div>
  </div>
      <div class="grid grid-cols-2 mt-6 mb-8 mx-3">
        <div class="bg-gray-100 rounded-md font-light m-3 shadow-sm p-6">
          <div class="">
            TitlejA
          </div>
          LKJ
        </div>
        <div class="bg-gray-100 rounded-md font-light m-3 shadow-sm p-6">
          SDF
        </div>
      </div>
      <div>
        { props.records }
        { props.users }
        { props.items }
      </div>

  </div>

      </>
  )

}

export function EditorSource() {
  return (
    <div class="mt-12 mb-16 rounded-lg shadow-md pt-8 p-4 justify-center  bg-white text-black mx-8">
      <Head>
        <title>Editor Home - devisa.io</title>
      </Head>
      <div class="grid-cols-2   divide-gray-400 mb-8">
        <div class="flex flex-row">
          <h1 class="font-light pl-8 text-4xl flex">
          <div class="rounded-full border-4 border-gray-200 bg-green-300  ">
          <CodeIcon className = "items-center w-10 h-10 pr-0 text-white mr"/>
          </div>
            <span class="ml-4">Source Code</span>
        </h1>
        </div>
        <div>
        <ul class="flex flex-row space-x-4 mr-16 text-sm font-light   float-right text-black hover:text-green-400 items-center justify-around">
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={InformationCircleIcon}>Info</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Your Automata</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Guides</EditorBtn>
          </li>
        </ul>
        </div>
    </div>

  <div class="flex-grow overflow-hidden h-full flex flex-col">
    <div class="h-16 lg:flex w-full border-b border-gray-200 dark:border-gray-800 hidden px-10">
      <div class="flex h-full text-gray-600 dark:text-gray-400">
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Company</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center">Users</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Expense Centres</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center">Currency Exchange</a>
      </div>
      <div class="ml-auto flex items-center space-x-7">

        <button class="flex items-center">
          <span class="relative flex-shrink-0">
            <img class="w-7 h-7 rounded-full" src="https://images.unsplash.com/photo-1521587765099-8835e7201186?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ" alt="profile" />
            <span class="absolute right-0 -mb-0.5 bottom-0 w-2 h-2 rounded-full bg-green-500 border border-white dark:border-gray-900"></span>
          </span>
          <span class="ml-2">James Smith</span>
          <svg viewBox="0 0 24 24" class="w-4 ml-1 flex-shrink-0" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <polyline points="6 9 12 15 18 9"></polyline>
          </svg>
        </button>
      </div>
    </div>

    <div class="flex-grow flex overflow-x-hidden">
      <div class="xl:w-72 w-48 flex-shrink-0 border-r border-gray-200 dark:border-gray-800 h-full overflow-y-auto lg:block hidden p-5">
        <div class="text-xs text-gray-400 tracking-wider">USERS</div>
        <div class="relative mt-2">
          <input type="text" class="pl-8 h-9 bg-transparent border border-gray-300 dark:border-gray-700 dark:text-white w-full rounded-md text-sm" placeholder="Search" />
          <svg viewBox="0 0 24 24" class="w-4 absolute text-gray-400 top-1/2 transform translate-x-0.5 -translate-y-1/2 left-2" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <circle cx="11" cy="11" r="8"></circle>
            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
          </svg>
        </div>
        <div class="space-y-4 mt-3">
          <button class="bg-white p-3 w-full flex flex-col rounded-md dark:bg-gray-800 shadow">
            <div class="flex xl:flex-row flex-col items-center font-medium text-gray-900 dark:text-white pb-2 mb-2 xl:border-b border-gray-200 border-opacity-75 dark:border-gray-700 w-full">
              <img src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=046c29138c1335ef8edee7daf521ba50" class="w-7 h-7 mr-2 rounded-full" alt="profile" />
              Kathyrn Murphy
            </div>
            <div class="flex items-center w-full">
              <div class="text-xs py-1 px-2 leading-none dark:bg-gray-900 bg-green-100 text-green-500 rounded-md">Design</div>
              <div class="ml-auto text-xs text-gray-500">$1,902.00</div>
            </div>
          </button>
          <button class="bg-white p-3 w-full flex flex-col rounded-md dark:bg-gray-800 shadow-lg relative ring-2 ring-green-500 focus:outline-none">
            <div class="flex xl:flex-row flex-col items-center font-medium text-gray-900 dark:text-white pb-2 mb-2 xl:border-b border-gray-200 border-opacity-75 dark:border-gray-700 w-full">
              <img src="https://assets.codepen.io/344846/internal/avatars/users/default.png?fit=crop&format=auto&height=512&version=1582611188&width=512" class="w-7 h-7 mr-2 rounded-full" alt="profile" />
              Mert Cukuren
            </div>
            <div class="flex items-center w-full">
              <div class="text-xs py-1 px-2 leading-none dark:bg-gray-900 bg-green-100 text-green-600 rounded-md">Sales</div>
              <div class="ml-auto text-xs text-gray-500">$2,794.00</div>
            </div>
          </button>
          <button class="bg-white p-3 w-full flex flex-col rounded-md dark:bg-gray-800 shadow">
            <div class="flex xl:flex-row flex-col items-center font-medium text-gray-900 dark:text-white pb-2 mb-2 xl:border-b border-gray-200 border-opacity-75 dark:border-gray-700 w-full">
              <img src="https://images.unsplash.com/photo-1541647376583-8934aaf3448a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ" class="w-7 h-7 mr-2 rounded-full" alt="profile" />
              Albert Flores
            </div>
            <div class="flex items-center w-full">
              <div class="text-xs py-1 px-2 leading-none dark:bg-gray-900 bg-yellow-100 text-yellow-600 rounded-md">Marketing</div>
              <div class="ml-auto text-xs text-gray-400">$0.00</div>
            </div>
          </button>
          <button class="bg-white p-3 w-full flex flex-col rounded-md dark:bg-gray-800 shadow">
            <div class="flex xl:flex-row flex-col items-center font-medium text-gray-900 dark:text-white pb-2 mb-2 xl:border-b border-gray-200 border-opacity-75 dark:border-gray-700 w-full">
              <img src="https://images.unsplash.com/photo-1519699047748-de8e457a634e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ" class="w-7 h-7 mr-2 rounded-full" alt="profile" />
              Jane Cooper
            </div>
            <div class="flex items-center w-full">
              <div class="text-xs py-1 px-2 leading-none dark:bg-gray-900 bg-green-100 text-green-500 rounded-md">Design</div>
              <div class="ml-auto text-xs text-gray-500">$762.00</div>
            </div>
          </button>
          <button class="bg-white p-3 w-full flex flex-col rounded-md dark:bg-gray-800 shadow">
            <div class="flex xl:flex-row flex-col items-center font-medium text-gray-900 dark:text-white pb-2 mb-2 xl:border-b border-gray-200 border-opacity-75 dark:border-gray-700 w-full">
              <img src="https://images.unsplash.com/photo-1507120878965-54b2d3939100?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=99fbace66d1bfa48c9c6dc8afcac3aab" class="w-7 h-7 mr-2 rounded-full" alt="profile" />
              Ronald Richards
            </div>
            <div class="flex items-center w-full">
              <div class="text-xs py-1 px-2 leading-none dark:bg-gray-900 bg-green-100 text-green-600 rounded-md">Sales</div>
              <div class="ml-auto text-xs text-gray-400">$0.00</div>
            </div>
          </button>
        </div>
        </div>
      </div>

    </div>
      </div>





  )

}
export function Old() {
  return (
    <div class="mt-12 mb-16 rounded-lg shadow-md pt-8 p-4 justify-center  bg-white text-black mx-8">
      <div class="grid grid-cols-2">
        <div class="flex flex-row">
        <h1 class="font-light pl-24 text-4xl flex">
          <div class="rounded-full border-4 border-gray-300 bg-gray-200">
          <HomeIcon className = "items-center w-10 h-10 pr-0 text-gray-400 mr"/>
          </div>
          Editor
        </h1>
        </div>
        <div>
        <ul class="flex flex-row space-x-4 mr-16 text-sm font-light   float-right text-black hover:text-green-400 items-center justify-around">
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={InformationCircleIcon}>Info</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Your Automata</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Guides</EditorBtn>
          </li>
        </ul>
        </div>
      </div>
      <br/>
      <hr/><br/><br/>
    </div>

  )
}

// export function Tabs() {
//   const [openTab, setTab] = React.useState(0);
//   return (
//     <>
//       <div className="flex flex-wrap">
//         <div className="w-full">
//           <ul
//             className="flex flex-row flex-wrap pt-3 pb-4 mb-0 list-none"
//             role="tablist"
//           >
//             <li className="flex-auto mr-2 -mb-px text-center last:mr-0">
//               <a
//                 className={
//                   "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
//                   (openTab === 1
//                     ? "text-white bg-" + color + "-600"
//                     : "text-" + color + "-600 bg-white")
//                 }
//                 onClick={e => {
//                   e.preventDefault();
//                   setTab(1);
//                 }}
//                 data-toggle="tab"
//                 href="#link1"
//                 role="tablist"
//               >
//                 <i className="mr-1 text-base fas fa-space-shuttle"></i> Profile
//               </a>
//             </li>
//             <li className="flex-auto mr-2 -mb-px text-center last:mr-0">
//               <a
//                 className={
//                   "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
//                   (openTab === 2
//                     ? "text-white bg-" + color + "-600"
//                     : "text-" + color + "-600 bg-white")
//                 }
//                 onClick={e => {
//                   e.preventDefault();
//                   setTab(2);
//                 }}
//                 data-toggle="tab"
//                 href="#link2"
//                 role="tablist"
//               >
//                 <i className="mr-1 text-base fas fa-cog"></i>  Settings
//               </a>
//             </li>
//             <li className="flex-auto mr-2 -mb-px text-center last:mr-0">
//               <a
//                 className={
//                   "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
//                   (openTab === 3
//                     ? "text-white bg-" + color + "-600"
//                     : "text-" + color + "-600 bg-white")
//                 }
//                 onClick={e => {
//                   e.preventDefault();
//                   setTab(3);
//                 }}
//                 data-toggle="tab"
//                 href="#link3"
//                 role="tablist"
//               >
//                 <i className="mr-1 text-base fas fa-briefcase"></i>  Options
//               </a>
//             </li>
//           </ul>
//           <div className="relative flex flex-col w-full min-w-0 mb-6 break-words bg-white rounded shadow-lg">
//             <div className="flex-auto px-4 py-5">
//               <div className="tab-content tab-space">
//                 <div className={openTab === 1 ? "block" : "hidden"} id="link1">
//                   <p>
//                     Collaboratively administrate empowered markets via
//                     plug-and-play networks. Dynamically procrastinate B2C users
//                     after installed base benefits.
//                     <br />
//                     <br /> Dramatically visualize customer directed convergence
//                     without revolutionary ROI.
//                   </p>
//                 </div>
//                 <div className={openTab === 2 ? "block" : "hidden"} id="link2">
//                   <p>
//                     Completely synergize resource taxing relationships via
//                     premier niche markets. Professionally cultivate one-to-one
//                     customer service with robust ideas.
//                     <br />
//                     <br />
//                     Dynamically innovate resource-leveling customer service for
//                     state of the art customer service.
//                   </p>
//                 </div>
//                 <div className={openTab === 3 ? "block" : "hidden"} id="link3">
//                   <p>
//                     Efficiently unleash cross-media information without
//                     cross-media value. Quickly maximize timely deliverables for
//                     real-time schemas.
//                     <br />
//                     <br /> Dramatically maintain clicks-and-mortar solutions
//                     without functional solutions.
//                   </p>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>

//   )

// }
  //
export function EditorUser() {
  return (
    <div class="mt-12 mb-16 rounded-lg shadow-md pt-8 p-4 justify-center  bg-white text-black mx-8">
      <div class="grid-cols-2   divide-gray-400 mb-8">
        <div class="flex flex-row">
          <h1 class="font-light pl-8 text-4xl flex">
          <div class="rounded-full border-4 border-gray-200 bg-green-300  ">
          <UserIcon className = "items-center w-10 h-10 pr-0 text-white mr"/>
          </div>
            <span class="ml-4">Source Code</span>
        </h1>
        </div>
        <div>
        <ul class="flex flex-row space-x-4 mr-16 text-sm font-light   float-right text-black hover:text-green-400 items-center justify-around">
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={InformationCircleIcon}>Info</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Your Automata</EditorBtn>
          </li>
          <li class="items-start -mt-10">
            <EditorBtn bg="white" icon={EyeIcon}>Guides</EditorBtn>
          </li>
        </ul>
        </div>
    </div>

  <div class="flex-grow overflow-hidden h-full flex flex-col">
    <div class="h-16 lg:flex w-full border-b border-gray-200 dark:border-gray-800 hidden px-10">
      <div class="flex h-full text-gray-600 dark:text-gray-400">
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Company</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-green-500 text-green-500 dark:text-white dark:border-white inline-flex mr-8 items-center">Users</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center mr-8">Expense Centres</a>
        <a href="#" class="cursor-pointer h-full border-b-2 border-transparent inline-flex items-center">Currency Exchange</a>
      </div>
      <div class="ml-auto flex items-center space-x-7">

        <button class="flex items-center">
          <span class="relative flex-shrink-0">
            <img class="w-7 h-7 rounded-full" src="https://images.unsplash.com/photo-1521587765099-8835e7201186?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ" alt="profile" />
            <span class="absolute right-0 -mb-0.5 bottom-0 w-2 h-2 rounded-full bg-green-500 border border-white dark:border-gray-900"></span>
          </span>
          <span class="ml-2">James Smith</span>
          <svg viewBox="0 0 24 24" class="w-4 ml-1 flex-shrink-0" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <polyline points="6 9 12 15 18 9"></polyline>
          </svg>
        </button>
      </div>
    </div>
    </div>
    </div>


  )
}

// export async function getStaticProps() {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library
  // const res = await axios.get('/api/records')
  // const records = await res.json()

  /* const records = await fetch('http://dvapi.onrender.com/record')
    .then(r => r.json());
  const users = await fetch('http://dvapi.onrender.com/record')
    .then(r => r.json());
  const items = await fetch('http://dvapi.onrender.com/record')
    .then(r => r.json()); */

  /* console.log(records)
  console.log(users)
  console.log(items) */

  // By returning { props: { posts } }, the Blog component
  // will receive `posts` as a prop at build time
  /* return {
    props: {
      records,
      items,
      users
    },
<<<<<<< HEAD
  } */
}
=======
  }
} */
>>>>>>> 019db05a0a0588952785408e395485cf240b6997
