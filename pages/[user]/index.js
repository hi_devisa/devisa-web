import DashLayout from '../../components/layout/dashboard'
import Modal from '../../components/ui/modal/simple'
import Post from '../../components/user/post'
import ModalAlt from '../../components/ui/modal/alt'
import Layout from '../../components/layout'
import Card from '../../components/profile/card'
import DropdownButton from '../../components/ui/dropdown/button'
import Dropdown from '../../components/dropdown'
import UserCardSmall from '../../components/user/cardSmall'
import { useSession } from 'next-auth/client'

// TODO Decide on whether to use credentials name or oauth name
export default function UserProfile() {
  const items2 = [
    { text: "Test", value: "#", disabled: false },
    { text: "Profile", value: "#", disabled: false },
    { text: "Feed", value: "#", disabled: false }
  ]
  const [ session, loading ] = useSession()
  const child1 = (
  <div class="container mx-auto p-6">
    <div class="flex -mx-2">
      <div class="w-3/5 px-2">
        <div class="mb-6">
          <h2 class="text-lg font-medium text-gray-900 pb-6">Overview</h2>
          <div class="bg-white rounded shadow overflow-hidden">
            <div class="px-6 py-5">
              <dl>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Application For</dt>
                    <dd class="text-base font-normal text-gray-900">Backend Developer</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Salary Expectation</dt>
                    <dd class="text-base font-normal text-gray-900">$120,000</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Email</dt>
                    <dd class="text-base font-normal text-gray-900">richardocooper@example.com</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Phone</dt>
                    <dd class="text-base font-normal text-gray-900">+1 555-555-5555</dd>
                    <Dropdown items={items2} text="hello">HellO!</Dropdown>
                    <Modal
                      text="Message"
                      title="Message Chris?"
                      content="
                       Server side renders at runtime (uses getInitialProps or getServerSideProps)
                    "/>
                    <ModalAlt/>

                  </div>
                </div>
                <div class="flex">
                  <div class="w-full">
                    <dt class="text-sm font-medium text-gray-500">Attachments</dt>
                    <dd class="mt-2">
                      <ul class="border border-gray-200 rounded">
                        <li class="flex items-center px-4 py-3">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            resume_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                        <li class="flex items-center px-4 py-3 border-t border-gray-200">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            cover_letter_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
            </div>
            <div>
              <button class="flex items-center justify-center w-full px-6 py-2 bg-gray-100">
                <span class="text-sm font-medium text-gray-500 mr-2">Read full application</span>
                <i class="fas fa-angle-down"></i>
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2 class="text-lg font-medium text-gray-900 pb-6">Notes</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div class="flex border-b">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="flex">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="bg-gray-100 p-6">
              <label for="note" class="text-sm font-semibold block">Add a note</label>
              <textarea name="note" id="note" class="w-full rounded-lg border"></textarea>
              <div class="flex justify-end">
                <button class="text-white bg-green-600 py-2 px-3 rounded">Post Note</button>
              </div>
            </div>
          </div>

          <h2 class="text-lg font-medium text-gray-900 pb-6">Notes</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div class="flex border-b">
              <Post/>
             </div>
          </div>
        </div>
      </div>
      <div class="w-2/5 px-2">
        <div class="pb-6">
          <Card/>
        </div>
        <h2 class="text-lg font-medium text-gray-900 pb-6">Timeline</h2>
<div class="bg-white shadow overflow-hidden sm:rounded-lg">
  <div class="px-4 py-5 sm:px-6">
    <h3 class="text-lg leading-6 font-medium text-gray-900">
      Applicant Information
    </h3>
    <p class="mt-1 max-w-2xl text-sm text-gray-500">
      Personal details and application.
    </p>
  </div>
  <div class="border-t border-gray-200">
    <dl>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Full name
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Margot Foster
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Application for
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Backend Developer
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Email address
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          margotfoster@example.com
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Salary expectation
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          $120,000
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          About
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Attachments
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  resume_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  coverletter_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
          </ul>
        </dd>
      </div>
    </dl>
  </div>
</div>
        <h2 class="mt-8 text-lg font-medium text-gray-900 pb-6">Timeline</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div>
              <div class="flex pt-6">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Applied</span> to <span class="font-semibold text-gray-900">Front-end Developer</span>
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to phone screening by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-check-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Completed</span> phone screening with Martha Gardner
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to interview by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4">
                  <span class="font-semibold text-gray-900">Completed</span> interview with Katherine Snyden
                </div>
                <div class="w-1/6 pb-4">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex bg-white px-6 py-4 border-t">
                <button class="w-full justify-center px-6 py-2 bg-green-600 rounded">
                  <span class="text-white">Advance to Offer</span>
                </button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  )
  const child2 = (
    <p class="text-4xl"> CHILD 2 </p>
  )
  const child3 = (
    <p class="text-4xl"> CHILD 3 </p>
  )
  const child4 = (
    <p class="text-4xl"> CHILD 4 </p>
  )
  if (loading) {
    return (
      <h1 class="text-4xl">
        Loading
      </h1>
    )
  }
  return (
    <DashLayout
      title = {`${ session.user.name }'s Profile`}
      mainBtn = "Edit Items"
      secondaryBtn = "Edit Dashboard"
      breadcrumb = { `${ session.user.name }'s Profile` }
      child1 = { child1 }
      tab1 = "Overview"
      child2 = { child2 }
      tab2 = "Timeline"
      child3 = { child3 }
      tab3 = "Collection"
      child4 = { child4 }
      tab4 = "Groups"
      >

    </DashLayout>
  )
}
