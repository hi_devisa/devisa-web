
import Swal from 'sweetalert2'
import { PrismaClient } from '@prisma/client'
import RecordList from '../../components/lab/recordList'
import Layout from '../../components/layout'
import PageHeader from '../../components/ui/pageHeader'
import Select from '../../components/ui/listbox'
import Dropdown from '../../components/dropdown'
import UserCard from '../../components/user/card'
import Card from '../../components/card'
import { useSession } from 'next-auth/client'

export default function UserProfile(props) {

  const items = [
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  function toast(event) {
    const to = Swal.mixin({
      toast: true,
      animation: false,
      position: 'bottom',
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (t) => {
        t.addEventListener('mouseenter', Swal.stopTimer),
        t.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    to.fire({animation: false})
  }
  function handleClick(event) {
    event.preventDefault();
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((res) => {
      if (res.value) {
        Swal.fire('Deleted!', 'File deleted', 'success')
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'File is safe', 'error')
      }
    })
  }
  const [session] = useSession()
  console.log(props)
  return (
    <Layout session = {session} title= { props.user + "'s Profile" } header= {false}>
<div class="bg-gray-100">
  <div class="bg-white shadow">
    <div class="container mx-auto px-6">
      <div class="flex items-center justify-between py-5">
        <div>
          <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-2">
            <i class="fas fa-angle-left"></i>
            <span>Backend Developer</span>
           </a>
          <div class="flex items-center">
            <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
            <h1 class="px-2 text-2xl text-gray-900 font-extrabold">Ricardo Cooper</h1>
          </div>
        </div>
        <div class="flex items-center">
          <button class="h-9 border border-gray-200 px-3 py-2 text-sm font-medium rounded text-gray-700 shadow">Disqualify</button>
          <button class="h-9 ml-3 px-3 py-2 text-sm font-medium rounded bg-indigo-600 text-white shadow">Advance to Offer</button>
        </div>
      </div>
    </div>
  </div>
  <div class="container mx-auto p-6">
    <div class="flex -mx-2">
      <div class="w-3/5 px-2">
        <div class="mb-6">
          <h2 class="text-lg font-medium text-gray-900 pb-6">Overview</h2>
          <div class="bg-white rounded shadow overflow-hidden">
            <div class="px-6 py-5">
              <dl>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Application For</dt>
                    <dd class="text-base font-normal text-gray-900">Backend Developer</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Salary Expectation</dt>
                    <dd class="text-base font-normal text-gray-900">$120,000</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Email</dt>
                    <dd class="text-base font-normal text-gray-900">richardocooper@example.com</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Phone</dt>
                    <dd class="text-base font-normal text-gray-900">+1 555-555-5555</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-full">
                    <dt class="text-sm font-medium text-gray-500">Attachments</dt>
                    <dd class="mt-2">
                      <ul class="border border-gray-200 rounded">
                        <li class="flex items-center px-4 py-3">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            resume_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                        <li class="flex items-center px-4 py-3 border-t border-gray-200">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            cover_letter_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
            </div>
            <div>
              <button class="flex items-center justify-center w-full px-6 py-2 bg-gray-100">
                <span class="text-sm font-medium text-gray-500 mr-2">Read full application</span>
                <i class="fas fa-angle-down"></i>
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2 class="text-lg font-medium text-gray-900 pb-6">Notes</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div class="flex border-b">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="flex">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="bg-gray-100 p-6">
              <label for="note" class="text-sm font-semibold block">Add a note</label>
              <textarea name="note" id="note" class="w-full rounded-lg border"></textarea>
              <div class="flex justify-end">
                <button class="text-white bg-indigo-600 py-2 px-3 rounded">Post Note</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="w-2/5 px-2">
        <h2 class="text-lg font-medium text-gray-900 pb-6">Timeline</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div>
              <div class="flex pt-6">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Applied</span> to <span class="font-semibold text-gray-900">Front-end Developer</span>
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to phone screening by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-check-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Completed</span> phone screening with Martha Gardner
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to interview by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4">
                  <span class="font-semibold text-gray-900">Completed</span> interview with Katherine Snyden
                </div>
                <div class="w-1/6 pb-4">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex bg-white px-6 py-4 border-t">
                <button class="w-full justify-center px-6 py-2 bg-indigo-600 rounded">
                  <span class="text-white">Advance to Offer</span>
                </button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
      {session?
        <div class="grid grid-cols-4">
          <div>
          <UserCard
            title={ props.user }
            image= { session? session.user.image : 'lightart.jpg' }
            href={"/" + props.user}
            imageHref= { "/" + props.user }
            name = { session? session.user.name : "joe"}>
            <button onClick={handleClick}>Click</button>
            <button onClick={toast}>Toast</button>
            <Dropdown items={items} text="Nav"/>
            <Select/>
            <p> sessionUser: { session? session.user.email : "joe@schmoe.com" } </p>
            <p> sessionUser: { session? session.user.name : "joe" } </p>
            <p> sessionExpires: { session? session.expires : "undefined"} </p>
            <p> sessionAccessToken: { session? session.accessToken : "n/a" }</p>
          </UserCard>
            <div class="my-4">
<div class="bg-white shadow overflow-hidden sm:rounded-lg">
  <div class="px-4 py-5 sm:px-6">
    <h3 class="text-lg leading-6 font-medium text-gray-900">
      Applicant Information
    </h3>
    <p class="mt-1 max-w-2xl text-sm text-gray-500">
      Personal details and application.
    </p>
  </div>
  <div class="border-t border-gray-200">
    <dl>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Full name
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Margot Foster
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Application for
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Backend Developer
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Email address
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          margotfoster@example.com
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Salary expectation
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          $120,000
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          About
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Attachments
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  resume_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">
                  Download
                </a>
              </div>
            </li>
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  coverletter_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">
                  Download
                </a>
              </div>
            </li>
          </ul>
        </dd>
      </div>
    </dl>
  </div>
</div>
            </div>
          <div class="mt-8">
            <Card/>
            </div>
          <div class=" rounded-lg shadow-lg mx-auto p-12 w-full">
            <p>
  </p>
  </div>
  </div>

          <div class=" rounded-lg shadow-md mx-8 p-12 col-span-2">
            <p class="text-2xl">User info</p>
              <RecordList/>
            <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, est ut egestas imperdiet, quam odio vestibulum neque, eget tincidunt lorem ligula at dui. Fusce rutrum ipsum vel commodo ullamcorper. Ut lacinia hendrerit lacus, ultrices accumsan metus placerat a. Morbi blandit dapibus auctor. Sed faucibus massa dolor. Phasellus at fringilla tortor. Proin ac sem molestie, vestibulum lacus ut, feugiat odio. Aenean ullamcorper orci non suscipit rutrum.

Cras urna nisl, ullamcorper vitae pulvinar et, elementum non mi. Fusce eu dui hendrerit, tincidunt metus volutpat, accumsan magna. In sed leo eget diam consequat iaculis nec at ligula. Integer ac sem odio. Etiam a arcu non justo pulvinar aliquet. Mauris scelerisque, justo sit amet vehicula aliquam, enim elit scelerisque magna, eu consectetur arcu tortor iaculis neque. Integer vitae ornare quam, id consequat dolor.

Suspendisse risus leo, hendrerit nec condimentum a, tempor ac sapien. Cras malesuada pulvinar eros non placerat. Mauris at mi semper, pretium mi non, consectetur velit. Pellentesque mi tortor, convallis vel velit interdum, posuere blandit justo. Etiam lobortis eros vitae dignissim lobortis. Nam risus libero, pellentesque non urna at, elementum venenatis sem. Nullam lobortis consectetur erat, a pellentesque magna dictum sit amet. Pellentesque rhoncus accumsan ipsum eu dapibus. Curabitur ornare suscipit maximus. Vestibulum et consectetur justo. Donec pretium diam sapien, non lobortis orci luctus id. Etiam consequat sit amet nisl vel sagittis. Mauris porttitor pellentesque ante, ac consequat turpis molestie sed. Fusce sit amet efficitur sem. Morbi gravida purus massa. Cras sed urna elit.

Curabitur sollicitudin mi in ipsum sollicitudin ornare. Sed id auctor libero. Vivamus in pretium lectus. Integer euismod, est a vestibulum tempor, tellus neque mattis nulla, sed laoreet nisl nisl nec metus. Sed mollis cursus massa vel mollis. Vivamus a ligula congue risus ultricies malesuada. Vivamus ac hendrerit orci. Donec velit quam, eleifend nec tellus ut, tristique vestibulum turpis. Donec quis interdum purus. Maecenas id quam turpis.

  Morbi venenatis arcu purus, eget consectetur risus commodo commodo. Integer semper dignissim diam at fringilla. In hac habitasse platea dictumst. Mauris lectus turpis, lacinia quis faucibus mollis, ultricies quis risus. Nullam ac nisi fermentum, lacinia lacus in, gravida risus. Nulla facilisi. Vestibulum ut justo a erat tempus dictum id non odio. Phasellus viverra ante sed condimentum pellentesque. Donec dui tellus, malesuada non sagittis sed, dignissim eu diam. Donec sodales nunc nec porttitor aliquam.</p>
          </div>
          <div>
            <div>
            <Card/>
            </div>
          <div class=" rounded-lg mx-auto mx-8 p-12 shadow-md">
            <p class="text-2xl">Records</p>
            <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, est ut egestas imperdiet, quam odio vestibulum neque, eget tincidunt lorem ligula at dui. Fusce rutrum ipsum vel commodo ullamcorper. Ut lacinia hendrerit lacus, ultrices accumsan metus placerat a. Morbi blandit dapibus auctor. Sed faucibus massa dolor. Phasellus at fringilla tortor. Proin ac sem molestie, vestibulum lacus ut, feugiat odio. Aenean ullamcorper orci non suscipit rutrum.

Cras urna nisl, ullamcorper vitae pulvinar et, elementum non mi. Fusce eu dui hendrerit, tincidunt metus volutpat, accumsan magna. In sed leo eget diam consequat iaculis nec at ligula. Integer ac sem odio. Etiam a arcu non justo pulvinar aliquet. Mauris scelerisque, justo sit amet vehicula aliquam, enim elit scelerisque magna, eu consectetur arcu tortor iaculis neque. Integer vitae ornare quam, id consequat dolor.

Suspendisse risus leo, hendrerit nec condimentum a, tempor ac sapien. Cras malesuada pulvinar eros non placerat. Mauris at mi semper, pretium mi non, consectetur velit. Pellentesque mi tortor, convallis vel velit interdum, posuere blandit justo. Etiam lobortis eros vitae dignissim lobortis. Nam risus libero, pellentesque non urna at, elementum venenatis sem. Nullam lobortis consectetur erat, a pellentesque magna dictum sit amet. Pellentesque rhoncus accumsan ipsum eu dapibus. Curabitur ornare suscipit maximus. Vestibulum et consectetur justo. Donec pretium diam sapien, non lobortis orci luctus id. Etiam consequat sit amet nisl vel sagittis. Mauris porttitor pellentesque ante, ac consequat turpis molestie sed. Fusce sit amet efficitur sem. Morbi gravida purus massa. Cras sed urna elit.

Curabitur sollicitudin mi in ipsum sollicitudin ornare. Sed id auctor libero. Vivamus in pretium lectus. Integer euismod, est a vestibulum tempor, tellus neque mattis nulla, sed laoreet nisl nisl nec metus. Sed mollis cursus massa vel mollis. Vivamus a ligula congue risus ultricies malesuada. Vivamus ac hendrerit orci. Donec velit quam, eleifend nec tellus ut, tristique vestibulum turpis. Donec quis interdum purus. Maecenas id quam turpis.

  Morbi venenatis arcu purus, eget consectetur risus commodo commodo. Integer semper dignissim diam at fringilla. In hac habitasse platea dictumst. Mauris lectus turpis, lacinia quis faucibus mollis, ultricies quis risus. Nullam ac nisi fermentum, lacinia lacus in, gravida risus. Nulla facilisi. Vestibulum ut justo a erat tempus dictum id non odio. Phasellus viverra ante sed condimentum pellentesque. Donec dui tellus, malesuada non sagittis sed, dignissim eu diam. Donec sodales nunc nec porttitor aliquam.</p>
          </div>
        </div>
  </div>
         :
        <p> No user logged in </p>
      }
    </Layout>
  )
}

// export async function getStaticProps(context) {
  // const res = await fetch(`http://localhost:3000/api/hello`)
  // const data = await res.text()
  /* const user  = context.params.user
  const data = true;
  console.log(context.params) */

  // if (!data || data) {
  //   return {
  //     redirect: {
  //       destination: '/',
  //       permanent: false,
  //     },
  //   }
  // }

  /* return {
    props: {
      user: user,
      data: data,
    }, // will be passed to the page component as props
  }
} */

/* export async function getStaticPaths() {
  return {
    paths: [
      { params: { user: 'chris' } },
      { params: { user: 'test' } },
    ],
  }
}
 */
