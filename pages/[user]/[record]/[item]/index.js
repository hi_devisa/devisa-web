import Swal from 'sweetalert2'
import Layout from '../../../../components/layout'
import Dropdown from '../../../../components/dropdown'
import UserCard from '../../../../components/user/card'
import Card from '../../../../components/card'
import { useSession } from 'next-auth/client'

export default function Item(props) {
  const [session] = useSession();
  const recordTitle =  props.user + "/" + props.record;
  const item = props.item
  return (
  <Layout session = {session} title={ recordTitle }>
      <UserCard
        title={ recordTitle + ": " + item }
        image= { session? session.user.image : "/lightart.jpg" }
        href={"/" + props.user + "/" + props.record + "/" + props.item}
        imageHref={"/" + props.user}
        name = { item }>
      </UserCard>

    </Layout>

  )

}

// export async function getStaticProps(context) {
  // const res = await fetch(`http://localhost:3000/api/hello`)
  // const data = await res.text()
  /* const user  = context.params.user
  const record  = context.params.record
  const item = context.params.item
  const data = true;
  console.log(context.params) */

  // if (!data || data) {
  //   return {
  //     record: record,
  //     user: user,
  //     item: item,
  //     redirect: {
  //       destination: '/',
  //       permanent: false,
  //     },
  //   }
  // }
/*
  return {
    props: {
      record: record,
      user: user,
      data: data,
      item: item,
    }, // will be passed to the page component as props
  }
}
 */
/* export async function getStaticPaths() {
  return {
    paths: [
      { params: { user: 'chris', record: 'test', item: 'item1' } },
      { params: { user: 'chris', record: 'test', item: 'item2' } },
      { params: { user: 'chris', record: 'record1', item: 'test' } },
    ],
  }
}
 */
