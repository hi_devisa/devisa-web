

import  axios  from 'axios'
import Swal from 'sweetalert2'
import Layout from '../../../components/layout'
import Dropdown from '../../../components/dropdown'
import UserCard from '../../../components/user/card'
import Card from '../../../components/card'
import Post from '../../../components/user/post'
import { useSession } from 'next-auth/client'

class Record {

  constructor(name, description, userId, items) {
    this.id = null;
    this.name = name;
    this.description = description;
    this.items = [];
    this.userId = userId;
    this.priv = true;
    this.status = "Active";
    this.image = image;
    this.createdAt = Date.now();
    this.updatedAt = Date.now();
  }

  constructorAll(id, name, description, image, userId, priv, status, createdAt, updatedAt) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.userId = userId;
    this.priv = priv;
    this.status = status;
    this.image = image;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  async insert() {
    const res = await axios.post("https://api.devisa.io/record" + this.userId, {
      data: JSON.stringify(this),
    })
      .catch(e => console.error(e))
      .then(r => r.text());
    return res;
  }
}

export default function RecordPage(props) {
  const [ session, loading ] = useSession();


  async function getById(id) {
    const res = await axios.get("https://api.devisa.io/record/" + id)
      .then(r => r.text());
    return new Record(
      this.id,
      this.name,
      this.description,
      this.image,
      this.userId,
      this.priv,
      this.status,
      this.createdAt,
      this.updatedAt
    )
  }

  const recordTitle =  props.user + "/" + props.record;
  return (
  <Layout session = {session} title={ recordTitle }>
    <br/>
    <br/>
    <p><span class="ml-8 text-5xl font-light"> { props.record }</span><span class="ml-10 text-4xl font-light text-gray-600">{ props.user }</span></p>
    <br/>
    <hr/>
    <br/><br/>
    <br/><br/>
    <br/><br/>
    <div class="grid grid-cols-2">
      <div>
      <UserCard
        title={ recordTitle }
        image= { session? session.user.image : "/lightart.jpg" }
        href={"/user/" + props.user + "/" + props.record}
        imageHref={"/" + props.user}
        name = { recordTitle }>
        This is the user's info space
      </UserCard>
      </div>
      <div>
        <Post
          user = "Chris P"
          username = "chris"
          img = "/lightart.jpg"
          subtitle = "Admin"
          subsubtitle = "Joined 22d ago"
        >
          This is a test post cotent lorem ipsum
        </Post>
        <Post
          user = "Chris P"
          username = "chris"
          img = "/lightart.jpg"
          subtitle = "Admin"
          subsubtitle = "Joined 22d ago"
        >
          This is a test post cotent lorem ipsum
        </Post>
      </div>
      </div>

    </Layout>

  )

}

// export async function getStaticProps(context) {
  // const res = await fetch(`http://localhost:3000/api/hello`)
  // const data = await res.text()
  /* const user  = context.params.user
  const record  = context.params.record */
  /* const data = true;
  console.log(context.params) */

  // if (!data || data) {
  //   return {
  //     redirect: {
  //       destination: '/',
  //       permanent: false,
  //     },
  //   }
  // }
/*
  return {
    props: {
      record: record,
      user: user,
      data: data,
    }, // will be passed to the page component as props
  }
} */
/*
export async function getStaticPaths() {
  return {
    paths: [
      { params: { user: 'chris', record: 'test' } },
      { params: { user: 'chris', record: 'record1' } },
    ],
  }
}
 */
