import Card from '../../components/card'
import Layout from '../../components/layout'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'

export default function Blog() {
  const [ session, loading ] = useSession()
  const router = useRouter()
  return (
    <>
      <Layout session={session} title="Blog" header={true}>
        <Card
          topic = "Blog coming soon!"
          title= "Our blog will be rolled out soon!">
          Stay tuned for first blog post as we come closer to an initial viable release for our life, project, and productivity data and management dashboard of the future.
        </Card>
      </Layout>
    </>
  )
}
