import Layout from '../../../components/layout'
import Head from 'next/head'
import { NextSeo } from 'next-seo'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
// import { Card, Tabs, Menu, Input, IconHome, Auth } from '@supabase/ui'

export default function About() {
  const [ session, loading ] = useSession()
  const router = useRouter()
  return (
    <Layout title="Posts" session={session}>
    </Layout>
  )
}
