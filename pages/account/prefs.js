
import DashLayout from '../../components/layout/dashboard'

export default function Dashboard() {
  const userDashboard = (
    <DashLayout
      title = "Preferences"
      mainBtn = "Save Preferences"
      secondaryBtn = "Edit Preferences"
      breadcrumb = "Home / Account / Preferences"
      >
        <p class="text-xl">Your Preferences</p>
    </DashLayout>
  )
    return userDashboard
}
