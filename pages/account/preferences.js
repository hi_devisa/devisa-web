import Swal from 'sweetalert2'
import { PrismaClient } from '@prisma/client'
import RecordList from '../../components/lab/recordList'
import Select from '../../components/ui/listbox'
import Dropdown from '../../components/dropdown'
import Link from 'next/link'
import Head from 'next/head'
import Card from '../../components/card'
import Switch from '../../components/ui/switch'
import Layout from '../../components/layout'
import PageHeader from '../../components/ui/pageHeader'
import { useSession } from 'next-auth/client'
import { Router, useRouter } from 'next/router'
import UserCard from '../../components/user/card'

export default function Account() {
  const [session] = useSession()
  const  router  = useRouter()
  function toast(event) {
    const to = Swal.mixin({
      toast: true,
      animation: false,
      position: 'bottom',
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (t) => {
        t.addEventListener('mouseenter', Swal.stopTimer),
        t.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    to.fire({animation: false})
  }
  function handleClick(event) {
    event.preventDefault();
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((res) => {
      if (res.value) {
        Swal.fire('Deleted!', 'File deleted', 'success')
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'File is safe', 'error')
      }
    })
  }
  return (
    <Layout title="Account Preferences" session={session} header={false}>
      <div class="mt-4 pb-8 mb-16">
      <PageHeader
        title="Account preferences"
        mainBtn="Edit Profile"
        btn1="Profile Settings"
        btn2="Account Settings"
        desc1="Remote"
        desc2="Seattle, WA"
        desc3="Self employed"
        desc4="Working on project B"/>
      </div>
        <div class="grid grid-cols-4">
          <div>
          <UserCard
            title={ "user" }
            image= { "lightart.jpg" }
            href={"/chris"}
            imageHref= { "/chris"}
            name = { "joe" }>
          </UserCard>
            <div class="my-4">
<div class="bg-white shadow overflow-hidden sm:rounded-lg">
  <div class="px-4 py-5 sm:px-6">
    <h3 class="text-lg leading-6 font-medium text-gray-900">
      Applicant Information
    </h3>
    <p class="mt-1 max-w-2xl text-sm text-gray-500">
      Personal details and application.
    </p>
  </div>
  <div class="border-t border-gray-200">
    <dl>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Full name
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Margot Foster
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Application for
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Backend Developer
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Email address
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          margotfoster@example.com
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Salary expectation
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          $120,000
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          About
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Attachments
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  resume_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  coverletter_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
          </ul>
        </dd>
      </div>
    </dl>
  </div>
</div>
            </div>
          <div class="mt-8">
            <Card/>
            </div>
          <div class=" rounded-lg shadow-lg mx-auto p-12 w-full">
            <p>
  </p>
  </div>
  </div>

          <div class=" rounded-lg shadow-md mx-8 p-12 col-span-2">
            <p class="text-2xl">User info</p>
              <RecordList/>
            <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, est ut egestas imperdiet, quam odio vestibulum neque, eget tincidunt lorem ligula at dui. Fusce rutrum ipsum vel commodo ullamcorper. Ut lacinia hendrerit lacus, ultrices accumsan metus placerat a. Morbi blandit dapibus auctor. Sed faucibus massa dolor. Phasellus at fringilla tortor. Proin ac sem molestie, vestibulum lacus ut, feugiat odio. Aenean ullamcorper orci non suscipit rutrum.

Cras urna nisl, ullamcorper vitae pulvinar et, elementum non mi. Fusce eu dui hendrerit, tincidunt metus volutpat, accumsan magna. In sed leo eget diam consequat iaculis nec at ligula. Integer ac sem odio. Etiam a arcu non justo pulvinar aliquet. Mauris scelerisque, justo sit amet vehicula aliquam, enim elit scelerisque magna, eu consectetur arcu tortor iaculis neque. Integer vitae ornare quam, id consequat dolor.

Suspendisse risus leo, hendrerit nec condimentum a, tempor ac sapien. Cras malesuada pulvinar eros non placerat. Mauris at mi semper, pretium mi non, consectetur velit. Pellentesque mi tortor, convallis vel velit interdum, posuere blandit justo. Etiam lobortis eros vitae dignissim lobortis. Nam risus libero, pellentesque non urna at, elementum venenatis sem. Nullam lobortis consectetur erat, a pellentesque magna dictum sit amet. Pellentesque rhoncus accumsan ipsum eu dapibus. Curabitur ornare suscipit maximus. Vestibulum et consectetur justo. Donec pretium diam sapien, non lobortis orci luctus id. Etiam consequat sit amet nisl vel sagittis. Mauris porttitor pellentesque ante, ac consequat turpis molestie sed. Fusce sit amet efficitur sem. Morbi gravida purus massa. Cras sed urna elit.

Curabitur sollicitudin mi in ipsum sollicitudin ornare. Sed id auctor libero. Vivamus in pretium lectus. Integer euismod, est a vestibulum tempor, tellus neque mattis nulla, sed laoreet nisl nisl nec metus. Sed mollis cursus massa vel mollis. Vivamus a ligula congue risus ultricies malesuada. Vivamus ac hendrerit orci. Donec velit quam, eleifend nec tellus ut, tristique vestibulum turpis. Donec quis interdum purus. Maecenas id quam turpis.

  Morbi venenatis arcu purus, eget consectetur risus commodo commodo. Integer semper dignissim diam at fringilla. In hac habitasse platea dictumst. Mauris lectus turpis, lacinia quis faucibus mollis, ultricies quis risus. Nullam ac nisi fermentum, lacinia lacus in, gravida risus. Nulla facilisi. Vestibulum ut justo a erat tempus dictum id non odio. Phasellus viverra ante sed condimentum pellentesque. Donec dui tellus, malesuada non sagittis sed, dignissim eu diam. Donec sodales nunc nec porttitor aliquam.</p>
          </div>
          <div>
            <div>
            <Card/>
            </div>
          <div class=" rounded-lg mx-auto mx-8 p-12 shadow-md">
            <p class="text-2xl">Records</p>
            <p>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, est ut egestas imperdiet, quam odio vestibulum neque, eget tincidunt lorem ligula at dui. Fusce rutrum ipsum vel commodo ullamcorper. Ut lacinia hendrerit lacus, ultrices accumsan metus placerat a. Morbi blandit dapibus auctor. Sed faucibus massa dolor. Phasellus at fringilla tortor. Proin ac sem molestie, vestibulum lacus ut, feugiat odio. Aenean ullamcorper orci non suscipit rutrum.

Cras urna nisl, ullamcorper vitae pulvinar et, elementum non mi. Fusce eu dui hendrerit, tincidunt metus volutpat, accumsan magna. In sed leo eget diam consequat iaculis nec at ligula. Integer ac sem odio. Etiam a arcu non justo pulvinar aliquet. Mauris scelerisque, justo sit amet vehicula aliquam, enim elit scelerisque magna, eu consectetur arcu tortor iaculis neque. Integer vitae ornare quam, id consequat dolor.

Suspendisse risus leo, hendrerit nec condimentum a, tempor ac sapien. Cras malesuada pulvinar eros non placerat. Mauris at mi semper, pretium mi non, consectetur velit. Pellentesque mi tortor, convallis vel velit interdum, posuere blandit justo. Etiam lobortis eros vitae dignissim lobortis. Nam risus libero, pellentesque non urna at, elementum venenatis sem. Nullam lobortis consectetur erat, a pellentesque magna dictum sit amet. Pellentesque rhoncus accumsan ipsum eu dapibus. Curabitur ornare suscipit maximus. Vestibulum et consectetur justo. Donec pretium diam sapien, non lobortis orci luctus id. Etiam consequat sit amet nisl vel sagittis. Mauris porttitor pellentesque ante, ac consequat turpis molestie sed. Fusce sit amet efficitur sem. Morbi gravida purus massa. Cras sed urna elit.

Curabitur sollicitudin mi in ipsum sollicitudin ornare. Sed id auctor libero. Vivamus in pretium lectus. Integer euismod, est a vestibulum tempor, tellus neque mattis nulla, sed laoreet nisl nisl nec metus. Sed mollis cursus massa vel mollis. Vivamus a ligula congue risus ultricies malesuada. Vivamus ac hendrerit orci. Donec velit quam, eleifend nec tellus ut, tristique vestibulum turpis. Donec quis interdum purus. Maecenas id quam turpis.

  Morbi venenatis arcu purus, eget consectetur risus commodo commodo. Integer semper dignissim diam at fringilla. In hac habitasse platea dictumst. Mauris lectus turpis, lacinia quis faucibus mollis, ultricies quis risus. Nullam ac nisi fermentum, lacinia lacus in, gravida risus. Nulla facilisi. Vestibulum ut justo a erat tempus dictum id non odio. Phasellus viverra ante sed condimentum pellentesque. Donec dui tellus, malesuada non sagittis sed, dignissim eu diam. Donec sodales nunc nec porttitor aliquam.</p>
          </div>
        </div>
  </div>
  </Layout>
)

}
