import Head from 'next/head'
import { NextSeo } from 'next-seo'
import Dashboard from '../components/layout/dashboard'
import GroupTable from '../components/ui/table/group'
import { Menu } from '@headlessui/react'
// import { Card, Tabs, Menu, Input, IconHome, Auth } from '@supabase/ui'

export default function App() {
  return (
    <Dashboard>
      <div class="px-4  mx-4 mt-0">
      <GroupTable/>
      </div>
    </Dashboard>
  )
}
