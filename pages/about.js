import Layout from '../components/layout'
import AboutCard from '../components/ui/about'
import Head from 'next/head'
import { NextSeo } from 'next-seo'
import { Menu } from '@headlessui/react'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
// import { Card, Tabs, Menu, Input, IconHome, Auth } from '@supabase/ui'

export default function About() {
  const [ session, loading ] = useSession()
  const router = useRouter()
  return (
    <Layout title="About Us" session={session} header={true}>
      <AboutCard img="/assets/undraw_Data_re_80ws.svg" title="Who we are">
          Hi there! Devisa is a project which intends to create the most substantial leap forward for life management solutions yet seen. We're building a next-generation platform to allow users to centrally manage their life data alongside a communiy of other like-minded users. We don't want to spoil too much just yet, but there's a lot in store to be excited about -- and we don't say that lightly. We'll be rolling out new windows into our development process as we come along, but if you'd like to reach us now, please <a class="font-bold text-green-500 dark:text-green-300" href="#">contact us</a>. We'll be seeing you around!
      </AboutCard>
    </Layout>
  )
}
