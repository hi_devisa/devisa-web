import Input from '../../components/ui/input/leftLabel'
import Gradient from '../../components/ui/button/gradient'
import ProductCard from '../../components/shop/productCard'
import Layout from '../../components/layout'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import * as fbq from '../../lib/fbPixel'

export default function Shop(item, value) {
  const [ session, loading ] = useSession()
  const router = useRouter()
  return (
    <>

      <Layout session={session} title="Shop" header={true}>
      <div class=" mb-8 grid grid-cols-3 gap-0">
        <ProductCard name="Devisa Stickers" price="$2" description="Just some silly Devisa stickers" img='stickers.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/ARRIJN2LETSGM7HB7RILM6E5"/>
        <ProductCard name="Devisa Dad hat" price="$16" description="A wavy Devisa dad hat" img='dadhat.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/IMGNSP5KQ3DBMOAXNWBLN7YW"/>
        <ProductCard name="Devisa Embroidered Beanie" price="$15" description="A out-of-this-world Devisa white beanie" img='beaniewhite.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/7F7FMPK5RCSJNLD4ZM3ESEMC"/>
      </div>
      <div class="mb-8 grid grid-cols-3 gap-0">
        <ProductCard name="Devisa Cuffed Beanie" price="$16" description="A wicked rad Devisa black beanie" img='beanieblack.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/CEWTFCTP2ZXB5NFOI3DIH5CL"/>
        <ProductCard name="Devisa Unisex Hoodie" price="$30" description="A simple Devisa hoodie!" img='hoodie.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/SZ3DFNRCRH2JUAC6MV2G5GRJ"/>
        <ProductCard name="Devisa Mug" price="$9-11" description="A super cool Devisa mug" img='mug.jpg' button="Buy now" url="https://checkout.square.site/merchant/MLAP029WZT51W/checkout/ORMQBOVHSE4QLYECRZFHGY7Q"/>
      </div>
        <div class="w-1/2 mx-auto">
          <div class="p-8">
        <div class="uppercase tracking-wide text-sm text-green-500 font-semibold">Get a gift card for someone!</div>
          <a href="https://squareup.com/gift/MLAP029WZT51W/order">
            <button type="submit" class="inline-flex justify-center mb-4 py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded text-white bg-green-500 hover:bg-green-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 transition duration-200 hover:shadow-md">
            Get a gift card
            </button>
          </a>
        </div>
      </div>
    <div class="w-1/2 mx-auto">
      <div class="p-8">
        <div class="uppercase tracking-wide text-sm text-green-500 font-semibold">Sign up for updates!</div>
        <div class=" mb-4 tracking-wide text-lg text-gray-900 font-medium">Get notified about new products and updates!</div>
          <div class="px-4 py-3 bg-gray-50 border border-gray-100 ring-1 ring-gray-200 shadow-sm text-right w-7/8 mx-auto sm:px-6 rounded">
        <div class="grid grid-cols-6 gap-4 min-w-full mb-8 mt-4 text-left">
          <div class="col-span-3 sm:col-span-3">
            <label for="first_name" class="block text-sm font-medium text-gray-700">Name</label>
            <input type="text" name="first_name" id="first_name" autocomplete="given-name" class="mt-1 focus:ring-green-500 focus:border-green-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Your name"/>
          </div>


          <div class="col-span-3 sm:col-span-3">
            <label for="last_name" class="block text-sm font-medium text-gray-700">Email</label>
            <input type="text" name="last_name" id="last_name" autocomplete="family-name" class="mt-1 focus:ring-green-500 focus:border-green-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="You@Example.com"/>
          </div>
        </div>
            <button type="submit" class="inline-flex justify-center py-2 px-4 mr-8 border border-transparent shadow-sm text-sm font-medium rounded text-white bg-gray-400 hover:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 transition duration-200 hover:shadow-md">
              About Us
            </button>
            <button type="submit" class="inline-flex justify-center mb-4 py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded text-white bg-green-500 hover:bg-green-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 transition duration-200 hover:shadow-md">
              Submit
            </button>
        </div>
        </div>
      </div>
      </Layout>
    </>
  )
}
