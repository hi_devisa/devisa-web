import Head from 'next/head'

export default function Logout() {
  return (
    <div class="body-bg min-h-screen pt-12 md:pt-20 pb-6 px-2 md:px-0 font-sans bg-green-500">
    <Head>
      <title>Logout - Devisa</title>
    </Head>
    <header class="max-w-lg mx-auto">
            <h1 class="font-thin  text-6xl font-bold text-white text-center">You have been logged out.</h1>
          <br/><br/>
            <h1 class="text-6xl font-bold text-green-600 text-center">...</h1>
          <br/><br/>
            <h3 class="text-5xl font-semibold text-white text-center">See you soon!</h3>
            <h1 class="text-6xl font-bold text-green-600 text-center">...</h1>
          <br/><br/>
            <h4 class="text-4xl font-semibold text-white text-center"><a href="/">Click here to go back home!</a></h4>
    </header>
    </div>

          )
          }
