import * as fbq from '../../../../lib/fbPixel'
import { useEffect } from 'react'


  // FB PIXEL EVENT BODY:
  // {
  //    name: string,
  //    price: double,
  //    checkout: string,
  // }


const fbPixelBuyNowClickedEvent = (itemName, itemPrice) => {
  fbq.event('Buy Now Clicked', { itemName: itemName, itemPrice: itemPrice })
}

export default (req, res) => {
  if (req.method === 'POST') {
    // const { item } = req.query <- for [item] directory
    fbPixelBuyNowClickedEvent(req.body.name, req.body.price)
  } else if (req.method == 'GET') {
    res.statusCode = 400
    res.end(`hello`)
  } else {
    res.statusCode = 400
  }
}
