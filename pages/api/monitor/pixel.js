
import React, { useEffect } from "react";

export const FB_PIXEL_ID = process.env.FB_PIXEL_ID

export const pageview = () => {
  useEffect(function mount() {
    window.fbq('track', 'PageView')
  })
}

// https://developers.facebook.com/docs/facebook-pixel/advanced/
export const event = (name, options = {}) => {
  useEffect(function mount() {
    window.fbq('track', name, options)
  })
}


