import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    prisma.link.findMany()
        .catch(e => res.status(400).send("Could not find link" + e))
        .then(result => res.status(200).send(result));
  })
  .post(async (req, res) => {
    // ADD AN EXISTING ITEM TO AN EXISTING RECORD
    await prisma.link.create({
      data: {
        name: req.body.name.toString(),
        value: req.body.value.toString(),
      }
      })
        .catch(e => res.status(400).send("Could not create link" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    // REMOVE A TOPIC FROM A RECORD
    const { recordId, topicId } = req.query
    await prisma.recordTopic.delete({
      where: {
        recordId: parseInt(recordId),
        topicId: parseInt(topicId)
      }
  })
      .then(r => res.status(200).send(r))
  })

export default handler;
