import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.record.findMany()
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get records: " + id + "; " + err));
  })
  .post(async (req, res) => {
    await prisma.record.create({
      data: {
        name: req.body.name,
        user: {
          connect: {
            id: parseInt(req.body.userId)
          }
        },
        private: req.body.private? req.body.private : true,
        status: req.body.status? req.body.status.toString() : "ACTIVE",
      }})
        .catch(e => res.status(400).send("Could not create profile" + e))
        .then(result => res.status(200).send(result));
  })

export default handler;
