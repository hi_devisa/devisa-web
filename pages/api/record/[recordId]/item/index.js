

import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    // GET ALL ITEMS FROM A RECORD
    const { recordId } = req.query
    await prisma.record.findFirst({
      where: {
        id: parseInt(recordId)
      },
      include: {
        items: {
          include: {
            item: true
          }
        }
      }
    })
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get items: ; " + err));
  })
  .post(async (req, res) => {
    // ADD A NEW ITEM TO AN EXISTING RECORD
    const { recordId } = req.query
    const itemId = await prisma.item.create({
      data: {
        name: req.body.name.toString(),
        userId: parseInt(req.body.userId),
      },
      select: {
        id: true
      }
    })
    await prisma.recordItem.create({
      data: {
        recordId: parseInt(recordId),
        itemId: parseInt(itemId.id)
      }
    })
        .catch(e => res.status(400).send("Could not create item" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    await prisma.recordItem.delete({
      where: {
        recordId: parseInt(recordId),
        itemId: parseInt(req.body.itemId)
      }
  })})

export default handler;
