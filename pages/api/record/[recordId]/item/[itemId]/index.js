import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .post(async (req, res) => {
    // ADD AN EXISTING ITEM TO AN EXISTING RECORD
    const { recordId, itemId } = req.query
    await prisma.recordItem.create({
      data: {
        record: {
          connect: {
            id: parseInt(recordId)
          }
        },
        item: {
          connect: {
            id: parseInt(itemId)
          }
        }
      }
      })
        .catch(e => res.status(400).send("Could not create item" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    // REMOVE AN ITEM FROM A RECORD
    const { recordId, itemId } = req.query
    await prisma.recordItem.delete({
      where: {
        recordId: parseInt(recordId),
        itemId: parseInt(itemId)
      }
  })
      .then(r => res.status(200).send(r))
  })

export default handler;
