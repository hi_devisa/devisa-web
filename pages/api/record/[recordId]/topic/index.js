

import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    // GET ALL ITEMS FROM A RECORD
    const { recordId } = req.query
    await prisma.record.findFirst({
      where: {
        id: parseInt(recordId)
      },
      include: {
        topics: {
          include: {
            topic: true
          }
        }
      }
    })
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get items: ; " + err));
  })
  .post(async (req, res) => {
    // ADD A NEW TOPIC TO AN EXISTING RECORD
    const { recordId } = req.query
    const topicId = await prisma.topic.create({
      data: {
        name: req.body.name.toString(),
      },
      select: {
        id: true
      }
      });
    await prisma.recordTopic.create({
      data: {
        topicId: parseInt(topicId.id),
        recordId: parseInt(recordId),
        primary: req.body.primary? req.body.primary : false,
      }
    })
        .catch(e => res.status(400).send("Could not create item" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    await prisma.recordTopic.delete({
      where: {
        recordId: parseInt(recordId),
        topicId: parseInt(req.body.topicId)
      }
  })})

export default handler;
