
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .post(async (req, res) => {
    // ADD AN EXISTING ITEM TO AN EXISTING RECORD
    const { recordId, topicId } = req.query
    await prisma.recordTopic.create({
      data: {
        record: {
          connect: {
            id: parseInt(recordId)
          }
        },
        topic: {
          connect: {
            id: parseInt(topicId)
          }
        }
      }
      })
        .catch(e => res.status(400).send("Could not create item" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    // REMOVE A TOPIC FROM A RECORD
    const { recordId, topicId } = req.query
    await prisma.recordTopic.delete({
      where: {
        recordId: parseInt(recordId),
        topicId: parseInt(topicId)
      }
  })
      .then(r => res.status(200).send(r))
  })

export default handler;
