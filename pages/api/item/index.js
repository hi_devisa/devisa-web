import { PrismaClient } from '@prisma/client'
import { getSession } from 'next-auth/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.item.findMany()
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get items: ; " + err));
  })
  .post(async (req, res) => {
    await prisma.item.create({
      data: {
        name: req.body.name.toString(),
        user: {
          connect: {
            id: parseInt(req.body.userId),
          }
        }
      }})
        .catch(e => res.status(400).send("Could not create item" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    const session = await getSession({ req });
    if ( session.accessToken? session.accessToken != null : false ) {
      await prisma.item.delete({
        where: {
          id: parseInt(req.body.id),
        }
      })
        .catch(e => console.error(e))
        .then(r => res.send(r));
    } else {
      res.send("Not authorized");
    }
  })

export default handler;
