import NextAuth from 'next-auth'
import  useSWR  from 'swr'
import axios from 'axios'
import Providers from 'next-auth/providers'
import { PrismaClient } from '@prisma/client'
import Adapters from "next-auth/adapters";
// const prisma = new PrismaClient()
const GOOGLE_AUTHORIZATION_URL =
  "https://accounts.google.com/o/oauth2/v2/auth?" +
  new URLSearchParams({
    prompt: "consent",
    access_type: "offline",
    response_type: "code",
  });

async function refreshAccessToken(token) {
  try {
    const url =
      "https://oauth2.googleapis.com/token?" +
      new URLSearchParams({
        client_id: process.env.GOOGLE_CLIENT_ID,
        client_secret: process.env.GOOGLE_CLIENT_SECRET,
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      });

    const response = await fetch(url, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
    });

    const refreshedTokens = await response.json();

    if (!response.ok) {
      throw refreshedTokens;
    }

    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      accessTokenExpires: Date.now() + refreshedTokens.expires_in * 1000,
      refreshToken: refreshedTokens.refresh_token ?? token.refreshToken, // Fall back to old refresh token
    };
  } catch (error) {
    console.log(error);

    return {
      ...token,
      error: "RefreshAccessTokenError",
    };
  }
}

const prisma = new PrismaClient()

const providers = [
  Providers.GitHub({
      clientId: process.env.GITHUB_CLIENT_ID,
      clientSecret: process.env.GITHUB_CLIENT_SECRET,
  }),
  Providers.Google({
    clientId: process.env.GOOGLE_OAUTH_CLIENT_ID,
    clientSecret: process.env.GOOGLE_OAUTH_CLIENT_SECRET,
  }),
  Providers.LinkedIn({
    clientId: process.env.LINKEDIN_CLIENT_ID,
    clientSecret: process.env.LINKEDIN_SECRET
  }),
  Providers.Facebook({
    clientId: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_SECRET
  }),
  Providers.GitLab({
    clientId: process.env.GITLAB_CLIENT_ID,
    clientSecret: process.env.GITLAB_CLIENT_SECRET
  }),
  Providers.Cognito({
    clientId: process.env.COGNITO_CLIENT_ID,
    clientSecret: process.env.COGNITO_CLIENT_SECRET,
    domain: process.env.COGNITO_DOMAIN,
  }),
  Providers.Email({}),
  Providers.Credentials({
    name: "Credentials",
    authorize: async (credentials) => {
      const {res, err} = await axios.get('/api/credentials/verify', {
        data: {
          username: credentials.username,
          password: credentials.password,
        }
      })
        .catch(e => console.error("Could not login user " + e))
        .then(res => {
          if (res) {
            return res
          } else {
            return null
          }
        }
      )
    },
    credentials: {
      username: { label: "Username", type: "text" },
      password: {  label: "Password", type: "password" }
    },
  }),
]

// const callbacks = {}
// callbacks.signIn = async function signIn(user, account, metadata) {
//   const user = await prisma.user.findUnique({
//     where: {
//       email: account.email.toString(),
//     }
//   })
//     .catch(e => console.error(e))
//     .then(r => r);
//   const credentials = await prisma.credentials.findUnique({
//     where: {
//       userId: parseInt(user.id)
//     }
//   })
//     .catch(e => {
//       console.error(e);
//       return false;
//     })
//     .then(r => {
//       return true;
//     });
// }

// const callbacks = {
//   // Getting the JWT token from API response
//   async jwt(token, user) {
//     if (user) {
//       token.accessToken = user.token
//     }

//     return token
//   },

//   async session(session, token) {
//     session.accessToken = token.accessToken
//     return session
//   }
// }

// const session = {
//   jwt: true
// }

const adapter = Adapters.Default({
  type: "postgres",
  url: process.env.DB_URL,
  synchronize: true,
})

const callbacks = {
  session: async (session, user) => {
      session.jwt = user.jwt;
      session.id = user.id;
      return Promise.resolve(session);
  },
  jwt: async (token, user, account) => {
      const isSignIn = user ? true : false;
      if (isSignIn) {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_API_URL}/auth/${account.provider}/callback?access_token=${account?.accessToken}`
        );
        const data = await response.json();
        token.jwt = data.jwt;
        token.id = data.user.id;
      }
      return Promise.resolve(token);
  },
}

// const adapter = Adapters.Prisma.Adapter({ prisma })



// const callbacks = {}

// callbacks.signIn = async function signIn(user, account, metadata) {
//   if (account.provider == 'github') {
//     const githubUser = {
//       id: metadata.id,
//       login: metadata.login,
//       name: metadata.name,
//       avatar: user.image
//     }
//     user.accessToken = await getTokenFromYourAPIServer('github', githubUser)
//     return true
//   } else if (account.provider == 'google') {
//     const googleUser = {
//       id: metadata.id,
//       login: metadata.login,
//       name: metadata.name,
//       avatar: user.image,
//     }
//     user.accessToken = await getTokenFromYourAPIServer('google', googleUser)
//     return true
//   }
//   return false
// }

// callbacks.jwt = async function jwt(token, user) {
//   if (user) {
//     token = { accessToken: user.accessToken }
//   }
//   return token
// }

// callbacks.session = async function jwt(session, token) {
//   session.accessToken = token.accessToken
//   return token
// }

const pages = {
    signIn: '/login',
    signOut: '/logout',
    newUser: '/auth/register/1',
    // error: '/auth/error', // Error code passed in query string as ?error=
    // verifyRequest: '/auth/verify-request', // (used for check email message)
    // newUser: null // If set, new users will be directed here on first sign in
  }

const prismaAd = Adapters.Prisma.Adapter({
  prisma ,
  // modelMapping: {
  //     User: 'users',
  //     Account: 'accounts',
  //     Session: 'sessions',
  //     VerificationRequest: 'verification_requests'
  //   }
})

const options = {
  providers: providers,
  adapter: prismaAd,
  // session: session,
  // callbacks: callbacks,
  // callbacks,
  pages: pages,
  debug: true,
  site: process.env.NEXTAUTH_URL,
  database: process.env.NEXT_PUBLIC_DATABASE_URL,
}

export default (req, res) => NextAuth(req, res, options)

