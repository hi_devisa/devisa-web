
import { getSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { userId } = req.query
    await prisma.record.findMany({
      where: {
        userId: parseInt(userId),
      }
    })
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get records by " + id + " : " +  err));
  })
  .post(async (req, res) => {
    const { userId } = req.query
    await prisma.record.create({
      data: {
        name: req.body.record,
        private: req.body.private ? req.body.private : true,
        status: req.body.status ? req.body.status : "ACTIVE",
        user: {
          connect: {
            id: parseInt(userId)
          }
        }
      }})
        .catch(e => res.status(400).send("Could not create record" + e))
        .then(result => res.status(200).send(result));
  })

export default handler;
