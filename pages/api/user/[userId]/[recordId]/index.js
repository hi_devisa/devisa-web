
import { getSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { userId, recordId } = req.query
    await prisma.record.findFirst({
      where: {
        id: parseInt(recordId)
      }
    })
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get record: " + record + "by " + id + " : " +  err));
  })
  .post(async (req, res) => {
    const { userId, recordId } = req.query
    await prisma.record.update({
      where: {
        id: parseInt(recordId)
      },
      data: {
        name: req.body.name.toString(),
        private: req.body.private ? req.body.private : true,
        status: req.body.status ? req.body.status : "ACTIVE",
        user: {
          connect: {
            id: parseInt(userId)
          }
        }
      }})
        .catch(e => res.status(400).send("Could not create record" + e))
        .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    const { id, record } = req.query
    await prisma.record.delete({
      where: {
        userId: parseInt(id),
        name: record.toString()
      }
  })
        .catch(e => res.status(400).send("Could not delete record" + e))
        .then(result => res.status(200).send(result));
  })

export default handler;
