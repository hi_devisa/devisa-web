import { useSession } from 'next-auth/client'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { userId } = req.query
    await prisma.user.findFirst({
      where: {
        id: {
          equals: parseInt(userId)
        }
      }
    })
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get user with id " + id + "; " + err));
  })
  .post(async (req, res) => {
    const { userId } = req.query
    await prisma.user.create({
      data: {
        name: body.name ? body.name : "",
      },
    })
      .catch(e => res.status(400).send("Could not create profile" + e))
      .then(result => res.status(200).send(result));
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
