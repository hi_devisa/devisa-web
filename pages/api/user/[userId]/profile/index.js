import { getSession } from 'next-auth/client'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { userId } = req.query
    await prisma.profile.findFirst({
      where: {
        userId: {
          equals: parseInt(userId)
        }
      }
    })
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get profile of user with id " + id + "; " + err));
  })
  .post(async (req, res) => {
    const { userId } = req.query
    await prisma.profile.create({
      data: {
        birthday: req.body.birthday ? req.body.birthday : null,
        country: req.body.country,
        website: req.body.website? req.body.website : null,
        role: req.body.role? req.body.role : "USER",
        user: {
          connect: {
            id: parseInt(userId),
          },
        },
        bio: req.body.bio? req.body.bio : null,
      },
    })
      .catch(e => res.status(400).send("Could not create profile" + e))
      .then(result => res.status(200).send(result));
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .delete(async (req, res) => {
    const { userId } = req.query
    await prisma.profile.delete({
      where: {
        userId: parseInt(userId)
      }
    })
      .catch(e => res.status(400).send("Could not create profile" + e))
      .then(result => res.status(200).send(result));
  });

export default handler;
