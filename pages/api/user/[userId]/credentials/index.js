import { getSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { userId } = req.query
    await prisma.credentials.findFirst({
      where: {
        userId: parseInt(userId)
      }
    })
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get credentials of user with id " + id + "; " + err));
  })
  .post(async (req, res) => {
    const { userId } = req.query
    const passwordHash = await genSalt(10, (err, salt) => {
      hash(req.body.password, salt, async (err, hash) => {
        await prisma.credentials.create({
          data: {
            username: req.body.username,
            password: hash,
            user: {
              connect: {
                id: parseInt(userId),
              },
            },
          },
        })
          .catch(e => res.status(400).send("Could not create profile" + e))
          .then(result => res.status(200).send(result));
      })
    })
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
