
import { getSession } from 'next-auth/client'
import { compare } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { id } = req.query
    const creds = await prisma.credentials.findFirst({
      select: {
        username: true,
        password: true,
      },
      where: {
        userId: parseInt(id)
      }
    })
      .then(resp => resp)
      .catch(err => resp.status(400).end("Could not get credentials of user with id " + id + "; " + err));
    if ( creds.username == req.body.username ) {
      await compare(req.body.password, creds.password)
        .then(match => res.status(200).send(match))
        .catch(e => console.error("Could not compare hashed password, "+e));
    } else {
      res.send(false)
    }
  })

export default handler;
