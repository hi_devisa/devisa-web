//      /user/credentials
import { getSession } from 'next-auth/client'
import nc from "next-connect";
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

// STEP 1 of signup -> credentials
async function createCredentials(userId, username, password) {
  const cred = {
    userId: userId,
    username: username,
    password: password,
  };
  const res = await axios.fetch("http://api.devisa.io/user/credentials", {
    method: "POST",
    data: JSON.stringify(cred),
  })
    .catch(e => console.error(e))
    .then(r => r);
  return res;
}

const handler = nc()
  // GET /user Get all creds
  .get(async (req, res) => {
    await axios.fetch("http://api.devisa.io/user/credentials", {
      method: "GET"
    })
      .catch(e => console.error(e))
      .then(r => res.json(r));
  })
  // POST /user Create new user (minimum name and email)
  .post(async (req, res) => {
    await createCredentials(username, password)
      .catch(e => console.error(e))
      .then(r => res.json(r));
  })