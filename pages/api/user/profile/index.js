//      /user/credentials
import { getSession } from 'next-auth/client'
import nc from "next-connect";
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

// STEP 1 of signup -> credentials
async function createProfile(userId, bio, gender, age, country) {
  const prof = {
    userId: userId,
    bio: bio,
    gender: gender,
    birthday: birthday,
    country: country,
  };
  const res = await axios.fetch("http://api.devisa.io/user/profile", {
    method: "POST",
    data: JSON.stringify(prof),
  })
    .catch(e => console.error(e))
    .then(r => r);
  return res;
}

const handler = nc()
  // GET /user Get all creds
  .get(async (req, res) => {
  })
  .post(async (req, res) => {
    await prisma.profile.create({
      data: {
        userId: parseInt(userId),
        birthday: Date.parse(birthday),
        country: country.toString(),
        gender: gender.toString(),
        bio: bio.toString(),
      }
    })
      .catch(e => console.error(e))
      .then(r => {
        console.log(r);
        res.json(r);
      });
  })
  // POST /user Create new user (minimum name and email)
  .post(async (req, res) => {
    await createCredentials(username, password)
      .catch(e => console.error(e))
      .then(r => res.json(r));
  })