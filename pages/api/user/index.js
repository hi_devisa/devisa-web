import { getSession } from 'next-auth/client'
import nc from "next-connect";
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

// STEP 1 of signup -> credentials
async function createUser(email, name) {
  const user = {
    email: email,
    name: name,
  };
  const res = await axios.fetch("http://api.devisa.io/user", {
    method: "POST",
    data: JSON.stringify(user),
  })
    .catch(e => console.error(e));
  return res;
}
const handler = nc()
  // GET /user Get all users
  .get(async (req, res) => {
    await axios.fetch("http://api.devisa.io/user", {
      method: "GET"
    })
      .catch(e => console.error(e))
      .then(r => res.json(r));

  })
  // POST /user Create new user (minimum name and email)
  .post(async (req, res) => {
    const user = await createUser(req.body.email, req.body.name)
      .catch(e => console.error(e));
    res.json(user);
  })


