import { useSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import axios from 'axios'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
        await prisma.user.findFirst({
          where: {
            email: req.body.email.toString()
          }
        })
          .then(resp => res.send(resp))
      })
export default handler;
