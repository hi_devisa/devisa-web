

import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    prisma.session.findMany()
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get sessions; " + err));
  })
  .post((req, res) => {
    res.json({ hello: "world" });
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
