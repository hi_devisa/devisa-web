
import { getSession } from 'next-auth/client'
import { compare } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const creds = await prisma.credentials.findFirst({
      select: {
        username: true,
        password: true,
      },
      where: {
        username: req.body.username
      }
    })
      .then(resp => resp)
      .catch(err => resp.status(400).end("Could not get credentials of user with id " + id + "; " + err));
    await compare(req.body.password, creds.password)
        .then(match => res.status(200).send(match))
        .catch(e => console.error("Could not compare hashed password, "+e));
    }
  )

export default handler;
