import { getSession } from 'next-auth/client'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.credentials.findMany()
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get credentials of user with id ; " + err));
  })
  .post(async (req, res) => {
    // CREATE NEW CREDENTIALS -> CREATE NEW USER -> CREATE NEW PROFILE?
    // Or CREATE NEW USER (with email -> verify email) -> CREATE NEW CREDENTIALS -> CREATE NEW PROFILE?
    const passwordHash = await genSalt(10, (err, salt) => {
      hash(req.body.password, salt, async (err, hash) => {
        await prisma.credentials.create({
          data: {
            username: req.body.username,
            password: passwordHash,
          },
        })
      })
    })
  })
  .delete(async (req, res) => {
    await prisma.credentials.delete({
      where: {
        username: req.body.username.toString(),
      }
    })
      .then(r => res.json(r))
      .catch(e => console.error(e))
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
