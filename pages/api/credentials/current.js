
import { useSession } from 'next-auth/client'
import { compare } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .post(async (req, res) => {
    const [ session, loading ] = useSession()
    const email = session.email
    const user = await prisma.credentials.findFirst({
      select: {
        id
      },
      where: {
        email: email
      }
    })
      .then(resp => resp);
    const passwordHash = await genSalt(10, (err, salt) => {
      hash(req.body.password, salt, async (err, hash) => {
        await prisma.credentials.create({
          data: {
            username: req.body.username,
            password: passwordHash,
            userId: parseInt(user.id)
          },
        })
      })
    })
      .then(resp => res.send(resp))
  })

export default handler;
