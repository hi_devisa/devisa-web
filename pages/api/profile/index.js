
import { getSession } from 'next-auth/client'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.profile.findMany()
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get profile of user with id " + id + "; " + err));
  })

