
import { getSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.topic.findMany()
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get topics: " + id + "; " + err));
  })
  .post(async (req, res) => {
    const { topic } = req.body.topic
    await prisma.topic.create({
      data: {
        name: topic.toString()
      }})
        .catch(e => res.status(400).send("Could not create profile" + e))
        .then(result => res.status(200).send(result));
  })

export default handler;
