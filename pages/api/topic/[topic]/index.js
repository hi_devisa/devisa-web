import { getSession } from 'next-auth/client'
import { genSalt, hash } from 'bcrypt'
import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    const { topic } = req.query
    await prisma.topic.findFirst({
      where: {
        name: toString(topic)
      }
    })
      .then(resp => res.json(resp))
      .catch(err => resp.end("Could not get topic : " + topic + "; " + err));
  })
  .post(async (req, res) => {
    const { topic } = req.query
    await prisma.topic.create({
      data: {
        name: topic.toString()
      }
    })
      .catch(e => res.status(400).send("Could not create profile" + e))
      .then(result => res.status(200).send(result));
  })
  .delete(async (req, res) => {
    const { topic } = req.query
    await prisma.topic.delete({
      where: {
        name: topic.toString()
      }
    })
      .catch(e => res.status(400).send("Could not create profile" + e))
      .then(result => res.status(200).send(result));
  })

export default handler;
