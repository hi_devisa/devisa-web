import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    await prisma.group.findMany()
      .then(resp => res.json(resp))
      .catch(err => res.end("Could not get groups: ; " + err));
  })
  .post(async (req, res) => {
    await prisma.group.create({
      data: {
        name: req.body.name.toString(),
        members: {
          create: {
            userId: parseInt(req.body.userId),
            groupRole: req.body.groupRole? req.body.groupRole.toString() : "ADMIN",
          }
        }
      }})
        .catch(e => res.status(400).send("Could not create profile" + e))
        .then(result => res.status(200).send(result));
  })

export default handler;
