import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .delete(async (req, res) => {
    const { groupId } = req.query
    await prisma.group.delete({
      where: {
        id: parseInt(groupId)
      }
    })
      .then(r => res.send(r))
      .catch(e => console.error(e));
  })
  .get(async (req, res) => {
    // GET GROUPS's MEMBERS + TOPICS + Records
    const { groupId } = req.query
    prisma.group.findFirst({
      where: {
        id: parseInt(groupId)
      },
      include: {
        members: {
          include: {
            user: true
          }
        },
        topics: {
          include: {
            topic: true
          }
        },
        records: {
          include: {
            record: {
              include: {
                items: true,
                topics: true
              }
            }
          }
        }
      }
    })
        .catch(e => res.status(400).send("Could not find groups topics/members" + e))
        .then(result => res.status(200).send(result));
  })
  .post(async (req, res) => {
    // UPDATE A GROUP
    const { groupId } = req.query
    if (req.body.name) {
      const topic = await prisma.topic.create({
        data: {
          name: req.body.name.toString()
        },
        select: {
          id: true
        }
      })
      await prisma.groupTopic.create({
        data: {
          group: {
            connect: {
              id: parseInt(groupId)
            }
          },
          topic: {
            connect: {
              id: parseInt(topic.id),
            }
          },
          primary: req.body.primary ? req.body.primary : false,
        }
        })
          .catch(e => res.status(400).send("Could not add new topic to group" + e))
          .then(result => res.status(200).send(result));

    } else {
      await prisma.groupTopic.create({
        data: {
          group: {
            connect: {
              id: parseInt(groupId)
            }
          },
          topic: {
            connect: {
              id: parseInt(req.body.topicId),
            }
          },
          primary: req.body.primary ? req.body.primary : false,
        }
        })
          .catch(e => res.status(400).send("Could not add topic to group" + e))
          .then(result => res.status(200).send(result));

    }
  })
export default handler;
