import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    // GET GROUPS's MEMBERS
    const { groupId } = req.query
    prisma.group.findFirst({
      where: {
        id: parseInt(groupId)
      },
      include: {
        members: {
          include: {
            user: true
          }
        }
      }
    })
        .catch(e => res.status(400).send("Could not find groups members" + e))
        .then(result => res.status(200).send(result));
  })
  .post(async (req, res) => {
    // ADD AN EXISTING USER TO A GROUP
    const { groupId } = req.query
    await prisma.groupMembership.create({
      data: {
        group: {
          connect: {
            id: parseInt(groupId)
          }
        },
        user: {
          connect: {
            id: parseInt(req.body.userId)
          }
        },
        groupRole: req.body.role? req.body.role : "MEMBER"
      }
      })
        .catch(e => res.status(400).send("Could not add user to group" + e))
        .then(result => res.status(200).send(result));
  })
export default handler;
