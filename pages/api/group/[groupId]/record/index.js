import { PrismaClient } from '@prisma/client'
import nc from "next-connect";

const prisma = new PrismaClient()
const handler = nc()
  .get(async (req, res) => {
    // GET GROUP's RECORDS
    const { groupId } = req.query
    prisma.group.findFirst({
      where: {
        id: parseInt(groupId)
      },
      include: {
        records: {
          include: {
            record: true
          }
        }
      }
    })
        .catch(e => res.status(400).send("Could not find groups records" + e))
        .then(result => res.status(200).send(result));
  })
  .post(async (req, res) => {
    // ADD AN EXISTING RECORD TO A GROUP
    const { groupId } = req.query
    await prisma.groupRecord.create({
      data: {
        groupId: parseInt(groupId),
        recordId: parseInt(req.body.recordId),
      }
      })
        .catch(e => res.status(400).send("Could not add record to group" + e))
        .then(result => res.status(200).send(result));
  })
export default handler;
