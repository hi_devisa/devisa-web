
import DashLayout from '../../components/layout/dashboard'

export default function Dashboard() {
  const userDashboard = (
    <DashLayout
      title = "Support"
      mainBtn = "Contact"
      secondaryBtn = "FAQ"
      breadcrumb = "Home / Account / Support"
      tab1 = "F.A.Q."
      tab2 = "Support Center"
      tab3 = "Contact Us"
      child1 = { <p>Hello</p> }
      child2 = { <p>There</p> }
      child3 = { <p>friend</p> }
      >
        <p class="text-xl">Support</p>
    </DashLayout>
  )
    return userDashboard
}
