import Features from '../../components/ui/features/features'
import FeatureCard from '../../components/ui/features/featureCard'
import Layout from '../../components/layout'
import {useSession} from 'next-auth/client'

export default function WebServices() {
  const [ session ] = useSession()
  return (
    <>
      <Layout session={session} title="Services">
        <p class="mt-20 text-2xl text-center">
            Web Services
        </p>
        <div class="mb-28">
          <Features/>
        </div>
      </Layout>
    </>
  )
}
