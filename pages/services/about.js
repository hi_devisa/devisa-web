import Features from '../../components/ui/features/features'
import Tabs from '../../components/ui/tabs'
import Layout from '../../components/layout'
import ListingCard from '../../components/ui/card/listing'
import {useSession} from 'next-auth/client'

export default function AboutServices() {
  const [ session ] = useSession()
  return (
    <>
      <Layout session={session} title="Services">
        <Tabs/>
        <ListingCard/>
        <p class="mt-20 text-2xl text-center">
            About Our Services
        </p>
        <div class="mb-28">
          <Features/>
        </div>
      </Layout>
    </>
  )
}
