import Features from '../../components/ui/features/features'
import { useSession } from 'next-auth/client'
import FeatureCard from '../../components/ui/features/featureCard'
import Layout from '../../components/layout'

export default function Services() {
  const [ session, loading ] = useSession()
  return (
    <>
      <Layout session={session} title="Services" header={ true }>
        <div class=" w-3/4 mx-auto justify-center">
          <FeatureCard
            btnMain="Book an Appointment"
            btnSecondary="Learn more"
            hrefMain="https://squareup.com/appointments/book/leu3sq1yyzt3e7/LWHPD62MTHNTQ/start"
            hrefSecondary="/services/web"
            img="/assets/undraw_All_the_data_re_hh4w.svg">
                  Looking to make your business idea into something concrete? Put some ground beneath your feet for a passion project? Modernize your online web presence, but perhaps don't know where to start, or are naturally apprehensive about the overwhelming amount of tech you or your company has to deal with? Reach out to us -- we can handle it quickly, painlessly, and for ultra-affordable prices.
          </FeatureCard>
        </div>
      </Layout>
    </>
  )
}
