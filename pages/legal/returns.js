


import Head from 'next/head'
import Layout from '../../components/layout'
import {useSession} from 'next-auth/client'
export default function Privacy() {
  const [ session ] = useSession()
  return (
    <>
      <Head>
        <title>Returns Policy - Devisa</title>
      </Head>
      <Layout session={session} title="Services">
    <h1> Returns Policy </h1>
      </Layout>
    </>
  )
}
