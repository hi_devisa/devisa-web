import Layout from '../../components/layout'
import Head from 'next/head'
import {useSession} from 'next-auth/client'

export default function Tos() {
  const [ session ] = useSession()
  return (
    <>
      <Head>
        <title>Terms of Service - Devisa</title>
      </Head>
      <Layout session={session} title="Services">
    <h1> Terms of service </h1>

       <h2>Terms of Service</h2>
       <p>We have the right to change these Terms of Use at any time. It is your responsibility to check this page and keep yourself updated with the latest edition of Devisa LLC's Terms of Use.</p>
       <p>Devisa LLC does its best to keep the information on this website current and complete, however, we do not guarantee this and we can't be held responsible if this is not so.</p>

       <h4>Account terms</h4>
        <p>You are responsible for maintaining the security of your account and password. Devisa LLC cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.</p>
        <p>You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.</p>
        <p>You represent and warrant that all information that you provide as part of the registration process or in association with your user account is true, accurate, current and complete.</p>
        <p>Your login may only be used by one person – a single login shared by multiple people is not permitted. You may create separate logins for as many people as you'd like.</p>

        <h4>Ownership and intellectual property</h4>
        <p>The content of this website and the services it offers are owned by Devisa LLC and may be protected by intellectual property laws.</p>
        <p>Devisa LLC grants you a limited, revocable, non-exclusive, non-transferable license to use our site and our services for your own individual, enterprise, and limited commercial use subject to the guides in this Terms of Use.</p>
        <p>You agree not to resell the services we offer. You may not modify, reverse engineer, decompile or disassemble our site or our services. You may not copy, adapt, alter, modify, translate, or create derivative works of our site or our services without written authorization of Devisa LLC.</p>
        <p>If you violate any of these Terms, your license will be suspended automatically and your access to our services will be suspended.</p>

        <h4>Use of the website and its products</h4>
        <p>If you accept these Terms of Use and you set up an account with us, you are welcome to use our services within the guidelines of these Terms of Use. Any intellectual property rights over our services are owned by Devisa LLC.</p>
        <p>All our services are provided on an "as is" basis and we will not be liable for any losses or damages arising from the use of our services</p>
        <p>We have the right to discontinue any of our services or the website with or without a prior notice.</p>

        <h4>Property rights</h4>
        <p>All trademarks, trade names, service marks and logos on this website are proprietary to Devisa LLC. Your use of any marks on the website and service in any manner other than as authorized in these Terms and Conditions, or as authorized in writing by Devisa LLC, is strictly prohibited.</p>

        <h4>Prohibited use of this website</h4>
        <p>Using our services you agree that</p>
        <ul>
            <li>You are of the age of majority or older in the jurisdiction in which you reside;</li>
            <li>You will not engage in any activities that may infringe the rights of Devisa LLC or any third party;</li>
            <li>You will not modify, adapt or hack this website and its services;</li>
            <li>You will not modify another website so as to falsely imply that it is associated with our services or our company;</li>
            <li>You will not act against these Terms of Use;</li>
            <li>You will not use our services in any racially, sexually or otherwise offensive way;</li>
            <li>You will not use any automated data gathering methods to source data from this website;</li>
            <li>You will not use any technologies to manipulate or display (e.g. within an iframe) the content of this website;</li>
        </ul>
        <p>In case you breach any of the provisions of these Terms of Use we have the right to suspend your account and seek legal remedies.</p>

        <h4>Termination of use</h4>
        <p>If we find that you breach any of the provisions of these Terms of Use, we are going to suspend your account, without prior notice.</p>

        <h4>Right to monitor</h4>
        <p>We have the right, but not the obligation, to monitor User Content posted or uploaded to this website to determine compliance with these Terms and any operating rules established by us and to satisfy any law, regulation or authorized government request.</p>

        <h4>Third party vendors</h4>
        <p>You understand that we use third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run the Service</p>

        <h4>Links to third party websites</h4>
        <p>We may publish links to third party websites, but this does not mean that we endorse or provide warranty over their services. Any publication of links to third party websites is for your convenience only and their services and/or products should be used and/or purchased at your own discretion.</p>

        <h4>Warranties and disclaimers</h4>
        <p>The use of this website is at your own discretion. All our services and products are provided on an "as is" "as available" and "with all faults" basis and Devisa LLC expressly disclaims any warranties (expressed or implied) over this website's content and functions.</p>
        <p>Should any part of this website or its services prove defective it is you that bears the full consequences and costs associated with that defect.</p>
        <p>Devisa LLC provide no warranty that:</p>
        <ul>
            <li>The services provided on this website are going to meet your requirements;</li>
            <li>This website is going to work without interruption;</li>
            <li>The website quality is going to meet your expectations;</li>
            <li>Any technical or factual errors will be corrected;</li>
        </ul>

        <h4>Personal information and privacy</h4>
        <p>Devisa LLC does not sell or rent your personal information. Any use of your personal information is within the guidelines of our Privacy Policy. Creating an account with Devisa LLC and using our services means that you agree with the Privacy Policy of this website.</p>
        <p>In case you do not agree with the regulations of our Privacy Policy, you have to cease using our website and its services immediately.</p>

        <h4>Indemnification</h4>
        <p>In case you breach these Terms of Use and with that the rights of a third party, you agree to indemnify and hold Devisa LLC and its officers, owners, partners, directors, employees, agents, subsidiaries and affiliates harmless against all claims, damages, costs, liabilities and expenses asserted against Devisa LLC.</p>

        <h4>Jurisdiction</h4>
        <p>These Terms of Use shall be governed in accordance with international treaty provisions and other applicable laws. Devisa LLC shall retain the right, at its option and for its exclusive benefit, to institute proceedings regarding or relating to your use of our web site and services in the courts of the country in which You reside</p>

        <h4>No monetary liabilities</h4>
        <p>Under no conditions will Devisa LLC be liable for any losses or damages arising from the use or inability to use our services</p>

        <h4>Refund</h4>
        <p>If you have not used our services, you may get a refund within a 30 days period from the date of signing up for our services.</p>


        <h4>No waiver</h4>
        <p>In case we don't act immediately against anyone breaching these Terms of Use, does not mean that we are losing our right to take any actions in response to the breach on a later stage.</p>

        <h4>Using your business name for promotions </h4>
        <p>Signing up for and using our services, gives us the right to use your business name and logo for the promotion of the same services</p>

        <h4>Computer viruses or connection problems</h4>
        <p>Devisa LLC is not responsible for any harm caused to your computer systems by computer viruses that may have been transmitted while accessing our website.</p>

      </Layout>
    </>
  )
}
