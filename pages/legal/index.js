import Head from 'next/head'
import Layout from '../../components/layout'
import {useSession} from 'next-auth/client'
import Link from 'next/link'
export default function Privacy() {
  const [ session ] = useSession()
  return (
    <>
      <Head>
        <title>Legal - Devisa</title>
      </Head>
      <Layout session={session} title="Services">
    <h1> Legal </h1>
    <h3>Pages</h3>
      <ul>
        <li><Link href="/legal/cookies">Cookies Policy</Link></li>
        <li><Link href="/legal/tos">Terms of Service</Link></li>
        <li><Link href="/legal/returns">Returns Policy</Link></li>
        <li><Link href="/legal/privacy">Privacy Policy</Link></li>
      </ul>
      </Layout>
    </>
  )
}
