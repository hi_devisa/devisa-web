import Head from 'next/head'
import Layout from '../../components/layout'
import {useSession} from 'next-auth/client'
export default function Privacy() {
  const [ session ] = useSession()
  return (
    <>
      <Head>
        <title>Privacy - Devisa</title>
      </Head>
      <Layout session={session} title="Services">
    <h1> Privacy Policy </h1>
      <h2>Privacy Policy</h2>
      <p>Your privacy is important to us and you understanding this policy is necessary so we tried to make it as human readable as possible. It is our (Devisa LLC’s) policy to respect your privacy regarding any information we may collect from you across our website.</p>

      <h4>Consent</h4>
      <p>By using <a href="https://devisa.io">https://devisa.io</a> you agree to the terms of this Privacy Policy. This Privacy Policy shall be governed in accordance to international treaty provisions and other applicable laws.</p>

      <h4>Updates to this Privacy Policy</h4>
      <p>We may update this Privacy Policy from time to time. We will notify our users of any significant changes to this Privacy Policy on the Website or through other appropriate communication channels. All changes shall be effective from the date of publication, unless otherwise provided in the notification.</p>

      <h4>Collection of personal information</h4>
      <p>We collect and receive personal data through the Website and Devisa LLC. Personal data means any information relating to an identified or identifiable natural person.</p>


     <p>To be able to complete your purchase order or bill you for subscription, we may collect some billing information, like your credit card number, expiration date, security code and billing address.</p>


     <p>In addition, we collect certain personal data by using cookies, including similar technologies such as local storage when you visit the website. Cookies are bits of text that are placed on your computer’s hard drive or mobile device when you visit certain websites. Cookies hold information that may be accessible by the party that places the cookie, which is either the website itself (first party cookie) or a third party (third party cookies). You do have control over cookies, and can refuse the use of cookies by selecting the appropriate setting on your browser. Most browsers will tell you how to stop accepting new cookies, how to be notified when you receive a new cookie, and how to disable or delete existing cookie.</p>

      <h4>Accessing your personal information</h4>
      <p>You have the right to review the information we store for you and make any changes you wish. You can do this from your account settings.</p>

      <h4>How your personal information is used</h4>
      <p>Devisa LLC collects and uses your personal information to:</p>
      <ul><li>Provide customer service</li><li>Process financial transactions</li><li>Conduct marketing research and analysis</li><li>Administer user account</li><li>Monitor user behaviour</li></ul>

      <h4>Data retention</h4>
      <p>We will retain your personal information with us, as long as your account is active.</p>

      <h4>Protection of your personal information</h4>
      <p>We maintain appropriate technical and organizational security safeguards designed to protect your personal data against accidental, unlawful or unauthorized destruction, loss, alteration, access, disclosure or use.</p>

      <p>However, due to the inherent open nature of the Internet, we cannot guarantee that communications between you and us or the personal information stored is absolutely secure. We will notify you of any data breach that is likely to have unfavorable consequences for your privacy in accordance with applicable law.</p>


      <h4>Links to other sites</h4>
      <p>Our website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies. We try to be conscious of these things when partnering with or linking to external parties.</p>


     <h4>Social media widgets</h4>
      <p>Interacting with any social media widgets on our website (i.e. a Facebook Like button) is governed by the privacy policy of the company providing the widget.</p>

      <h4>What legal basis do we have for processing your personal information?</h4>
      <p>We process your personal information because you’ve given us permission, and we need to process the information to provide the services you have requested and/or to fulfil our contract with you. We may also process your personal information to comply with the law e.g., our tax obligations.</p>

      <h4>Children</h4>
      <p>We don't intend to collect data from anyone under the age of 18. In case that you think that your child may have shared his/her personal information with us, please, contact us.</p>


      <h4>Additional rights for EU citizens</h4>
      <p>If you are an EU citizen, you may have the right to exercise additional rights granted to you by the EU law:</p>
      <p><strong>Right of erasure:</strong> In case that the information we’ve collected for you is no longer required for the purposes it was gathered for in first place, you may ask that we remove it from our systems. However, we may still need to retain certain information for record keeping and accounting.</p>
      <p><strong>Right to object to processing:</strong> You may request that we stop sending you marketing information.</p>
      <p><strong>Right to restrict processing:</strong> If you think that the information we hold for you is inaccurate or unlawfully collected, you may request that we no longer process it.</p>
      <p><strong>Right to data portability:</strong> You may request that we provide all the information we hold for you in a machine readable format to another company.</p>
      <p>If you wish to exercise such rights as any EU citizen, please, email us at <a href="mailto:contact@devisa.io">contact@devisa.io</a></p>


      <h4>How to contact us</h4>
      <p>If you have any comments or inquiries about the information in this Privacy Policy, if you would like us to update your personal data, or to exercise your rights, please contact us by email at <a href="mailto:contact@devisa.io">contact@devisa.io</a></p>
     </Layout>

    </>
  )
}
