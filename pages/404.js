export default function NotFound() {
  return (
    <div class="body-bg min-h-screen pt-12 md:pt-20 pb-6 px-2 md:px-0 font-sans bg-green-500">
    <header class="max-w-lg mx-auto">
            <h1 class="font-thin  text-6xl font-bold text-white text-center">Oops!</h1>
          <br/><br/>
            <h1 class="text-6xl font-bold text-green-600 text-center">...</h1>
          <br/><br/>
            <h3 class="text-5xl font-semibold text-white text-center">Nothing here, apparently.</h3>
            <h1 class="text-6xl font-bold text-green-600 text-center">...</h1>
          <br/><br/>
            <h4 class="text-4xl font-semibold text-white text-center"><a href="/">Click here to go back home!</a></h4>
    </header>
    </div>

          )
          }
