import Tabs from '../../../components/ui/tabs/simple'
import DashLayout from '../../../components/layout/dashboard'

export default function Dashboard() {
  const child1 = (
    <p class="text-4xl"> CHILD1</p>
  )
  const child2 = (
    <p class="text-4xl"> CHILD 2 </p>
  )
  const child3 = (
    <p class="text-4xl"> CHILD 3 </p>
  )
  const child4 = (
    <p class="text-4xl"> CHILD 4 </p>
  )
  const tab1 = (
    <div class="flex">
  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" class="mr-2" fill="currentColor" class="bi bi-graph-up mr-2 mt-1" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z"/>
</svg>
    Overview
    </div>
  )
  const userDashboard = (
    <DashLayout
      title = "Insights"
      mainBtn = "Edit Views"
      secondaryBtn = "More Info"
      breadcrumb = "Home / Dashboard / Insights"
      child1 = { child1 }
      tab1 = { tab1 }
      child2 = { child2 }
      tab2 = "Records"
      child3 = { child3 }
      tab3 = "Recipes"
      >
      <div class="px-8">

      </div>
        <p class="text-xl">Insights</p>
    </DashLayout>
  )
    return userDashboard
}
