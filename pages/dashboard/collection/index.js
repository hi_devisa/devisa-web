import DashLayout from '../../../components/layout/dashboard'
import Card from '../../../components/profile/card'

export default function Dashboard() {
  const userDashboard = (
    <DashLayout
      title = "Your Collection"
      mainBtn = "Add to collection"
      secondaryBtn = "Edit collection"
      breadcrumb = "Home / Your Collection"
      >
        <p class="text-xl">Collection</p>
        <Card/>
    </DashLayout>
  )
    return userDashboard
}
