import DashLayout from '../../components/layout/dashboard'

export default function Dashboard() {
  const userDashboard = (
    <DashLayout
      title = "Dashboard Home"
      mainBtn = "Edit Items"
      secondaryBtn = "Edit Dashboard"
      breadcrumb = "Home / Dashboard"
      >
        <p class="text-xl">Dashboard Home</p>
    </DashLayout>
  )
    return userDashboard
}
