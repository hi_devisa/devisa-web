import { useState, useRef, useEffect } from 'react'
import Link from 'next/link'
import Newsletter from '../components/ui/newsletter'
import Gradient from '../components/ui/button/gradient'
import { useRouter } from 'next/router'
import { gql, useQuery } from '@apollo/client'
import Head from 'next/head'
import Nav from '../components/ui/nav'
import BigNav from '../components/ui/nav/big'
import Banner from '../components/ui/banner/banner'
import Footer from '../components/footer/details'
import Hero from '../components/hero'
import Intro from '../components/intro'
import styles from '../styles/Home.module.css'

import LoginCard from '../components/loginCard'
import Card from '../components/card'
import { NextSeo } from 'next-seo'
import { useSession } from 'next-auth/client'

const ViewerQuery = gql`
  query ViewerQuery {
    viewer {
      id
      email
    }
  }
`

export default function Home() {
  const [ session, loading ] = useSession()
  const [showBanner, setShowBanner] = useState(true);

  const router = useRouter()
  const banner = useRef(null);

  useEffect(() => {
    if (!showBanner) return;
      function handleClick(event) {
        setShowBanner(false);
      }
    window.addEventListener("click", handleClick);
    return () => window.removeEventListener("click", handleClick);
  }, [showBanner]);
  return (
    <>
      { showBanner &&
      <Banner
        button="Get updates"
        href="https://devisa.io/contact"
        onClick={ () => setShowBanner(b => !b) }
      >
        You're here early! Sign up to our newsletter for updates!
      </Banner>
      }
      <script src="https://cdn.jsdelivr.net/npm/tsparticles@1.26.2/dist/tsparticles.min.js" integrity="sha256-QkQN8TZh6VXJW5P2KcxA7WHu8Id1vb/ytdMmX+6OWkA=" crossorigin="anonymous"></script>
    <div id="tsparticles" class="bg-gradient-to-br from-white via-gray-50 to-pink-50 min-h-screen">
      <NextSeo>
        title = "devisa.io"
        description = "The life automator, the life aggregator, the life organizer."
        canonical = "https://devisa.io"
      </NextSeo>
      <Head>
        <title>Home - devisa.io</title>
      </Head>
      <div id="particles-js"></div>
      <Nav session={session}/>
      {/* <BigNav/> */}
      <section class="text-gray-600">
  <div class="container mx-auto flex px-5 py-6 md:flex-row flex-col items-center w-full">
    <div class="lg:flex-grow w-full   flex flex-col md:items-start md:text-left mb-16 max-w-1/2   md:mb-0 items-center text-center p-6 mr-12 rounded-lg">
      <h1 class="  sm:text-4xl text-3xl mb-4 font-medium text-gray-900 subpixel-antialiased font-extralight">We're on a <span class="font-medium text-seagreen-600">mission
      </span></h1>
      <p class="mb-8 leading-relaxed subpixel-antialiased">Hi, Devisa here. We provide software development services and consulting. If you're looking to get your web and/or mobile application off the ground, don't hesitate to <Link href="/services"><span class="text-green-500 font-bold">reach out to us. </span></Link>We're also on a mission to engineer the the most significant leap forward yet seen for life management applications. <Link href="/about"><span class="text-green-500 font-bold">Learn more.</span></Link></p>
      <div class="flex justify-center">
        {/* <Link href="/services"><button class="inline-flex text-white bg-green-500 border-0 py-2 px-6 focus:outline-none hover:bg-green-600 rounded text-lg">Services</button></Link> */}
        <Link href="/services"><button class="ml-4 inline-flex text-white bg-green-600 shadow border-0 py-2 px-6 focus:outline-none hover:bg-gray-200 rounded text-lg">Our services</button></Link>
        <Link href="/about"><button class="ml-4 inline-flex text-gray-800 bg-white shadow border-0 py-2 px-6 focus:outline-none hover:bg-gray-200 rounded text-lg">Learn more</button></Link>
      </div>
    </div>
    <div class="w-full">
      <img class="object-cover object-center rounded" alt="hero" src="/assets/undraw_Personal_goals_re_iow7.svg"/>
    </div>
  </div>
</section>


      <br/>
      <Footer/>
    </div>
  </>
  )
}
