import Head from 'next/head'
import Link from 'next/link'
import useSWR from 'swr'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab, faGoogle, faGitlab, faLinkedin, faFacebook, faTwitter, faGithub } from '@fortawesome/free-brands-svg-icons'
import { useEffect, useState } from 'react'
import {
  useSession, signIn, signOut,
} from 'next-auth/client'

// const fetcher = (url, token) =>
//   fetch(url, {
//     method: 'GET',
//     headers: new Headers({ 'Content-Type': 'application/json', token }),
//     credentials: 'same-origin',
//   }).then((res) => res.json())


async function createUser(email, name) {
  const user = {
    email: email,
    name: name,
  };
  const res = await axios.fetch("http://api.devisa.io/user", {
    method: "POST",
    data: JSON.stringify(user),
  })
    .catch(e => console.error(e))
    .then(r => resp.send(r))
}

export default function Signup() {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  // const { mutate, loggedIn } = useUser();

  // useEffect(() => {
  //   if (loggedIn) Router.replace("/");
  // }, [loggedIn]);

  // if (loggedIn) return <> Redirecting.... </>;

  // const { user, session } = Auth.useUser()
  // const { data, error } = useSWR(session ? ['/api/getUser', session.access_token] : null, fetcher)
  // const [authView, setAuthView] = useState('sign_in')

  // useEffect(() => {
  //   const { data: authListener } = getSb().auth.onAuthStateChange((event, session) => {
  //     if (event === 'PASSWORD_RECOVERY') setAuthView('update_password')
  //     if (event === 'USER_UPDATED') setTimeout(() => setAuthView('sign_in'), 1000)
  //     // Send session to /api/auth route to set the auth cookie.
  //     // NOTE: this is only needed if you're doing SSR (getServerSideProps)!
  //     fetch('/api/auth', {
  //       method: 'POST',
  //       headers: new Headers({ 'Content-Type': 'application/json' }),
  //       credentials: 'same-origin',
  //       body: JSON.stringify({ event, session }),
  //     }).then((res) => res.json())
  //   })

  //   return () => {
  //     authListener.unsubscribe()
  //   }
  // }, [])

  // const View = () => {
  //   if (!user) {
  //     return (
  //       <Space direction="vertical" size={8}>
  //         <div>
  //           <img src="https://app.supabase.io/img/supabase-dark.svg" width="96" />
  //           <Typography.Title level={3}>Welcome to supabase Auth</Typography.Title>
  //         </div>
  //         <Auth
  //           supabaseClient={getSb()}
  //           providers={['google', 'github']}
  //           view={authView}
  //           socialLayout="horizontal"
  //           socialButtonSize="xlarge"
  //         />
  //       </Space>
  //     )
  //   } else {
  //   return (
  //             <Space direction="vertical" size={6}>
  //       {authView === 'update_password' && <Auth.UpdatePassword supabaseClient={getSb()} />}
  //       {user && (
  //         <>
  //           <Typography.Text>You're signed in</Typography.Text>
  //           <Typography.Text strong>Email: {user.email}</Typography.Text>

  //           <Button
  //             icon={<Icon type="LogOut" />}
  //             type="outline"
  //             onClick={() => getSb().auth.signOut()}
  //           >
  //             Log out
  //           </Button>
  //           {error && <Typography.Text danger>Failed to fetch user!</Typography.Text>}
  //           {data && !error ? (
  //             <>
  //               <Typography.Text type="success">
  //                 User data retrieved server-side (in API route):
  //               </Typography.Text>

  //               <Typography.Text>
  //                 <pre>{JSON.stringify(data, null, 2)}</pre>
  //               </Typography.Text>
  //             </>
  //           ) : (
  //             <div>Loading...</div>
  //           )}

  //           <Typography.Text>
  //             <Link href="/profile">
  //               <a>SSR example with getServerSideProps</a>
  //             </Link>
  //           </Typography.Text>
  //         </>
  //       )}
  //     </Space>
  //   )
  //   }
  // }

  return (
    <div class="body-bg min-h-screen pt-6 md:pt-5 pb-6 px-2 md:px-0 font-sans bg-green-500">
    <Head>
      <title>Signup - Devisa</title>
    </Head>
    <header class="max-w-lg mx-auto pt-5">
            <h1 class="text-4xl font-bold text-gray-50 text-center transition-opacity from-transparent"><Link href="/" class="hover:text-gray-200 transition duration-200">Devisa</Link><span class="text-green-600"> Sign up</span></h1>
    </header>

    <main class="bg-white max-w-lg mx-auto p-3 md:p-8 mt-10 rounded-lg shadow-2xl overflow-y-hidden">
        <section class="text-center">
            <h4 class="font-medium text-3xl text-gray-600 mb-10">Glad to have you along.</h4>
        </section>
        <div class="flex items-center justify-between mt-8">
            <span class="w-1/5 border-b dark:border-gray-600 md:w-1/4"></span>

            <span href="#" class="text-xs text-gray-500 uppercase dark:text-gray-400 hover:underline">sign up with external service</span>

            <span class="w-1/5 border-b dark:border-gray-600 md:w-1/4"></span>
        </div>

        <div class="grid grid-cols-2 gap-2 space-y-1">

          <button
            onClick = {() => signIn("google")}
            id = "google"
            class="col-span-2 w-full bg-indigo-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faGoogle}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  Sign up with Google</span>
          </button>

          <button
            onClick = {() => signIn("facebook")}
            id = "facebook"
            class="w-full bg-blue-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faFacebook}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  Facebok</span>
              </button>

          <button
            onClick = {() => signIn("linkedin")}
            id = "linkedin"
            class="w-full bg-blue-900 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faLinkedin}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  LinkedIn</span>
              </button>
          <button
            onClick = {() => signIn("gitlab")}
            id = "gitlab"
            class="w-full bg-yellow-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faGitlab}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  GitLab</span>
              </button>
              <button
                onClick={() => signIn("github")}
                id = "github"
                class="w-full bg-gray-700 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-gray-800 dark:hover:bg-gray-600 hover:shadow-lg">

                <span class="text-white mr-25"><FontAwesomeIcon icon={faGithub}/></span>
                <span class="w-5/6 px-4 py-3 font-bold text-center">GitHub</span>
              </button>

        </div>
            <div class="flex items-center justify-between mt-8">
                <span class="w-1/5 border-b dark:border-gray-600 md:w-1/4"></span>

                <span href="#" class="text-xs text-gray-500 uppercase dark:text-gray-400 hover:underline">or sign up with e-mail</span>

                <span class="w-1/5 border-b dark:border-gray-600 md:w-1/4"></span>
            </div>
            <form method="POST" action="#">
            <div class="mt-4">
                <label class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200" htmlFor="LoggingEmailAddress">Your Name</label>
                <input
                    value={email}
                    onChange={(e) => setName(e.target.value)}
                    id="email"
                    class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-green-500 dark:focus:border-green-500 focus:outline-none focus:ring"
                    type="email"/>
            </div>
            <div class="mt-4">
                <label class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200" htmlFor="LoggingEmailAddress">Email Address</label>
                <input
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    id="email"
                    class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-green-500 dark:focus:border-green-500 focus:outline-none focus:ring"
                    type="email"/>
            </div>
            <div class="mt-8">
                <Link href="#">
                <button
                    onClick={submit.bind(this)}
                    type="submit"
                    class="w-full flex items-center justify-center mt-4 text-white transition-colors duration-200 transform bg-green-400 rounded-lg shadow-md hover:shadow-lg hover:bg-green-500 transition duration-200 focus:outline-none focus:bg-green-500 dark:bg-gray-700 dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-600">
                    <span class="w-5/6 px-4 py-3 font-bold text-center">Continue to signup</span>
                </button>
            </Link>
            </div>
            </form>
    <div class="max-w-lg mx-auto text-center mt-3 mb-0">
        <p class="text-gray-600 text-sm">Already have an account? <span class=" text-green-600 font-bold hover:underline"><Link href="/login">Login</Link></span>.</p>
    </div>
    </main>

    <footer class="max-w-lg mx-auto flex justify-center text-white">

    </footer>
</div>
                      )
              }
function submit(email, password) {
  alert("Email: " + email + ", pass: " + password)
  // supabase.auth
  //   .signIn({email, password})
  //   .then((response) => {
  //     document.querySelector('#access-token').value = response.data.access_token
  //     document.querySelector('#refresh-token').value = response.data.refresh_token
  //     alert("Logged in as " + response.user.email)
  //   })
  //   .catch((err) => {
  //     alert(err.response.text)
  //   })
}
