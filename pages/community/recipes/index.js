import axios from 'axios'
import { useEffect, useState } from 'react'
import GroupTable from '../../../components/ui/table/group'
import DashLayout from '../../../components/layout/dashboard'
import { useSession } from 'next-auth/client'

export default function Recipes(props) {
  return (
    <DashLayout
      title = "Recipes"
      mainBtn = "Recipe Settings"
      secondaryBtn = "Edit Recipes"
      breadcrumb = "Home / Community / Recipes"
      >
    </DashLayout>

  )
}

//   const getCurrent = async (email) => {
//     await axios.get("/api/session/current", {
//       email: email.toString()
//     })
//       .then(res => {
//         return res.data
//       })
//   }
//   const [ user, setUser ] = useState(null)
//   useEffect(async () => {
//     if (session) {
//       await getCurrent(seession.user.email)
//         .then(user => { setUser(user) })
//       }
//     }
//   )
//   const [ session, loading ] = useSession()
//   const createGroup = async (id) => {
//     await axios.post("/api/group", {
//       "name": "testGroup",
//       "userId": parseInt(id),
//     })
//   }
//   const userDashboard = (
//     <DashLayout
//       title = "Recipes"
//       mainBtn = "Recipe Settings"
//       secondaryBtn = "Edit Recipes"
//       breadcrumb = "Home / Community / Recipes"
//       >
//         <p class="text-xl">Community Recipes</p>
//       { session ?
//         <p class="text-4xl"> { user.id }</p>
//         : <p class="text-4xl"> NO SESSION </p>
//       }
//       <button onClick={async () => { await createGroup(user.id) }}>
//         Create</button>
//     </DashLayout>
//   )
//     return userDashboard
// }

