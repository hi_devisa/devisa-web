import axios from 'axios'
import GroupTable from '../../../components/ui/table/group'
import DashLayout from '../../../components/layout/dashboard'
import { useSession } from 'next-auth/client'

export default function Feed(props) {
  const userDashboard = (
    <DashLayout
      title = "Groups"
      mainBtn = "Groups Settings"
      secondaryBtn = "Edit Groups"
      breadcrumb = "Home / Community / Groups"
      >
      <GroupTable/>
    </DashLayout>
  )
    return userDashboard
}
