import { useState, useEffect } from 'react'
import axios from 'axios'
import DashLayout from '../../../components/layout/dashboard'

export default function Topics(props) {
  const [topics, setTopics] = useState([])
  const [records, setRecords] = useState([])
  const [users, setUsers] = useState([])
  useEffect(() => {
    fetchTopics()
    fetchRecords()
    fetchUsers()
  }, [])
  const fetchTopics = async () => {
    await axios.get("/api/topic")
      .then(resp => resp.data)
      .then(resp => { setTopics(resp) });
  }
  const fetchRecords = async () => {
    await axios.get("/api/user/1/records")
      .then(resp => resp.data)
      .then(resp => { setRecords(resp) });
  }
  const fetchUsers = async () => {
    await axios.get("/api/user")
      .then(resp => resp.data)
      .then(resp => { setUsers(resp) });
  }
  const userDashboard = (
    <DashLayout
      title = "Topics"
      mainBtn = "New topic"
      secondaryBtn = "Edit topics"
      breadcrumb = "Home / Community / Topics"
      >
        <p class="text-xl">Community Topics</p>
      <div className="overflow-hidden bg-white shadow rounded-md">
        <ul>
          {topics.map((topic) => (
            <p>{topic.id}: {topic.name} </p>
          ))}
        </ul>
      </div>
      <div className="overflow-hidden bg-white shadow rounded-md">
        <ul>
          {records.map((rec) => (
            <p>{rec.id}: {rec.name}, {rec.userId}, {rec.private}, {rec.createdAt} </p>
          ))}
        </ul>
      </div>
      <div className="overflow-hidden bg-white shadow rounded-md">
        <ul>
          {users.map((u) => (
            <p>{u.id}: {u.name}, {u.email}, {u.image}, {u.createdAt} </p>
          ))}
        </ul>
      </div>
    </DashLayout>
  )
    return userDashboard
}

