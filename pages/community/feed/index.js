import DashLayout from '../../../components/layout/dashboard'
import Post from '../../../components/user/post'
import Comments from '../../../components/user/comments'
import Listbox from '../../../components/ui/listbox'
import Tooltip from '../../../components/ui/tooltip/tooltip'
import Toast from '../../../components/ui/toast/toast'

export default function Feed() {
  const userDashboard = (
    <DashLayout
      title = "Community Feed"
      mainBtn = "Feed Settings"
      secondaryBtn = "Edit Feed"
      breadcrumb = "Home / Community / Feed"
      >
        <p class="text-xl">News feed</p>
      <Post/>
      <Listbox/>
      <Tooltip
        color="green"
        placement="bottom"
        title = "This is the tooltip title"
        tooltip="This is the tooltip text"
        opacity="90" >
      <h1 class="text-4xl"> HELLO TESTING TOOLTIP</h1>
    </Tooltip>
      <Toast/>
    </DashLayout>
  )
    return userDashboard
}
