
import DashLayout from '../../components/layout/dashboard'

export default function DvsaLab() {
  // HERE WE WILL CREATE A JUPYTER LAB - LIKE REPL
  const userDashboard = (
    <DashLayout
      title = "Invest"
      mainBtn = "New"
      secondaryBtn = "Edit"
      breadcrumb = "Home / Invest"
      bypassLogin = { true }
      >
        <p class="text-xl">Dashboard Home</p>
    </DashLayout>
  )
    return userDashboard
}


