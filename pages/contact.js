import Head from 'next/head'
import Input from '../components/ui/input/leftLabel'
import Link from 'next/link'
import { useSession } from 'next-auth/client'
import Footer from '../components/footer/details'
import { NextSeo } from 'next-seo'
import Nav from '../components/ui/nav'
import styles from '../styles/Home.module.css'
import Layout from '../components/layout'
import Card from '../components/card'
import { useRouter } from 'next/router'

export default function Home() {
  const [ session, loading ] = useSession()
  return (
    <Layout title="Contact Us" session={session} header={true}>
  <div class="mx-auto container px-4 xl:px-4">
            <div class="mt-12 lg:mt-24">
                <div class="grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8">
                    <div class="bg-white shadow-md cursor-pointer  py-10 rounded-3xl flex flex-col items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52" fill="none">
                            <path d="M7.64706 2H18.9412L24.5882 16.1866L17.5294 20.4426C20.5533 26.6039 25.5157 31.5905 31.6471 34.6291L35.8824 27.5358L50 33.2105V44.5597C50 46.0647 49.405 47.5081 48.346 48.5723C47.287 49.6365 45.8506 50.2344 44.3529 50.2344C33.3391 49.5618 22.951 44.8619 15.1487 37.0215C7.34636 29.1811 2.66932 18.7423 2 7.67463C2 6.16963 2.59496 4.72626 3.65398 3.66206C4.71301 2.59786 6.14937 2 7.64706 2Z" stroke="#052E47" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                        <h2 class="text-lg font-bold tracking-wide mt-4 mb-5 text-gray-900">Phone</h2>
                        <p class="text-base font-semibold tracking-wide text-gray-900">+1 (844) 425 0400</p>
                        <p class="text-base font-semibold tracking-wide text-gray-900 mt-4">+1 (206) 432 7348</p>
                    </div>
                    <div class="bg-white shadow-md cursor-pointer py-10 rounded-3xl flex flex-col items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 50 51" fill="none">
                            <path d="M1 10.0439L17 1L33 10.0439L49 1V40.1904L33 49.2344L17 40.1904L1 49.2344V10.0439Z" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M17 1V40.1904" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M33 10.0439V49.2344" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                        <h2 class="text-lg font-bold tracking-wide mt-4 mb-5 text-gray-700">Location</h2>
                        <div class="text-gray-700 text-base tracking-wide text-center">
                            <p class="leading-7">Seattle WA</p>
                            <p class="leading-7">98105, United States</p>
                        </div>
                    </div>
                    <div class="bg-white shadow-md cursor-pointer py-10 rounded-3xl flex flex-col items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="51" viewBox="0 0 50 51" fill="none">
                            <path d="M25 49.2344C38.2548 49.2344 49 38.4367 49 25.1172C49 11.7976 38.2548 1 25 1C11.7452 1 1 11.7976 1 25.1172C1 38.4367 11.7452 49.2344 25 49.2344Z" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M25 11.7183V25.117L33 33.1562" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                        <h2 class="text-lg font-bold tracking-wide mt-4 mb-5 text-gray-700">Time</h2>
                        <div class="text-gray-700 text-base tracking-wide text-center">
                            <p class="leading-7">7 days a week</p>
                          <p class="leading-7">08:00 AM - 08:00 PM</p>
                        </div>
                    </div>
                    <div class="bg-white shadow-md cursor-pointer py-10 rounded-3xl flex flex-col items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="64" height="51" viewBox="0 0 64 51" fill="none">
                            <path d="M55.8571 1H7.85714C4.07004 1 1 4.08504 1 7.89062V42.3438C1 46.1493 4.07004 49.2344 7.85714 49.2344H55.8571C59.6442 49.2344 62.7142 46.1493 62.7142 42.3438V7.89062C62.7142 4.08504 59.6442 1 55.8571 1Z" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M1 7.89014L31.8571 28.562L62.7142 7.89014" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                        <h2 class="text-lg font-bold tracking-wide mt-4 mb-5 text-gray-700">E-mail</h2>
                        <div class="text-gray-700 text-base tracking-wide text-center">
                          <a href="mailto:hi@devisa.io"><p class="text-green-500">hi@devisa.io</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </Layout>

  )
}

function submit(e) {
    const formData = new FormData(form);
    e.preventDefault();
    var object = {};
    formData.forEach((value, key) => {
        object[key] = value
    });
    var json = JSON.stringify(object);
    result.innerHTML = "Please wait..."

    fetch('https://api.web3forms.com/submit', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: json
        })
        .then(async (response) => {
            let json = await response.json();
            if (response.status == 200) {
                result.innerHTML = json.message;
                 result.classList.remove('text-gray-500');
                result.classList.add('text-green-500');
            } else {
                console.log(response);
                result.innerHTML = json.message;
                 result.classList.remove('text-gray-500');
                 result.classList.add('text-red-500');
            }
        })
        .catch(error => {
            console.log(error);
            result.innerHTML = "Something went wrong!";
        })
        .then(function() {
            form.reset();
            setTimeout(() => {
                result.style.display = "none";
            }, 5000);
        });
}
