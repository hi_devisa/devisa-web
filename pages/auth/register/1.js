
import Head from 'next/head'
import CountrySelect from '../../../components/ui/form/countrySelect'
import Dropdown from '../../../components/dropdown'
import Link from 'next/link'
import useSWR from 'swr'
import Input from '../../../components/ui/input'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab, faGoogle, faGitlab, faLinkedin, faFacebook, faTwitter, faGithub } from '@fortawesome/free-brands-svg-icons'
import axios from 'axios'
import { useEffect, useState } from 'react'
import {
  useSession, signIn, signOut,
} from 'next-auth/client'

export default function NewAccount() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [ session, loading ] = useSession()
  const getUserId = async () => {
    if (session) {
      await axios.get("/api/user/email", {
        data: {
          email: session.user.email
        }
      })
        .then(res => { return parseInt(res.id) })
        .catch(e => console.error(e))
    } else {
      return null }
  }
  const createCredentials = async ( username, password ) => {
    const id = await getUserId()
      .then(res => parseInt(res))
    await axios.post("/api/user/" + id + "credentials", {
      username: username,
      password: password
    })
      .catch(e => console.error(e))
      .then(res => {
          return res
        }
      )
  }

  if (loading) {
    return (
      <p class="text-4x">Loading</p>
    )
  }
  return (
    <div class="body-bg min-h-screen pt-6 md:pt-5 pb-6 px-8 md:px-0 font-sans bg-green-500" style={{backgroundImage: `url(lightart.jpg)`}}>
    <Head>
      <title>Account Registration - Devisa</title>
    </Head>

    <main class="bg-white max-w-lg mx-auto p-3 md:p-8 mt-10 rounded-lg shadow-2xl overflow-y-hidden">
        <section class="text-center">
            <h4 class="font-medium text-3xl text-gray-600 mb-10">Welcome to Devisa!.</h4>
          <h2 class="font-medium text-md text-gray-500 mb-4">Let's start by creating your username and password. Your username will be how other users see you on the site, and you can optionally choose to login with your credentials</h2>
          <div class="my-6 w-2/3 mx-auto">
                    <div class="col-span-3 sm:col-span-2">
                      <label for="company_website" class="block text-sm font-medium text-gray-700">
                        Username
                      </label>
                      <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="text" name="company_website" id="company_website" class="focus:ring-green-500 focus:border-green-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="www.example.com"
                          onChange={(e) => { setUsername(e.target.value) }}/>
                      </div>
                    </div>
          </div>
          <div class="my-6 w-2/3 mx-auto">
                    <div class="col-span-3 sm:col-span-2">
                      <label for="company_website" class="block text-sm font-medium text-gray-700">
                        Password
                      </label>
                      <div class="mt-1 flex rounded-md shadow-sm">
                        <input type="password" name="company_website" id="company_website" class="focus:ring-green-500 focus:border-green-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="www.example.com" onChange={(e) => {setPassword(e.target.value)}}/>
                      </div>
                    </div>
          </div>
          <div class="grid grid-cols-4">
            <div class="col-span-2"></div>
            <div>
              <Link href="/auth/register/2">
          <button
            class="px-4 py-2 bg-green-100 rounded-lg shadow   text-sm text-gray-800"
            onClick={async (e) => {
              e.preventDefault();
              await createCredentials(username, password)
                .then(res => console.log(res));
            }}
          >Next</button>
              </Link>
            </div>
          </div>
  </section>
          </main>
          </div>
                      )
              }
function submit(email, password) {
  alert("Email: " + email + ", pass: " + password)
  // supabase.auth
  //   .signIn({email, password})
  //   .then((response) => {
  //     document.querySelector('#access-token').value = response.data.access_token
  //     document.querySelector('#refresh-token').value = response.data.refresh_token
  //     alert("Logged in as " + response.user.email)
  //   })
  //   .catch((err) => {
  //     alert(err.response.text)
  //   })
}
