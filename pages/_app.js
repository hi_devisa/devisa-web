import '../styles/globals.css'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import "tailwindcss/tailwind.css";
import FacebookPixel from '../components/monitoring/facebookPixel'
import GoogleTagManager from '../components/goog'
import * as gtag from '../lib/gtag'
import { ApolloProvider } from '@apollo/client'
import { useApollo } from '@apollo/client';
import { DefaultSeo } from 'next-seo'
import SEO from '../next-seo.config'
import { Provider } from 'next-auth/client'



function MyApp({ Component, pageProps }) {
  const router = useRouter()
  // useEffect(() => {
    // const handleRouteChange = (url) => {
    //   gtag.pageview(url)
    // }
    // router.events.on('routeChangeComplete', handleRouteChange)
    // return () => {
    //   router.events.off('routeChangeComplete', handleRouteChange)
    // }
  // }, [router.events])

  return (
    //    <GoogleTagManager>
      // <DefaultSeo {...SEO}/>

      <Provider session={pageProps.session}>
        <FacebookPixel>
          <Component {...pageProps} />
        </FacebookPixel>
      </Provider>
    // </GoogleTagManager>
  )
}

export default MyApp
