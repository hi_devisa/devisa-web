import Layout from '../components/layout'
import Feature from '../components/ui/features/feature'
import FeatureCard from '../components/ui/features/featureCard'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'

export default function Features(props) {
  const [ session, loading ] = useSession()
  return (
    <Layout title="Features" session={session}>

        <Feature/>
    </Layout>
  )
}
