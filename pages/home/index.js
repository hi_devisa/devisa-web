export default function Home() {
  // function searchHandler(element) {
  //     let Input = element.parentElement.getElementsByTagName("input")[0];
  //     Input.classList.toggle("w-0");
  //     Input.classList.toggle("w-24");
  // }
  // function dropdownHandler(element) {
  //     let single = element.getElementsByTagName("ul")[0];
  //     single.classList.toggle("hidden");
  // }
  // function MenuHandler(el, val) {
  //     let MainList = el.parentElement.getElementsByTagName("ul")[0];
  //     let closeIcon = el.parentElement.getElementsByClassName("close-m-menu")[0];
  //     let showIcon = el.parentElement.getElementsByClassName("show-m-menu")[0];
  //     if (val) {
  //         MainList.classList.remove("hidden");
  //         el.classList.add("hidden");
  //         closeIcon.classList.remove("hidden");
  //     } else {
  //         showIcon.classList.remove("hidden");
  //         MainList.classList.add("hidden");
  //         el.classList.add("hidden");
  //     }
  // }
  // let sideBar = document.getElementById("mobile-nav");
  // let menu = document.getElementById("menu");
  // let cross = document.getElementById("cross");
  // sideBar.style.transform = "translateX(-100%)";
  // const sidebarHandler = (check) => {
  //     if (check) {
  //         sideBar.style.transform = "translateX(0px)";
  //         menu.classList.add("hidden");
  //         cross.classList.remove("hidden");
  //     } else {
  //         sideBar.style.transform = "translateX(-100%)";
  //         menu.classList.remove("hidden");
  //         cross.classList.add("hidden");
  //     }
  // };
  // let list = document.getElementById("list");
  // let chevrondown = document.getElementById("chevrondown");
  // let chevronup = document.getElementById("chevronup");
  // const listHandler = (check) => {
  //     if (check) {
  //         list.classList.remove("hidden");
  //         chevrondown.classList.remove("hidden");
  //         chevronup.classList.add("hidden");
  //     } else {
  //         list.classList.add("hidden");
  //         chevrondown.classList.add("hidden");
  //         chevronup.classList.remove("hidden");
  //     }
  // };
  // let list2 = document.getElementById("list2");
  // let chevrondown2 = document.getElementById("chevrondown2");
  // let chevronup2 = document.getElementById("chevronup2");
  // const listHandler2 = (check) => {
  //     if (check) {
  //         list2.classList.remove("hidden");
  //         chevrondown2.classList.remove("hidden");
  //         chevronup2.classList.add("hidden");
  //     } else {
  //         list2.classList.add("hidden");
  //         chevrondown2.classList.add("hidden");
  //         chevronup2.classList.remove("hidden");
  //     }
  // };

  return (

    <div>
        <div id="mobile-nav" class="w-full h-full xl:hidden absolute z-40">
            <div class="bg-green-300 opacity-50 inset-0 fixed w-full h-full" onclick="sidebarHandler(false)"></div>
            <div class="w-64 z-20 absolute left-0 z-40 top-0 bg-green-500 shadow flex-col justify-between transition duration-150 ease-in-out h-full">
                <div class="flex flex-col justify-between h-full">
                    <div class="px-6 pt-4 overflow-y-auto">
                        <div class="flex items-center justify-end">
                            <div id="cross" class="hidden text-white" onclick="sidebarHandler(false)">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" />
                                    <line x1="18" y1="6" x2="6" y2="18" />
                                    <line x1="6" y1="6" x2="18" y2="18" />
                                </svg>
                            </div>
                        </div>
                        <ul class="f-m-m">
                            <a>
                                <li class="text-white pt-8">
                                    <div class="flex items-center">
                                        <div class="md:w-6 md:h-6 w-5 h-5">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="none">
                                                <path d="M7.16667 3H3.83333C3.3731 3 3 3.3731 3 3.83333V7.16667C3 7.6269 3.3731 8 3.83333 8H7.16667C7.6269 8 8 7.6269 8 7.16667V3.83333C8 3.3731 7.6269 3 7.16667 3Z" stroke="#66EA7E" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M7.16667 11.6667H3.83333C3.3731 11.6667 3 12.0398 3 12.5V15.8333C3 16.2936 3.3731 16.6667 3.83333 16.6667H7.16667C7.6269 16.6667 8 16.2936 8 15.8333V12.5C8 12.0398 7.6269 11.6667 7.16667 11.6667Z" stroke="#66EA7E" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M16.1667 11.6667H12.8333C12.3731 11.6667 12 12.0398 12 12.5V15.8334C12 16.2936 12.3731 16.6667 12.8333 16.6667H16.1667C16.6269 16.6667 17 16.2936 17 15.8334V12.5C17 12.0398 16.6269 11.6667 16.1667 11.6667Z" stroke="#66EA7E" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M16.1667 3H12.8333C12.3731 3 12 3.3731 12 3.83333V7.16667C12 7.6269 12.3731 8 12.8333 8H16.1667C16.6269 8 17 7.6269 17 7.16667V3.83333C17 3.3731 16.6269 3 16.1667 3Z" stroke="#66EA7E" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                        </div>
                                        <p class="text-green-400 ml-3 text-lg">Dashboard</p>
                                    </div>
                                </li>
                            </a>
                            <a>
                                <li class="text-white pt-8">
                                    <div class="flex items-center">
                                        <div class="flex items-center">
                                            <div class="md:w-6 md:h-6 w-5 h-5">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" fill="none">
                                                    <path
                                                        d="M2.33333 4.83333H4.83333C5.05435 4.83333 5.26631 4.74554 5.42259 4.58926C5.57887 4.43298 5.66667 4.22101 5.66667 4V3.16667C5.66667 2.72464 5.84226 2.30072 6.15482 1.98816C6.46738 1.67559 6.89131 1.5 7.33333 1.5C7.77536 1.5 8.19928 1.67559 8.51184 1.98816C8.8244 2.30072 9 2.72464 9 3.16667V4C9 4.22101 9.0878 4.43298 9.24408 4.58926C9.40036 4.74554 9.61232 4.83333 9.83333 4.83333H12.3333C12.5543 4.83333 12.7663 4.92113 12.9226 5.07741C13.0789 5.23369 13.1667 5.44565 13.1667 5.66667V8.16667C13.1667 8.38768 13.2545 8.59964 13.4107 8.75592C13.567 8.9122 13.779 9 14 9H14.8333C15.2754 9 15.6993 9.17559 16.0118 9.48816C16.3244 9.80072 16.5 10.2246 16.5 10.6667C16.5 11.1087 16.3244 11.5326 16.0118 11.8452C15.6993 12.1577 15.2754 12.3333 14.8333 12.3333H14C13.779 12.3333 13.567 12.4211 13.4107 12.5774C13.2545 12.7337 13.1667 12.9457 13.1667 13.1667V15.6667C13.1667 15.8877 13.0789 16.0996 12.9226 16.2559C12.7663 16.4122 12.5543 16.5 12.3333 16.5H9.83333C9.61232 16.5 9.40036 16.4122 9.24408 16.2559C9.0878 16.0996 9 15.8877 9 15.6667V14.8333C9 14.3913 8.8244 13.9674 8.51184 13.6548C8.19928 13.3423 7.77536 13.1667 7.33333 13.1667C6.89131 13.1667 6.46738 13.3423 6.15482 13.6548C5.84226 13.9674 5.66667 14.3913 5.66667 14.8333V15.6667C5.66667 15.8877 5.57887 16.0996 5.42259 16.2559C5.26631 16.4122 5.05435 16.5 4.83333 16.5H2.33333C2.11232 16.5 1.90036 16.4122 1.74408 16.2559C1.5878 16.0996 1.5 15.8877 1.5 15.6667V13.1667C1.5 12.9457 1.5878 12.7337 1.74408 12.5774C1.90036 12.4211 2.11232 12.3333 2.33333 12.3333H3.16667C3.60869 12.3333 4.03262 12.1577 4.34518 11.8452C4.65774 11.5326 4.83333 11.1087 4.83333 10.6667C4.83333 10.2246 4.65774 9.80072 4.34518 9.48816C4.03262 9.17559 3.60869 9 3.16667 9H2.33333C2.11232 9 1.90036 8.9122 1.74408 8.75592C1.5878 8.59964 1.5 8.38768 1.5 8.16667V5.66667C1.5 5.44565 1.5878 5.23369 1.74408 5.07741C1.90036 4.92113 2.11232 4.83333 2.33333 4.83333"
                                                        stroke="currentColor"
                                                        stroke-width="1"
                                                        stroke-linecap="round"
                                                        stroke-linejoin="round"
                                                    />
                                                </svg>
                                            </div>

                                            <p class="text-white ml-3 text-lg">Products</p>
                                        </div>
                                        <div id="chevronup" onclick="listHandler(true)" class="ml-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="14" height="14" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <polyline points="6 9 12 15 18 9" />
                                            </svg>
                                        </div>
                                        <div id="chevrondown" onclick="listHandler(false)" class="hidden ml-4">
                                            <svg class="icon icon-tabler icon-tabler-chevron-up" width="14" height="14" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <polyline points="6 15 12 9 18 15" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div id="list" class="hidden">
                                        <ul class="my-3">
                                            <li class="text-sm text-green-500 py-2 px-6">Best Sellers</li>
                                            <li class="text-sm text-white hover:text-green-500 py-2 px-6">Out of Stock</li>
                                            <li class="text-sm text-white hover:text-green-500 py-2 px-6">New Products</li>
                                        </ul>
                                    </div>
                                </li>
                            </a>
                            <a>
                                <li class="text-white pt-8">
                                    <div class="flex items-center">
                                        <div class="md:w-6 md:h-6 w-5 h-5">
                                            <svg viewBox="0 0 20 20" fill="none">
                                                <path d="M6.66667 13.3333L8.33334 8.33334L13.3333 6.66667L11.6667 11.6667L6.66667 13.3333Z" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5Z" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                        </div>
                                        <p class="text-white ml-3 text-lg">Performance</p>
                                    </div>
                                </li>
                            </a>
                            <a>
                                <li class="text-white pt-8">
                                    <div class="flex items-center">
                                        <div class="flex items-center">
                                            <div class="md:w-6 md:h-6 w-5 h-5">
                                                <svg viewBox="0 0 20 20" fill="none">
                                                    <path d="M5.83333 6.66667L2.5 10L5.83333 13.3333" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                    <path d="M14.1667 6.66667L17.5 10L14.1667 13.3333" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                    <path d="M11.6667 3.33333L8.33333 16.6667" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" />
                                                </svg>
                                            </div>
                                            <p class="text-white ml-3 text-lg">Deliverables</p>
                                        </div>
                                        <div>
                                            <div id="chevronup2" onclick="listHandler2(true)" class="ml-4">
                                                <svg class="icon icon-tabler icon-tabler-chevron-down" width="14" height="14" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                    <polyline points="6 9 12 15 18 9" />
                                                </svg>
                                            </div>
                                            <div id="chevrondown2" onclick="listHandler2(false)" class="hidden ml-4">
                                                <svg  class="icon icon-tabler icon-tabler-chevron-up" width="14" height="14" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                    <polyline points="6 15 12 9 18 15" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="list2" class="hidden">
                                        <ul class="my-3">
                                            <li class="text-sm text-green-500 py-2 px-6">Best Sellers</li>
                                            <li class="text-sm text-white hover:text-green-500 py-2 px-6">Out of Stock</li>
                                            <li class="text-sm text-white hover:text-green-500 py-2 px-6">New Products</li>
                                        </ul>
                                    </div>
                                </li>
                            </a>
                        </ul>
                    </div>
                    <div class="w-full">
                        <div class="flex justify-center mb-4 w-full px-6">
                            <div class="relative w-full">
                                <div class="text-gray-500 absolute ml-4 inset-0 m-auto w-4 h-4">
                                    <svg  class="icon icon-tabler icon-tabler-search" width="16" height="16" viewBox="0 0 24 24" stroke-width="1" stroke="#A0AEC0" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z"></path>
                                        <circle cx="10" cy="10" r="7"></circle>
                                        <line x1="21" y1="21" x2="15" y2="15"></line>
                                    </svg>
                                </div>
                                <input class="bg-gray-100 focus:outline-none rounded w-full text-sm text-gray-500 bg-gray-700 pl-10 py-2" type="text" placeholder="Search" />
                            </div>
                        </div>
                        <div class="border-t border-gray-700">
                            <div class="w-full flex items-center justify-between px-6 pt-1">
                                <div class="flex items-center">
                                    <img alt="profile-pic" src="https://tuk-cdn.s3.amazonaws.com/assets/components/boxed_layout/bl_1.png" class="w-8 h-8 rounded-md" />
                                    <p class=" text-white text-base leading-4 ml-2">Jane Doe</p>
                                </div>
                                <ul class="flex">
                                    <li class="cursor-pointer text-white pt-5 pb-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-messages" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="#FFFFFF" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <path d="M21 14l-3 -3h-7a1 1 0 0 1 -1 -1v-6a1 1 0 0 1 1 -1h9a1 1 0 0 1 1 1v10"></path>
                                            <path d="M14 15v2a1 1 0 0 1 -1 1h-7l-3 3v-10a1 1 0 0 1 1 -1h2"></path>
                                        </svg>
                                    </li>
                                    <li class="cursor-pointer text-white pt-5 pb-3 pl-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-bell" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="#FFFFFF" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <path d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6"></path>
                                            <path d="M9 17v1a3 3 0 0 0 6 0v-1"></path>
                                        </svg>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="w-full mx-auto bg-gray-800 border-b border-gray-700 shadow relative z-20">
            <div class="container px-6 h-16 flex items-center justify-between mx-auto">
                <div class="flex items-center">
                    <div class="rounded-full relative p-3 flex justify-end text-white">
                        <input type="text" class="bg-transparent focus:outline-none text-xs w-0 transition duration-150 ease-in-out absolute left-0 ml-10" placeholder="Type something..." />
                        <svg onclick="searchHandler(this)" xmlns="http://www.w3.org/2000/svg" class="cursor-pointer icon icon-tabler icon-tabler-search" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <circle cx="10" cy="10" r="7"></circle>
                            <line x1="21" y1="21" x2="15" y2="15"></line>
                        </svg>
                    </div>
                </div>
                <ul class="flex items-center justify-center h-full">
                    <li class="cursor-pointer h-full xl:flex items-center text-sm text-green-500 tracking-normal hidden">Dashboard</li>
                    <li class="hover:text-green-500 cursor-pointer h-full xl:flex items-center text-sm text-white ml-0 xl:ml-10 tracking-normal hidden">Products</li>
                    <li class="mx-0 xl:mx-12 cursor-pointer">
                        <svg aria-label="Home" id="logo" enable-background="new 0 0 300 300" height="44" viewBox="0 0 300 300" width="43">
                            <g>
                                <path
                                    fill="#66EA7E"
                                    d="m234.735 35.532c-8.822 0-16 7.178-16 16s7.178 16 16 16 16-7.178 16-16-7.178-16-16-16zm0 24c-4.412 0-8-3.588-8-8s3.588-8 8-8 8 3.588 8 8-3.588 8-8 8zm-62.529-14c0-2.502 2.028-4.53 4.53-4.53s4.53 2.028 4.53 4.53c0 2.501-2.028 4.529-4.53 4.529s-4.53-2.027-4.53-4.529zm89.059 60c0 2.501-2.028 4.529-4.53 4.529s-4.53-2.028-4.53-4.529c0-2.502 2.028-4.53 4.53-4.53s4.53 2.029 4.53 4.53zm-40.522-5.459-88-51.064c-1.242-.723-2.773-.723-4.016 0l-88 51.064c-1.232.715-1.992 2.033-1.992 3.459v104c0 1.404.736 2.705 1.938 3.428l88 52.936c.635.381 1.35.572 2.062.572s1.428-.191 2.062-.572l88-52.936c1.201-.723 1.938-2.023 1.938-3.428v-104c0-1.426-.76-2.744-1.992-3.459zm-90.008-42.98 80.085 46.47-52.95 31.289-23.135-13.607v-21.713c0-2.209-1.791-4-4-4s-4 1.791-4 4v21.713l-26.027 15.309c-1.223.719-1.973 2.029-1.973 3.447v29.795l-52 30.727v-94.688zm0 198.707-80.189-48.237 51.467-30.412 24.723 14.539v19.842c0 2.209 1.791 4 4 4s4-1.791 4-4v-19.842l26.027-15.307c1.223-.719 1.973-2.029 1.973-3.447v-31.667l52-30.728v94.729z"
                                />
                            </g>
                        </svg>
                    </li>
                    <li class="hover:text-green-500 cursor-pointer h-full xl:flex items-center text-sm text-white mr-10 tracking-normal hidden">Performance</li>
                    <li class="hover:text-green-500 cursor-pointer h-full xl:flex items-center text-sm text-white tracking-normal hidden">Deliverables</li>
                </ul>
                <div aria-haspopup="true" class="cursor-pointer h-full xl:flex items-center justify-end hidden relative" onclick="dropdownHandler(this)">
                    <ul class="p-2 w-40 border-r bg-white absolute rounded z-40 right-0 shadow mt-64 hidden">
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-green-700 focus:text-green-700 focus:outline-none">
                            <div class="flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" />
                                    <circle cx="12" cy="7" r="4" />
                                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                </svg>
                                <span class="ml-2">My Profile</span>
                            </div>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 focus:text-green-700 focus:outline-none flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <circle cx="12" cy="12" r="9" />
                                <line x1="12" y1="17" x2="12" y2="17.01" />
                                <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                            </svg>
                            <span class="ml-2">Help Center</span>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                <circle cx="12" cy="12" r="3" />
                            </svg>
                            <span class="ml-2">Account Settings</span>
                        </li>
                    </ul>
                    <div class="rounded">
                        <img class="rounded h-10 w-10 object-cover" src="https://tuk-cdn.s3.amazonaws.com/assets/components/boxed_layout/bl_1.png" alt="logo" />
                    </div>
                    <div class="text-gray-600 ml-2">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" />
                            <polyline points="6 9 12 15 18 9" />
                        </svg>
                    </div>
                </div>
                <div class="visible xl:hidden flex items-center">
                  <ul class="p-2 border-r bg-gray-50 absolute rounded top-0 left-0 right-0 shadow mt-16 md:mt-16 hidden">
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mt-2 py-3 hover:text-green-700 focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Dashboard</span>
                                    </div>
                                </li>
                                <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 focus:text-green-700 focus:outline-none flex justify-center"
                                    onclick="dropdownHandler(this)">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Products</span>
                                    </div>
                                    <ul class="ml-2 mt-3 hidden">
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Landing Pages
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Templates
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Components
                                        </li>
                                    </ul>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Performance</span>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mb-2 py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Deliverables</span>
                                </li>
                                <li>
                                    <hr class="border-b border-gray-300 w-full"/>
                                </li>
                                <li class="ml-2 cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <div class="w-12 cursor-pointer flex text-sm border-2 border-transparent rounded focus:outline-none focus:border-white transition duration-150 ease-in-out">
                                            <img class="rounded h-10 w-10 object-cover"
                                                 src="https://tuk-cdn.s3.amazonaws.com/assets/components/horizontal_navigation/hn_1.png"
                                                 alt="logo"/>
                                        </div>
                                        <p class="leading-6 text-base ml-1 cursor-pointer">Jane Doe</p>
                                        <div class="sm:ml-2 text-white relative">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 class="icon icon-tabler icon-tabler-chevron-down cursor-pointer"
                                                 width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor" fill="none" stroke-linecap="round"
                                                 stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                <polyline points="6 9 12 15 18 9"></polyline>
                                            </svg>
                                        </div>
                                    </div>
                                    <p class="leading-6 text-base ml-1 cursor-pointer">Jane Doe</p>
                                    <div class="sm:ml-2 text-white relative">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down cursor-pointer" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                        </svg>
                                    </div>
                                </li>

                            </ul>

                    <div id="menu" class="text-white" onclick="sidebarHandler(true)">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <line x1="4" y1="6" x2="20" y2="6" />
                            <line x1="4" y1="12" x2="20" y2="12" />
                            <line x1="4" y1="18" x2="20" y2="18" />
                        </svg>
                    </div>
                </div>
            </div>
        </nav>
        <div class="relative z-10 bg-gray-800 pt-8 pb-16">
            <div class="container px-6 mx-auto flex flex-col lg:flex-row items-start lg:items-center justify-between">
                <div>
                    <p class="flex items-center text-gray-300 text-xs">
                        <span class="cursor-pointer">Portal</span>
                        <span class="mx-2">&gt;</span>
                        <span class="cursor-pointer">Dashboard</span>
                        <span class="mx-2">&gt;</span>
                        <span class="cursor-pointer">KPIs</span>
                    </p>
                    <h4 class="text-2xl font-bold leading-tight text-white">Dashboard</h4>
                </div>
                <div class="mt-6 lg:mt-0">
                    <button class="focus:outline-none mr-3 bg-transparent transition duration-150 ease-in-out hover:bg-gray-700 rounded text-white px-5 py-2 text-sm border border-white">Back</button>
                    <button class="focus:outline-none transition duration-150 ease-in-out hover:bg-gray-200 border bg-white rounded text-green-700 px-8 py-2 text-sm">Edit Profile</button>
                </div>
            </div>
        </div>
        <div class="bg-gray-200 pb-10">
            <div class="container px-6 mx-auto">
                <div class="relative z-10 w-full">
                    <div class="w-full -mt-8 h-auto">
                        <div class="w-full h-auto lg:h-20 mb-6 rounded shadow bg-white">
                            <div class="lg:hidden bg-white w-full relative">
                                <div class="absolute inset-0 m-auto mr-4 z-0 w-6 h-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-selector" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#A0AEC0" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <polyline points="8 9 12 5 16 9" />
                                        <polyline points="16 15 12 19 8 15" />
                                    </svg>
                                </div>
                                <select aria-label="Selected tab" class="form-select block w-full p-3 border border-gray-300 rounded text-gray-600 appearance-none bg-transparent relative z-10">
                                    <option selected="" class="text-sm text-gray-600">Quarterly</option>
                                    <option class="text-sm text-gray-600">My Profile</option>
                                    <option class="text-sm text-gray-600">Forecasting</option>
                                    <option class="text-sm text-gray-600">Ratings</option>
                                    <option class="text-sm text-gray-600">History Sheet</option>
                                    <option class="text-sm text-gray-600">Account Settings</option>
                                </select>
                            </div>
                            <ul class="hidden lg:flex flex-row items-center h-full">
                                <li class="ml-4 my-2 lg:my-0 rounded text-base text-gray-800 px-4 py-2 bg-gray-200">Quarterly</li>
                                <li class="pl-10 my-0 text-base text-gray-600">My Profile</li>
                                <li class="pl-10 my-0 text-base text-gray-600">Forecasting</li>
                                <li class="pl-10 my-0 text-base text-gray-600">Ratings</li>
                                <li class="pl-10 my-0 text-base text-gray-600">History Sheet</li>
                                <li class="pl-10 my-0 text-base text-gray-600">Account Settings</li>
                            </ul>
                        </div>
                        <div class="container mx-auto h-64">
                    <div class="bg-gray-50 w-full min-h-full rounded shadow">
    <main class="bg-gray-50 dark:bg-gray-800 relative  overflow-hidden relative">
        <div class="flex items-start justify-between">
            <div class="w-full mx-4 py-6">
                    <div class="overflow-auto  pb-24 pt-2 pr-2 pl-2 md:pt-0 md:pr-0 md:pl-0">
                        <div class="flex flex-col flex-wrap sm:flex-row ">
                            <div class="w-full sm:w-1/2 xl:w-1/3">
                                <div class="mb-4">
                                    <div class="shadow-lg rounded-2xl p-4 bg-white dark:bg-gray-700 w-full">
                                        <div class="flex items-center justify-between mb-6">
                                            <div class="flex items-center">
                                                <span class="rounded-xl relative p-2 bg-blue-100">
                                                    <svg width="25" height="25" viewBox="0 0 256 262" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid">
                                                        <path d="M255.878 133.451c0-10.734-.871-18.567-2.756-26.69H130.55v48.448h71.947c-1.45 12.04-9.283 30.172-26.69 42.356l-.244 1.622 38.755 30.023 2.685.268c24.659-22.774 38.875-56.282 38.875-96.027" fill="#4285F4">
                                                        </path>
                                                        <path d="M130.55 261.1c35.248 0 64.839-11.605 86.453-31.622l-41.196-31.913c-11.024 7.688-25.82 13.055-45.257 13.055-34.523 0-63.824-22.773-74.269-54.25l-1.531.13-40.298 31.187-.527 1.465C35.393 231.798 79.49 261.1 130.55 261.1" fill="#34A853">
                                                        </path>
                                                        <path d="M56.281 156.37c-2.756-8.123-4.351-16.827-4.351-25.82 0-8.994 1.595-17.697 4.206-25.82l-.073-1.73L15.26 71.312l-1.335.635C5.077 89.644 0 109.517 0 130.55s5.077 40.905 13.925 58.602l42.356-32.782" fill="#FBBC05">
                                                        </path>
                                                        <path d="M130.55 50.479c24.514 0 41.05 10.589 50.479 19.438l36.844-35.974C195.245 12.91 165.798 0 130.55 0 79.49 0 35.393 29.301 13.925 71.947l42.211 32.783c10.59-31.477 39.891-54.251 74.414-54.251" fill="#EB4335">
                                                        </path>
                                                    </svg>
                                                </span>
                                                <div class="flex flex-col">
                                                    <span class="font-bold text-md text-black dark:text-white ml-2">
                                                        Google
                                                    </span>
                                                    <span class="text-sm text-gray-500 dark:text-white ml-2">
                                                        Google Inc.
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="flex items-center">
                                                <button class="border p-1 border-gray-200 rounded-full">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" class="w-4 h-4 text-yellow-500" fill="currentColor" viewBox="0 0 1792 1792">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z">
                                                        </path>
                                                    </svg>
                                                </button>
                                                <button class="text-gray-200">
                                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1088 1248v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68zm0-512v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68zm0-512v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68z">
                                                        </path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="flex items-center justify-between mb-4 space-x-12">
                                            <span class="px-2 py-1 flex items-center font-semibold text-xs rounded-md text-gray-500 bg-gray-200">
                                                PROGRESS
                                            </span>
                                            <span class="px-2 py-1 flex items-center font-semibold text-xs rounded-md text-red-400 border border-red-400  bg-white">
                                                HIGHT PRIORITY
                                            </span>
                                        </div>
                                        <div class="block m-auto">
                                            <div>
                                                <span class="text-sm inline-block text-gray-500 dark:text-gray-100">
                                                    Task done :
                                                    <span class="text-gray-700 dark:text-white font-bold">
                                                        25
                                                    </span>
                                                    /50
                                                </span>
                                            </div>
                                            <div class="w-full h-2 bg-gray-200 rounded-full mt-2">
                                                <div class="w-1/2 h-full text-center text-xs text-white bg-purple-500 rounded-full">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex items-center justify-start my-4 space-x-4">
                                            <span class="px-2 py-1 flex items-center text-xs rounded-md font-semibold text-green-500 bg-green-50">
                                                IOS APP
                                            </span>
                                            <span class="px-2 py-1 flex items-center text-xs rounded-md text-blue-500 font-semibold bg-blue-100">
                                                UI/UX
                                            </span>
                                        </div>
                                        <div class="flex -space-x-2">
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/1.jpg" alt="Guy"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/2.jpeg" alt="Max"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/3.jpg" alt="Charles"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/4.jpg" alt="Jade"/>
                                            </a>
                                        </div>
                                        <span class="px-2 py-1 flex w-36 mt-4 items-center text-xs rounded-md font-semibold text-yellow-500 bg-yellow-100">
                                            DUE DATE : 18 JUN
                                        </span>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <div class="shadow-lg rounded-2xl p-4 bg-white dark:bg-gray-700 w-full">
                                        <div class="flex items-center justify-between mb-6">
                                            <div class="flex items-center">
                                                <span class="rounded-xl relative p-2 bg-blue-100">
                                                    <svg width="25" height="25" viewBox="0 0 2447.6 2452.5" xmlns="http://www.w3.org/2000/svg">
                                                        <g clip-rule="evenodd" fill-rule="evenodd">
                                                            <path d="m897.4 0c-135.3.1-244.8 109.9-244.7 245.2-.1 135.3 109.5 245.1 244.8 245.2h244.8v-245.1c.1-135.3-109.5-245.1-244.9-245.3.1 0 .1 0 0 0m0 654h-652.6c-135.3.1-244.9 109.9-244.8 245.2-.2 135.3 109.4 245.1 244.7 245.3h652.7c135.3-.1 244.9-109.9 244.8-245.2.1-135.4-109.5-245.2-244.8-245.3z" fill="#36c5f0">
                                                            </path>
                                                            <path d="m2447.6 899.2c.1-135.3-109.5-245.1-244.8-245.2-135.3.1-244.9 109.9-244.8 245.2v245.3h244.8c135.3-.1 244.9-109.9 244.8-245.3zm-652.7 0v-654c.1-135.2-109.4-245-244.7-245.2-135.3.1-244.9 109.9-244.8 245.2v654c-.2 135.3 109.4 245.1 244.7 245.3 135.3-.1 244.9-109.9 244.8-245.3z" fill="#2eb67d">
                                                            </path>
                                                            <path d="m1550.1 2452.5c135.3-.1 244.9-109.9 244.8-245.2.1-135.3-109.5-245.1-244.8-245.2h-244.8v245.2c-.1 135.2 109.5 245 244.8 245.2zm0-654.1h652.7c135.3-.1 244.9-109.9 244.8-245.2.2-135.3-109.4-245.1-244.7-245.3h-652.7c-135.3.1-244.9 109.9-244.8 245.2-.1 135.4 109.4 245.2 244.7 245.3z" fill="#ecb22e">
                                                            </path>
                                                            <path d="m0 1553.2c-.1 135.3 109.5 245.1 244.8 245.2 135.3-.1 244.9-109.9 244.8-245.2v-245.2h-244.8c-135.3.1-244.9 109.9-244.8 245.2zm652.7 0v654c-.2 135.3 109.4 245.1 244.7 245.3 135.3-.1 244.9-109.9 244.8-245.2v-653.9c.2-135.3-109.4-245.1-244.7-245.3-135.4 0-244.9 109.8-244.8 245.1 0 0 0 .1 0 0" fill="#e01e5a">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                </span>
                                                <div class="flex flex-col">
                                                    <span class="font-bold text-md text-black dark:text-white ml-2">
                                                        Slack
                                                    </span>
                                                    <span class="text-sm text-gray-500 dark:text-white ml-2">
                                                        Slack corporation
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="flex items-center">
                                                <button class="border p-1 border-gray-200 rounded-full">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" class="w-4 h-4 text-yellow-500" fill="currentColor" viewBox="0 0 1792 1792">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z">
                                                        </path>
                                                    </svg>
                                                </button>
                                                <button class="text-gray-200">
                                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M1088 1248v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68zm0-512v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68zm0-512v192q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h192q40 0 68 28t28 68z">
                                                        </path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="flex items-center justify-between mb-4 space-x-12">
                                            <span class="px-2 py-1 flex items-center font-semibold text-xs rounded-md text-green-700 bg-green-50">
                                                COMPLETED
                                            </span>
                                            <span class="px-2 py-1 flex items-center font-semibold text-xs rounded-md text-green-600 border border-green-600 bg-white">
                                                MEDIUM PRIORITY
                                            </span>
                                        </div>
                                        <div class="block m-auto">
                                            <div>
                                                <span class="text-sm inline-block text-gray-500 dark:text-gray-100">
                                                    Task done :
                                                    <span class="text-gray-700 dark:text-white font-bold">
                                                        50
                                                    </span>
                                                    /50
                                                </span>
                                            </div>
                                            <div class="w-full h-2 bg-gray-200 rounded-full mt-2">
                                                <div class="w-full h-full text-center text-xs text-white bg-pink-400 rounded-full">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex items-center justify-start my-4 space-x-4">
                                            <span class="px-2 py-1 flex items-center text-xs rounded-md font-semibold text-green-500 bg-green-50">
                                                IOS APP
                                            </span>
                                            <span class="px-2 py-1 flex items-center text-xs rounded-md text-yellow-600 font-semibold bg-yellow-200">
                                                ANDROID
                                            </span>
                                        </div>
                                        <div class="flex -space-x-2">
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/1.jpg" alt="Guy"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/2.jpeg" alt="Max"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/3.jpg" alt="Charles"/>
                                            </a>
                                            <a href="#" class="">
                                                <img class="inline-block h-10 w-10 rounded-full object-cover ring-2 ring-white" src="/images/person/4.jpg" alt="Jade"/>
                                            </a>
                                        </div>
                                        <span class="px-2 py-1 flex w-36 mt-4 items-center text-xs rounded-md font-semibold text-yellow-500 bg-yellow-100">
                                            DUE DATE : 18 JUN
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-full sm:w-1/2 xl:w-1/3">
                                <div class="mb-4 mx-0 sm:ml-4 xl:mr-4">
                                    <div class="shadow-lg rounded-2xl bg-white dark:bg-gray-700 w-full">
                                        <p class="font-bold text-md p-4 text-black dark:text-white">
                                            My Tasks
                                            <span class="text-sm text-gray-500 dark:text-gray-300 dark:text-white ml-2">
                                                (05)
                                            </span>
                                        </p>
                                        <ul>
                                            <li class="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        01
                                                    </span>
                                                    <span>
                                                        Create wireframe
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" class="mx-4 text-gray-400 dark:text-gray-300" viewBox="0 0 1024 1024">
                                                    <path d="M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0 0 51.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z" fill="currentColor">
                                                    </path>
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        02
                                                    </span>
                                                    <span>
                                                        Dashboard design
                                                    </span>
                                                    <span class="lg:ml-6 ml-2 flex items-center text-gray-400 dark:text-gray-300">
                                                        3
                                                        <svg width="15" height="15" fill="currentColor" class="ml-1" viewBox="0 0 512 512">
                                                            <path d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2l-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29c7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1l-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160s-93.3 160-208 160z" fill="currentColor">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                    <span class="mx-4 flex items-center text-gray-400 dark:text-gray-300">
                                                        3
                                                        <svg width="15" height="15" class="ml-1" fill="currentColor" viewBox="0 0 384 512">
                                                            <path d="M384 144c0-44.2-35.8-80-80-80s-80 35.8-80 80c0 36.4 24.3 67.1 57.5 76.8c-.6 16.1-4.2 28.5-11 36.9c-15.4 19.2-49.3 22.4-85.2 25.7c-28.2 2.6-57.4 5.4-81.3 16.9v-144c32.5-10.2 56-40.5 56-76.3c0-44.2-35.8-80-80-80S0 35.8 0 80c0 35.8 23.5 66.1 56 76.3v199.3C23.5 365.9 0 396.2 0 432c0 44.2 35.8 80 80 80s80-35.8 80-80c0-34-21.2-63.1-51.2-74.6c3.1-5.2 7.8-9.8 14.9-13.4c16.2-8.2 40.4-10.4 66.1-12.8c42.2-3.9 90-8.4 118.2-43.4c14-17.4 21.1-39.8 21.6-67.9c31.6-10.8 54.4-40.7 54.4-75.9zM80 64c8.8 0 16 7.2 16 16s-7.2 16-16 16s-16-7.2-16-16s7.2-16 16-16zm0 384c-8.8 0-16-7.2-16-16s7.2-16 16-16s16 7.2 16 16s-7.2 16-16 16zm224-320c8.8 0 16 7.2 16 16s-7.2 16-16 16s-16-7.2-16-16s7.2-16 16-16z" fill="currentColor">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" class="mx-4 text-gray-400 dark:text-gray-300" viewBox="0 0 1024 1024">
                                                    <path d="M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0 0 51.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z" fill="currentColor">
                                                    </path>
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        03
                                                    </span>
                                                    <span>
                                                        Components card
                                                    </span>
                                                    <span class="lg:ml-6 ml-2 flex items-center text-gray-400 dark:text-gray-300">
                                                        3
                                                        <svg width="15" height="15" fill="currentColor" class="ml-1" viewBox="0 0 512 512">
                                                            <path d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2l-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29c7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1l-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160s-93.3 160-208 160z" fill="currentColor">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" class="mx-4 text-gray-400 dark:text-gray-300" viewBox="0 0 1024 1024">
                                                    <path d="M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0 0 51.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z" fill="currentColor">
                                                    </path>
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-400 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        04
                                                    </span>
                                                    <span class="line-through">
                                                        Google logo design
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" viewBox="0 0 1024 1024" class="text-green-500 mx-4">
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8l157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-400  justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        05
                                                    </span>
                                                    <span class="line-through">
                                                        Header navigation
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" viewBox="0 0 1024 1024" class="text-green-500 mx-4">
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8l157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        06
                                                    </span>
                                                    <span>
                                                        International
                                                    </span>
                                                    <span class="lg:ml-6 ml-2 flex items-center text-gray-400 dark:text-gray-300">
                                                        3
                                                        <svg width="15" height="15" fill="currentColor" class="ml-1" viewBox="0 0 512 512">
                                                            <path d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2l-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29c7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1l-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160s-93.3 160-208 160z" fill="currentColor">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                    <span class="mx-4 flex items-center text-gray-400 dark:text-gray-300">
                                                        3
                                                        <svg width="15" height="15" class="ml-1" fill="currentColor" viewBox="0 0 384 512">
                                                            <path d="M384 144c0-44.2-35.8-80-80-80s-80 35.8-80 80c0 36.4 24.3 67.1 57.5 76.8c-.6 16.1-4.2 28.5-11 36.9c-15.4 19.2-49.3 22.4-85.2 25.7c-28.2 2.6-57.4 5.4-81.3 16.9v-144c32.5-10.2 56-40.5 56-76.3c0-44.2-35.8-80-80-80S0 35.8 0 80c0 35.8 23.5 66.1 56 76.3v199.3C23.5 365.9 0 396.2 0 432c0 44.2 35.8 80 80 80s80-35.8 80-80c0-34-21.2-63.1-51.2-74.6c3.1-5.2 7.8-9.8 14.9-13.4c16.2-8.2 40.4-10.4 66.1-12.8c42.2-3.9 90-8.4 118.2-43.4c14-17.4 21.1-39.8 21.6-67.9c31.6-10.8 54.4-40.7 54.4-75.9zM80 64c8.8 0 16 7.2 16 16s-7.2 16-16 16s-16-7.2-16-16s7.2-16 16-16zm0 384c-8.8 0-16-7.2-16-16s7.2-16 16-16s16 7.2 16 16s-7.2 16-16 16zm224-320c8.8 0 16 7.2 16 16s-7.2 16-16 16s-16-7.2-16-16s7.2-16 16-16z" fill="currentColor">
                                                            </path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" class="mx-4 text-gray-400 dark:text-gray-300" viewBox="0 0 1024 1024">
                                                    <path d="M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0 0 51.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z" fill="currentColor">
                                                    </path>
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                            <li class="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3">
                                                <div class="flex items-center justify-start text-sm">
                                                    <span class="mx-4">
                                                        07
                                                    </span>
                                                    <span>
                                                        Production data
                                                    </span>
                                                </div>
                                                <svg width="20" height="20" fill="currentColor" class="mx-4 text-gray-400 dark:text-gray-300" viewBox="0 0 1024 1024">
                                                    <path d="M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0 0 51.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z" fill="currentColor">
                                                    </path>
                                                    <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="currentColor">
                                                    </path>
                                                </svg>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mb-4 sm:ml-4 xl:mr-4">
                                    <div class="shadow-lg rounded-2xl bg-white dark:bg-gray-700 w-full">
                                        <div class="flex items-center p-4 justify-between">
                                            <p class="font-bold text-md text-black dark:text-white">
                                                Google
                                            </p>
                                            <button class="text-sm p-1 text-gray-400 border rounded border-gray-400 mr-4">
                                                <svg width="15" height="15" fill="currentColor" viewBox="0 0 20 20">
                                                    <g fill="none">
                                                        <path d="M17.222 8.685a1.5 1.5 0 0 1 0 2.628l-10 5.498A1.5 1.5 0 0 1 5 15.496V4.502a1.5 1.5 0 0 1 2.223-1.314l10 5.497z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="py-2 px-4 bg-blue-100 dark:bg-gray-800 text-gray-600 border-l-4 border-blue-500 flex items-center justify-between">
                                            <p class="text-xs flex items-center dark:text-white">
                                                <svg width="20" height="20" fill="currentColor" class="text-blue-500 mr-2" viewBox="0 0 24 24">
                                                    <g fill="none">
                                                        <path d="M12 5a8.5 8.5 0 1 1 0 17a8.5 8.5 0 0 1 0-17zm0 3a.75.75 0 0 0-.743.648l-.007.102v4.5l.007.102a.75.75 0 0 0 1.486 0l.007-.102v-4.5l-.007-.102A.75.75 0 0 0 12 8zm7.17-2.877l.082.061l1.149 1a.75.75 0 0 1-.904 1.193l-.081-.061l-1.149-1a.75.75 0 0 1 .903-1.193zM14.25 2.5a.75.75 0 0 1 .102 1.493L14.25 4h-4.5a.75.75 0 0 1-.102-1.493L9.75 2.5h4.5z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                                Create wireframe
                                            </p>
                                            <div class="flex items-center">
                                                <span class="font-bold text-xs dark:text-gray-200 mr-2 ml-2 md:ml-4">
                                                    25 min 20s
                                                </span>
                                                <button class="text-sm p-1 text-gray-400 border rounded bg-blue-500 mr-4">
                                                    <svg width="17" height="17" fill="currentColor" viewBox="0 0 24 24" class="text-white">
                                                        <g fill="none">
                                                            <path d="M9 6a1 1 0 0 1 1 1v10a1 1 0 1 1-2 0V7a1 1 0 0 1 1-1zm6 0a1 1 0 0 1 1 1v10a1 1 0 1 1-2 0V7a1 1 0 0 1 1-1z" fill="currentColor">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="flex items-center p-4 justify-between border-b-2 border-gray-100">
                                            <p class="font-bold text-md text-black dark:text-white">
                                                Slack
                                            </p>
                                            <button class="text-sm p-1 text-gray-400 border rounded border-gray-400 mr-4">
                                                <svg width="15" height="15" fill="currentColor" viewBox="0 0 20 20">
                                                    <g fill="none">
                                                        <path d="M17.222 8.685a1.5 1.5 0 0 1 0 2.628l-10 5.498A1.5 1.5 0 0 1 5 15.496V4.502a1.5 1.5 0 0 1 2.223-1.314l10 5.497z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="py-2 px-4 text-gray-600 flex items-center justify-between border-b-2 border-gray-100">
                                            <p class="text-xs flex items-center dark:text-white">
                                                <svg width="20" height="20" fill="currentColor" class="mr-2" viewBox="0 0 24 24">
                                                    <g fill="none">
                                                        <path d="M12 5a8.5 8.5 0 1 1 0 17a8.5 8.5 0 0 1 0-17zm0 3a.75.75 0 0 0-.743.648l-.007.102v4.5l.007.102a.75.75 0 0 0 1.486 0l.007-.102v-4.5l-.007-.102A.75.75 0 0 0 12 8zm7.17-2.877l.082.061l1.149 1a.75.75 0 0 1-.904 1.193l-.081-.061l-1.149-1a.75.75 0 0 1 .903-1.193zM14.25 2.5a.75.75 0 0 1 .102 1.493L14.25 4h-4.5a.75.75 0 0 1-.102-1.493L9.75 2.5h4.5z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                                International
                                            </p>
                                            <div class="flex items-center">
                                                <span class="text-xs text-gray-400 mr-2 ml-2 md:ml-4">
                                                    30 min
                                                </span>
                                                <button class="text-sm p-1 text-gray-400 border rounded border-gray-400 mr-4">
                                                    <svg width="15" height="15" fill="currentColor" viewBox="0 0 20 20">
                                                        <g fill="none">
                                                            <path d="M17.222 8.685a1.5 1.5 0 0 1 0 2.628l-10 5.498A1.5 1.5 0 0 1 5 15.496V4.502a1.5 1.5 0 0 1 2.223-1.314l10 5.497z" fill="currentColor">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="py-2 px-4 text-gray-600 flex items-center justify-between border-b-2 border-gray-100">
                                            <p class="text-xs flex items-center dark:text-white">
                                                <svg width="20" height="20" fill="currentColor" class="mr-2" viewBox="0 0 24 24">
                                                    <g fill="none">
                                                        <path d="M12 5a8.5 8.5 0 1 1 0 17a8.5 8.5 0 0 1 0-17zm0 3a.75.75 0 0 0-.743.648l-.007.102v4.5l.007.102a.75.75 0 0 0 1.486 0l.007-.102v-4.5l-.007-.102A.75.75 0 0 0 12 8zm7.17-2.877l.082.061l1.149 1a.75.75 0 0 1-.904 1.193l-.081-.061l-1.149-1a.75.75 0 0 1 .903-1.193zM14.25 2.5a.75.75 0 0 1 .102 1.493L14.25 4h-4.5a.75.75 0 0 1-.102-1.493L9.75 2.5h4.5z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                                Slack logo design
                                            </p>
                                            <div class="flex items-center">
                                                <span class="text-xs text-gray-400 mr-2 ml-2 md:ml-4">
                                                    30 min
                                                </span>
                                                <button class="text-sm p-1 text-gray-400 border rounded border-gray-400 mr-4">
                                                    <svg width="15" height="15" fill="currentColor" viewBox="0 0 20 20">
                                                        <g fill="none">
                                                            <path d="M17.222 8.685a1.5 1.5 0 0 1 0 2.628l-10 5.498A1.5 1.5 0 0 1 5 15.496V4.502a1.5 1.5 0 0 1 2.223-1.314l10 5.497z" fill="currentColor">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="py-2 px-4 text-gray-600 flex items-center justify-between">
                                            <p class="text-xs flex items-center dark:text-white">
                                                <svg width="20" height="20" fill="currentColor" class="mr-2" viewBox="0 0 24 24">
                                                    <g fill="none">
                                                        <path d="M12 5a8.5 8.5 0 1 1 0 17a8.5 8.5 0 0 1 0-17zm0 3a.75.75 0 0 0-.743.648l-.007.102v4.5l.007.102a.75.75 0 0 0 1.486 0l.007-.102v-4.5l-.007-.102A.75.75 0 0 0 12 8zm7.17-2.877l.082.061l1.149 1a.75.75 0 0 1-.904 1.193l-.081-.061l-1.149-1a.75.75 0 0 1 .903-1.193zM14.25 2.5a.75.75 0 0 1 .102 1.493L14.25 4h-4.5a.75.75 0 0 1-.102-1.493L9.75 2.5h4.5z" fill="currentColor">
                                                        </path>
                                                    </g>
                                                </svg>
                                                Dahboard template
                                            </p>
                                            <div class="flex items-center">
                                                <span class="text-xs text-gray-400 mr-2 ml-2 md:ml-4">
                                                    30 min
                                                </span>
                                                <button class="text-sm p-1 text-gray-400 border rounded border-gray-400 mr-4">
                                                    <svg width="15" height="15" fill="currentColor" viewBox="0 0 20 20">
                                                        <g fill="none">
                                                            <path d="M17.222 8.685a1.5 1.5 0 0 1 0 2.628l-10 5.498A1.5 1.5 0 0 1 5 15.496V4.502a1.5 1.5 0 0 1 2.223-1.314l10 5.497z" fill="currentColor">
                                                            </path>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-full sm:w-1/2 xl:w-1/3">
                                <div class="mb-4">
                                    <div class="shadow-lg rounded-2xl p-4 bg-white dark:bg-gray-700">
                                        <div class="flex flex-wrap overflow-hidden">
                                            <div class="w-full rounded shadow-sm">
                                                <div class="flex items-center justify-between mb-4">
                                                    <div class="text-left font-bold text-xl text-black dark:text-white">
                                                        Dec 2021
                                                    </div>
                                                    <div class="flex space-x-4">
                                                        <button class="p-2 rounded-full bg-blue-500 text-white">
                                                            <svg width="15" height="15" fill="currentColor" viewBox="0 0 24 24">
                                                                <path fill="currentColor" d="M13.83 19a1 1 0 0 1-.78-.37l-4.83-6a1 1 0 0 1 0-1.27l5-6a1 1 0 0 1 1.54 1.28L10.29 12l4.32 5.36a1 1 0 0 1-.78 1.64z">
                                                                </path>
                                                            </svg>
                                                        </button>
                                                        <button class="p-2 rounded-full bg-blue-500 text-white">
                                                            <svg width="15" height="15" fill="currentColor" viewBox="0 0 24 24">
                                                                <path fill="currentColor" d="M10 19a1 1 0 0 1-.64-.23a1 1 0 0 1-.13-1.41L13.71 12L9.39 6.63a1 1 0 0 1 .15-1.41a1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z">
                                                                </path>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="-mx-2">
                                                    <table class="w-full dark:text-white">
                                                        <tr>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                S
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                M
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                T
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                W
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                T
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                F
                                                            </th>
                                                            <th class="py-3 px-2 md:px-3 ">
                                                                S
                                                            </th>
                                                        </tr>
                                                        <tr class="text-gray-400 dark:text-gray-500">
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                25
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                26
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                27
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                28
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                29
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-gray-300 dark:text-gray-500">
                                                                30
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center text-gray-800 cursor-pointer">
                                                                1
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                2
                                                            </td>
                                                            <td class="py-3 relative px-1  hover:text-blue-500 text-center cursor-pointer">
                                                                3
                                                                <span class="absolute rounded-full h-2 w-2 bg-blue-500 bottom-0 left-1/2 transform -translate-x-1/2">
                                                                </span>
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                4
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                5
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                6
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                7
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3 md:px-2 relative lg:px-3 hover:text-blue-500 text-center cursor-pointer">
                                                                8
                                                                <span class="absolute rounded-full h-2 w-2 bg-yellow-500 bottom-0 left-1/2 transform -translate-x-1/2">
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                9
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                10
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                11
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                12
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  text-center text-white cursor-pointer">
                                                                <span class="p-2 rounded-full bg-blue-500">
                                                                    13
                                                                </span>
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                14
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                15
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                16
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                17
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                18
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                19
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                20
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                21
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                22
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                23
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                24
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 relative text-center cursor-pointer">
                                                                25
                                                                <span class="absolute rounded-full h-2 w-2 bg-red-500 bottom-0 left-1/2 transform -translate-x-1/2">
                                                                </span>
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                26
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                27
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                28
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                29
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                30
                                                            </td>
                                                            <td class="py-3 px-2 md:px-3  hover:text-blue-500 text-center cursor-pointer">
                                                                31
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <div class="shadow-lg rounded-2xl p-4 bg-white dark:bg-gray-700 w-full">
                                        <p class="font-bold text-md text-black dark:text-white">
                                            Messages
                                        </p>
                                        <ul>
                                            <li class="flex items-center my-6 space-x-2">
                                                <a href="#" class="block relative">
                                                    <img alt="profil" src="/images/person/1.jpg" class="mx-auto object-cover rounded-full h-10 w-10 "/>
                                                </a>
                                                <div class="flex flex-col">
                                                    <span class="text-sm text-gray-900 font-semibold dark:text-white ml-2">
                                                        Charlie Rabiller
                                                    </span>
                                                    <span class="text-sm text-gray-400 dark:text-gray-300 ml-2">
                                                        Hey John ! Do you read the NextJS doc ?
                                                    </span>
                                                </div>
                                            </li>
                                            <li class="flex items-center my-6 space-x-2">
                                                <a href="#" class="block relative">
                                                    <img alt="profil" src="/images/person/5.jpg" class="mx-auto object-cover rounded-full h-10 w-10 "/>
                                                </a>
                                                <div class="flex flex-col">
                                                    <span class="text-sm text-gray-900 font-semibold dark:text-white ml-2">
                                                        Marie Lou
                                                    </span>
                                                    <span class="text-sm text-gray-400 dark:text-gray-300 ml-2">
                                                        No I think the dog is better...
                                                    </span>
                                                </div>
                                            </li>
                                            <li class="flex items-center my-6 space-x-2">
                                                <a href="#" class="block relative">
                                                    <img alt="profil" src="/images/person/6.jpg" class="mx-auto object-cover rounded-full h-10 w-10 "/>
                                                </a>
                                                <div class="flex flex-col">
                                                    <span class="text-sm text-gray-900 font-semibold dark:text-white ml-2">
                                                        Ivan Buck
                                                    </span>
                                                    <span class="text-sm text-gray-400 dark:text-gray-300 ml-2">
                                                        Seriously ? haha Bob is not a children !
                                                    </span>
                                                </div>
                                            </li>
                                            <li class="flex items-center my-6 space-x-2">
                                                <a href="#" class="block relative">
                                                    <img alt="profil" src="/images/person/7.jpg" class="mx-auto object-cover rounded-full h-10 w-10 "/>
                                                </a>
                                                <div class="flex flex-col">
                                                    <span class="text-sm text-gray-900 font-semibold dark:text-white ml-2">
                                                        Marina Farga
                                                    </span>
                                                    <span class="text-sm text-gray-400 dark:text-gray-300 ml-2">
                                                        Do you need that deisgn ?
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                    </div>


  )
}

