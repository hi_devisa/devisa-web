export default function SignupAlt() {
  return (
<div class="container mx-auto">
  <nav class="flex py-8 pb-6 border-b-2 border-gray-300 mb-8">
    <div class="flex w-full items-center">
      <div class="w-1/5">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" class="h-8" viewBox="0 0 273 64"><path fill="url(#paint0_linear)" fill-rule="evenodd" d="M32 16c-7.2 0-11.7 3.6-13.5 10.8 2.7-3.6 5.85-4.95 9.45-4.05 2.054.5135 3.5221 2.0036 5.1471 3.6531C35.7443 29.0901 38.8081 32.2 45.5 32.2c7.2 0 11.7-3.6 13.5-10.8-2.7 3.6-5.85 4.95-9.45 4.05-2.054-.5135-3.5221-2.0036-5.1471-3.6531C41.7557 19.1099 38.6919 16 32 16zM18.5 32.2C11.3 32.2 6.8 35.8 5 43c2.7-3.6 5.85-4.95 9.45-4.05 2.054.5135 3.5221 2.0036 5.1471 3.6531C22.2443 45.2901 25.3081 48.4 32 48.4c7.2 0 11.7-3.6 13.5-10.8-2.7 3.6-5.85 4.95-9.45 4.05-2.054-.5135-3.5221-2.0036-5.1471-3.6531C28.2557 35.3099 25.1919 32.2 18.5 32.2z" clip-rule="evenodd"/><path fill="#2D3748" fill-rule="evenodd" d="M85.996 29.652h-4.712v9.12c0 2.432 1.596 2.394 4.712 2.242V44.7c-6.308.76-8.816-.988-8.816-5.928v-9.12h-3.496V25.7h3.496v-5.104l4.104-1.216v6.32h4.712v3.952zm17.962-3.952h4.104v19h-4.104v-2.736c-1.444 2.014-3.686 3.23-6.65 3.23-5.168 0-9.462-4.37-9.462-9.994 0-5.662 4.294-9.994 9.462-9.994 2.964 0 5.206 1.216 6.65 3.192V25.7zm-6.004 15.58c3.42 0 6.004-2.546 6.004-6.08 0-3.534-2.584-6.08-6.004-6.08-3.42 0-6.004 2.546-6.004 6.08 0 3.534 2.584 6.08 6.004 6.08zm16.948-18.43c-1.444 0-2.622-1.216-2.622-2.622 0-1.444 1.178-2.622 2.622-2.622s2.622 1.178 2.622 2.622c0 1.406-1.178 2.622-2.622 2.622zM112.85 44.7v-19h4.104v19h-4.104zm8.854 0V16.96h4.104V44.7h-4.104zm30.742-19h4.332l-5.966 19h-4.028l-3.952-12.806-3.99 12.806h-4.028l-5.966-19h4.332l3.686 13.11 3.99-13.11h3.914l3.952 13.11 3.724-13.11zm9.424-2.85c-1.444 0-2.622-1.216-2.622-2.622 0-1.444 1.178-2.622 2.622-2.622s2.622 1.178 2.622 2.622c0 1.406-1.178 2.622-2.622 2.622zm-2.052 21.85v-19h4.104v19h-4.104zm18.848-19.494c4.256 0 7.296 2.888 7.296 7.828V44.7h-4.104V33.452c0-2.888-1.672-4.408-4.256-4.408-2.698 0-4.826 1.596-4.826 5.472V44.7h-4.104v-19h4.104v2.432c1.254-1.976 3.306-2.926 5.89-2.926zm26.752-7.106h4.104v26.6h-4.104v-2.736c-1.444 2.014-3.686 3.23-6.65 3.23-5.168 0-9.462-4.37-9.462-9.994 0-5.662 4.294-9.994 9.462-9.994 2.964 0 5.206 1.216 6.65 3.192V18.1zm-6.004 23.18c3.42 0 6.004-2.546 6.004-6.08 0-3.534-2.584-6.08-6.004-6.08-3.42 0-6.004 2.546-6.004 6.08 0 3.534 2.584 6.08 6.004 6.08zm23.864 3.914c-5.738 0-10.032-4.37-10.032-9.994 0-5.662 4.294-9.994 10.032-9.994 3.724 0 6.954 1.938 8.474 4.902l-3.534 2.052c-.836-1.786-2.698-2.926-4.978-2.926-3.344 0-5.89 2.546-5.89 5.966 0 3.42 2.546 5.966 5.89 5.966 2.28 0 4.142-1.178 5.054-2.926l3.534 2.014c-1.596 3.002-4.826 4.94-8.55 4.94zm15.314-14.25c0 3.458 10.222 1.368 10.222 8.398 0 3.8-3.306 5.852-7.41 5.852-3.8 0-6.536-1.71-7.752-4.446l3.534-2.052c.608 1.71 2.128 2.736 4.218 2.736 1.824 0 3.23-.608 3.23-2.128 0-3.382-10.222-1.482-10.222-8.284 0-3.572 3.078-5.814 6.954-5.814 3.116 0 5.7 1.444 7.03 3.952l-3.458 1.938c-.684-1.482-2.014-2.166-3.572-2.166-1.482 0-2.774.646-2.774 2.014zm17.518 0c0 3.458 10.222 1.368 10.222 8.398 0 3.8-3.306 5.852-7.41 5.852-3.8 0-6.536-1.71-7.752-4.446l3.534-2.052c.608 1.71 2.128 2.736 4.218 2.736 1.824 0 3.23-.608 3.23-2.128 0-3.382-10.222-1.482-10.222-8.284 0-3.572 3.078-5.814 6.954-5.814 3.116 0 5.7 1.444 7.03 3.952l-3.458 1.938c-.684-1.482-2.014-2.166-3.572-2.166-1.482 0-2.774.646-2.774 2.014z" clip-rule="evenodd"/><defs><linearGradient id="paint0_linear" x1="3.5" x2="59" y1="16" y2="48" gradientUnits="userSpaceOnUse"><stop stop-color="#2298BD"/><stop offset="1" stop-color="#0ED7B5"/></linearGradient></defs></svg>
      </div>

      <div class="w-2/5 flex items-center">
        <a class="flex mr-8" href="#">
          <span class="w-6 h-6 mr-1 text-blue-600">
            <i class="fas fa-list"></i>
          </span>

          <span class="font-semibold">
            Services
          </span>
        </a>

        <a class="flex text-gray-500 mr-8" href="#">
          <span class="w-6 h-6 mr-1">
            <i class="fas fa-cog"></i>
          </span>

          <span class="font-semibold">
            Settings
          </span>
        </a>

        <a class="flex text-gray-500" href="#">
          <span class="w-6 h-6 mr-1">
            <i class="fas fa-credit-card"></i>
          </span>

          <span class="font-semibold">
            Billing
          </span>
        </a>
      </div>

      <div class="flex w-2/5 justify-end">
        <a class="text-blue-700 mr-3 pr-3 border-r border-gray-400" href="#">
          <span class="font-semibold">
            100 Credits
          </span>
        </a>

        <a class="flex items-center" href="#">
          <span class="pr-2">
            <img class="rounded-full border" src="https://i.pravatar.cc/24?img=24" alt="Username" />
          </span>

          <span>Logout</span>
        </a>
      </div>
    </div>
  </nav>

  <div class="flex">
    <div class="w-1/5">
      <h5 class="text-sm font-semibold uppercase mb-4">
        Settings
      </h5>

      <ul class="text-sm">
        <li class="mb-2">
          <a class="flex" href="#">
            <span class="text-blue-700 w-6 h-6 mr-1">
              <i class="fas fa-user"></i>
            </span>

            <span class="font-semibold">
              My Profile
            </span>
          </a>
        </li>

        <li class="mb-2">
          <a class="flex text-gray-500" href="#">
            <span class="w-6 h-6 mr-1">
              <i class="fas fa-lock"></i>
            </span>

            <span class="font-semibold">
              Security
            </span>
          </a>
        </li>

        <li class="mb-2">
          <a class="flex text-gray-500 group" href="#">
            <span class="w-6 h-6 mr-1">
              <i class="fas fa-volume-up"></i>
            </span>

            <span class="font-semibold">
              Notifications
            </span>
          </a>
        </li>

        <li>
          <a class="flex text-gray-500" href="#">
            <span class="w-6 h-6 mr-1">
              <i class="fas fa-cloud"></i>
            </span>

            <span class="font-semibold">
              API Access
            </span>
          </a>
        </li>
      </ul>
    </div>

    <div class="w-4/5 bg-white shadow border-t-4 border-blue-600">
      <div class="py-4 px-6 border-b border-gray-400">
        <h1 class="text-lg font-semibold uppercase">My Profile</h1>
      </div>

      <div class="p-6">
        ...
      </div>
    </div>
  </div>
</div>
)
}
