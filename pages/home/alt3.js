export default function Alt3() {
    return (
        <div>
<div class="text-gray-700 bg-white border-t border-b body-font">
            <div class="flex flex-col flex-wrap p-5 mx-auto md:items-center md:flex-row">
                <a href="./index.html" class="pr-2 lg:pr-8 lg:px-6 focus:outline-none">
                    <div class="inline-flex items-center">
                        <div class="w-2 h-2 p-2 mr-2 rounded-full bg-gradient-to-tr from-cyan-400 to-lightBlue-500">
                        </div>
                        <h2
                            class="font-semibold tracking-tighter transition duration-1000 ease-in-out transform text-blueGray-500 dark:text-blueGray-200 lg:text-md text-bold lg:mr-8">
                            Wicked Blocks
                        </h2>
                    </div>
                </a>
                <nav class="flex flex-wrap items-center justify-center text-base ">
                    <a href="#"
                        class="mr-5 text-sm font-semibold text-gray-600 lg:ml-24 hover:text-gray-800">Pricing</a>
                    <a href="#" class="mr-5 text-sm font-semibold text-gray-600 hover:text-gray-800">Contact</a>
                    <a href="#" class="mr-5 text-sm font-semibold text-gray-600 hover:text-gray-800">Services</a>
                    <a href="#" class="mr-5 text-sm font-semibold text-gray-600 hover:text-gray-800">Now</a>
                </nav>
                <div class="flex ml-auto">
                    <button
                        class="items-center px-8 py-2 ml-auto font-semibold text-white transition duration-500 ease-in-out transform bg-black rounded-lg hover:bg-blueGray-900 focus:ring focus:outline-none">Button</button>
                    <button
                        class="items-center px-8 py-2 mt-4 ml-5 font-semibold text-black transition duration-500 ease-in-out transform bg-white border rounded-lg lg:inline-flex lg:mt-px hover:border-black0 hover:bg-black hover:text-white focus:ring focus:outline-none">Button
                        <svg class="hidden lg:block" fill="none" stroke="currentColor" stroke-linecap="round"
                            stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-1" viewBox="0 0 24 24">
                            <path d="M5 12h14M12 5l7 7-7 7"></path>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <div
            class="container w-full p-20 m-4 mx-auto my-16 text-center bg-white border-2 border-dashed border-blueGray-300 h-96 rounded-xl">
            <p class="mt-20 italic tracking-tighter text-md text-blueGray-500 title-font">



                    <section class="text-gray-700 body-font">
            <div class="container flex flex-col items-center px-5 py-16 mx-auto lg:px-20 lg:py-24 md:flex-row">
                <div class="flex flex-col items-center w-full pt-0 mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:items-start md:text-left md:mb-0 lg:text-center">
                    <h1 class="mb-8 text-2xl font-bold tracking-tighter text-center text-black lg:text-left lg:text-5xl title-font">
                        Medium lenght display headline.
                    </h1>
                    <p class="mb-8 text-base leading-relaxed text-center text-gray-700 lg:text-left lg:text-1xl">
                        Deploy your mvp in minutes, not days. WT offers you a a wide selection swapable sections for
                        your landing page.
                    </p>
                    <div class="flex justify-center">
                        <input class="flex-grow w-full px-4 py-2 mb-4 mr-4 text-base text-black transition duration-1000 ease-in-out transform rounded-lg bg-blueGray-200 focus:outline-none focus:border-purple-500 sm:mb-0 focus:bg-white focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"
                               placeholder="Your Email"
                               type="email"/>
                        <button
                                class="flex items-center px-6 py-2 mt-auto font-semibold text-white transition duration-500 ease-in-out transform bg-black rounded-lg hover:bg-gray-900 focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2">
                            Action
                        </button>
                    </div>
                    <p class="w-full mt-2 mb-8 text-sm text-left text-gray-600">
                        Neutra shabby chic ramps, viral fixie.
                    </p>
                </div>
                <div class="w-5/6 lg:max-w-lg lg:w-full md:w-1/2">
                    <img class="object-cover object-center rounded-lg "
                         alt="hero"
                         src="https://dummyimage.com/720x600/F3F4F7/8693ac"/>
                </div>
            </div>
        </section>


            </p>
        </div>
        </div>
    )
}
