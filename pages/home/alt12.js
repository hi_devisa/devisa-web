import UserCard from '../../components/user/card'
import Comments from '../../components/user/comments'
import Expandable from '../../components/ui/button/expandable'
import Link from 'next/link'
import Nav from '../../components/ui/nav'
import { useSession } from 'next-auth/client'
export default function SignupAlt() {
  const [ session, loading ] = useSession()
  return (
      <div class="overflow-hidden">
<div class="bg-white border-b w-full h-16 absolute top-0 left-0 flex pl-64 overflow-hidden shadow">

  <Nav session={session}/>
  <div class="flex-1 text-sm text-gray-800 h-full flex items-center px-4 font-normal tracking-wide">Dashboard</div>

  <div class="flex">
    <div class="border-l w-16 flex items-center justify-center text-gray-800">
      <svg class="w-6 h-6 fill-current" viewBox="0 0 512 512"><path d="m256 512c-141.164062 0-256-114.835938-256-256s114.835938-256 256-256 256 114.835938 256 256-114.835938 256-256 256zm0-480c-123.519531 0-224 100.480469-224 224s100.480469 224 224 224 224-100.480469 224-224-100.480469-224-224-224zm0 0"/><path d="m346.667969 181.332031h-181.335938c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h181.335938c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path d="m346.667969 272h-181.335938c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h181.335938c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/><path d="m346.667969 362.667969h-181.335938c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h181.335938c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0"/></svg>
    </div>
  </div>
</div>

<div class="bg-white border-r w-64 h-screen   left-0 overflow-hidden fixed">

  <a href="#" class="block w-full h-16 border-b flex items-center justify-center text-xl font-bold text-gray-800">
                    <img src="/logo/satgrn.png"
                        width = "72"
                        height = "72"/>
                    <div class="rounded-full relative p-0 flex justify-end text-black text-lg">
                        <p class="subpixel-antialiased hover:text-limegreen-400 transition duration-200"><Link href="/">Devisa&nbsp;&nbsp;</Link></p>
                    </div>
  </a>

    <ul>
    <li><a href="#" class="flex items-center tracking-wide font-normal text-sm h-12 text-gray-700 hover:text-black">
      <svg class="w-5 h-5 fill-current mx-3" viewBox="0 0 512 512"><path d="m197.332031 170.667969h-160c-20.585937 0-37.332031-16.746094-37.332031-37.335938v-96c0-20.585937 16.746094-37.332031 37.332031-37.332031h160c20.589844 0 37.335938 16.746094 37.335938 37.332031v96c0 20.589844-16.746094 37.335938-37.335938 37.335938zm-160-138.667969c-2.941406 0-5.332031 2.390625-5.332031 5.332031v96c0 2.945313 2.390625 5.335938 5.332031 5.335938h160c2.945313 0 5.335938-2.390625 5.335938-5.335938v-96c0-2.941406-2.390625-5.332031-5.335938-5.332031zm0 0"/><path d="m197.332031 512h-160c-20.585937 0-37.332031-16.746094-37.332031-37.332031v-224c0-20.589844 16.746094-37.335938 37.332031-37.335938h160c20.589844 0 37.335938 16.746094 37.335938 37.335938v224c0 20.585937-16.746094 37.332031-37.335938 37.332031zm-160-266.667969c-2.941406 0-5.332031 2.390625-5.332031 5.335938v224c0 2.941406 2.390625 5.332031 5.332031 5.332031h160c2.945313 0 5.335938-2.390625 5.335938-5.332031v-224c0-2.945313-2.390625-5.335938-5.335938-5.335938zm0 0"/><path d="m474.667969 512h-160c-20.589844 0-37.335938-16.746094-37.335938-37.332031v-96c0-20.589844 16.746094-37.335938 37.335938-37.335938h160c20.585937 0 37.332031 16.746094 37.332031 37.335938v96c0 20.585937-16.746094 37.332031-37.332031 37.332031zm-160-138.667969c-2.945313 0-5.335938 2.390625-5.335938 5.335938v96c0 2.941406 2.390625 5.332031 5.335938 5.332031h160c2.941406 0 5.332031-2.390625 5.332031-5.332031v-96c0-2.945313-2.390625-5.335938-5.332031-5.335938zm0 0"/><path d="m474.667969 298.667969h-160c-20.589844 0-37.335938-16.746094-37.335938-37.335938v-224c0-20.585937 16.746094-37.332031 37.335938-37.332031h160c20.585937 0 37.332031 16.746094 37.332031 37.332031v224c0 20.589844-16.746094 37.335938-37.332031 37.335938zm-160-266.667969c-2.945313 0-5.335938 2.390625-5.335938 5.332031v224c0 2.945313 2.390625 5.335938 5.335938 5.335938h160c2.941406 0 5.332031-2.390625 5.332031-5.335938v-224c0-2.941406-2.390625-5.332031-5.332031-5.332031zm0 0"/></svg>
      Dashboard</a>


    <a href="#" class="flex items-center tracking-wide font-normal text-sm h-12 text-white bg-green-500 rounded-r-lg my-2 shadow-xl">
      <svg class="w-5 h-5 fill-current mx-3" viewBox="-21 0 512 512"><path d="m389.332031 160c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m389.332031 512c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m80 336c-44.097656 0-80-35.882812-80-80s35.902344-80 80-80 80 35.882812 80 80-35.902344 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m135.703125 240.425781c-5.570313 0-10.988281-2.902343-13.910156-8.0625-4.375-7.679687-1.707031-17.453125 5.972656-21.824219l197.953125-112.855468c7.65625-4.414063 17.449219-1.726563 21.800781 5.976562 4.375 7.679688 1.707031 17.449219-5.972656 21.824219l-197.953125 112.851563c-2.496094 1.40625-5.203125 2.089843-7.890625 2.089843zm0 0"/><path d="m333.632812 416.425781c-2.6875 0-5.398437-.683593-7.894531-2.109375l-197.953125-112.855468c-7.679687-4.371094-10.34375-14.144532-5.972656-21.824219 4.351562-7.699219 14.125-10.367188 21.804688-5.972657l197.949218 112.851563c7.679688 4.375 10.347656 14.144531 5.976563 21.824219-2.945313 5.183594-8.363281 8.085937-13.910157 8.085937zm0 0"/></svg>
      Incentives</a>
  </li>


  <li><a href="#" class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:text-black">
     <svg class="w-5 h-5 fill-current mx-3" viewBox="-43 0 512 512"><path d="m16 512c-8.832031 0-16-7.167969-16-16v-480c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v480c0 8.832031-7.167969 16-16 16zm0 0"/><path d="m240 277.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c2.945312 0 5.332031-2.386719 5.332031-5.332031v-202.667969c0-2.941406-2.386719-5.332031-5.332031-5.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c20.585938 0 37.332031 16.746094 37.332031 37.332031v202.667969c0 20.585938-16.746093 37.332031-37.332031 37.332031zm0 0"/><path d="m389.332031 362.667969h-213.332031c-20.585938 0-37.332031-16.746094-37.332031-37.335938v-64c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v64c0 2.902344 2.429687 5.335938 5.332031 5.335938h213.332031c2.902344 0 5.335938-2.433594 5.335938-5.335938v-202.664062c0-2.902344-2.433594-5.335938-5.335938-5.335938h-128c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h128c20.589844 0 37.335938 16.746094 37.335938 37.335938v202.664062c0 20.589844-16.746094 37.335938-37.335938 37.335938zm0 0"/></svg>
      Customers</a>
  </li>

  </ul>
    <div class="font-bold uppercase px-3 text-xs text-gray-600 tracking-wider mt-8 mb-4">Team Settings</div>


    <a href="#" class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:text-black">
      <svg class="w-5 h-5 fill-current mx-3" viewBox="-21 0 512 512"><path d="m389.332031 160c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m389.332031 512c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m80 336c-44.097656 0-80-35.882812-80-80s35.902344-80 80-80 80 35.882812 80 80-35.902344 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m135.703125 240.425781c-5.570313 0-10.988281-2.902343-13.910156-8.0625-4.375-7.679687-1.707031-17.453125 5.972656-21.824219l197.953125-112.855468c7.65625-4.414063 17.449219-1.726563 21.800781 5.976562 4.375 7.679688 1.707031 17.449219-5.972656 21.824219l-197.953125 112.851563c-2.496094 1.40625-5.203125 2.089843-7.890625 2.089843zm0 0"/><path d="m333.632812 416.425781c-2.6875 0-5.398437-.683593-7.894531-2.109375l-197.953125-112.855468c-7.679687-4.371094-10.34375-14.144532-5.972656-21.824219 4.351562-7.699219 14.125-10.367188 21.804688-5.972657l197.949218 112.851563c7.679688 4.375 10.347656 14.144531 5.976563 21.824219-2.945313 5.183594-8.363281 8.085937-13.910157 8.085937zm0 0"/></svg>
      Team Profile</a>


    <a href="#" class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:text-black">
     <svg class="w-5 h-5 fill-current mx-3" viewBox="-43 0 512 512"><path d="m16 512c-8.832031 0-16-7.167969-16-16v-480c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v480c0 8.832031-7.167969 16-16 16zm0 0"/><path d="m240 277.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c2.945312 0 5.332031-2.386719 5.332031-5.332031v-202.667969c0-2.941406-2.386719-5.332031-5.332031-5.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c20.585938 0 37.332031 16.746094 37.332031 37.332031v202.667969c0 20.585938-16.746093 37.332031-37.332031 37.332031zm0 0"/><path d="m389.332031 362.667969h-213.332031c-20.585938 0-37.332031-16.746094-37.332031-37.335938v-64c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v64c0 2.902344 2.429687 5.335938 5.332031 5.335938h213.332031c2.902344 0 5.335938-2.433594 5.335938-5.335938v-202.664062c0-2.902344-2.433594-5.335938-5.335938-5.335938h-128c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h128c20.589844 0 37.335938 16.746094 37.335938 37.335938v202.664062c0 20.589844-16.746094 37.335938-37.335938 37.335938zm0 0"/></svg>
      Team Members</a>

    <div class="font-bold uppercase px-3 text-xs text-gray-600 tracking-wider mt-8 mb-4">Team Billing</div>


    <a href="#" class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:text-black">
      <svg class="w-5 h-5 fill-current mx-3" viewBox="-21 0 512 512"><path d="m389.332031 160c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m389.332031 512c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m80 336c-44.097656 0-80-35.882812-80-80s35.902344-80 80-80 80 35.882812 80 80-35.902344 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m135.703125 240.425781c-5.570313 0-10.988281-2.902343-13.910156-8.0625-4.375-7.679687-1.707031-17.453125 5.972656-21.824219l197.953125-112.855468c7.65625-4.414063 17.449219-1.726563 21.800781 5.976562 4.375 7.679688 1.707031 17.449219-5.972656 21.824219l-197.953125 112.851563c-2.496094 1.40625-5.203125 2.089843-7.890625 2.089843zm0 0"/><path d="m333.632812 416.425781c-2.6875 0-5.398437-.683593-7.894531-2.109375l-197.953125-112.855468c-7.679687-4.371094-10.34375-14.144532-5.972656-21.824219 4.351562-7.699219 14.125-10.367188 21.804688-5.972657l197.949218 112.851563c7.679688 4.375 10.347656 14.144531 5.976563 21.824219-2.945313 5.183594-8.363281 8.085937-13.910157 8.085937zm0 0"/></svg>
      Subscription</a>


    <a href="#" class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:text-black">
     <svg class="w-5 h-5 fill-current mx-3" viewBox="-43 0 512 512"><path d="m16 512c-8.832031 0-16-7.167969-16-16v-480c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v480c0 8.832031-7.167969 16-16 16zm0 0"/><path d="m240 277.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c2.945312 0 5.332031-2.386719 5.332031-5.332031v-202.667969c0-2.941406-2.386719-5.332031-5.332031-5.332031h-224c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h224c20.585938 0 37.332031 16.746094 37.332031 37.332031v202.667969c0 20.585938-16.746093 37.332031-37.332031 37.332031zm0 0"/><path d="m389.332031 362.667969h-213.332031c-20.585938 0-37.332031-16.746094-37.332031-37.335938v-64c0-8.832031 7.167969-16 16-16s16 7.167969 16 16v64c0 2.902344 2.429687 5.335938 5.332031 5.335938h213.332031c2.902344 0 5.335938-2.433594 5.335938-5.335938v-202.664062c0-2.902344-2.433594-5.335938-5.335938-5.335938h-128c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h128c20.589844 0 37.335938 16.746094 37.335938 37.335938v202.664062c0 20.589844-16.746094 37.335938-37.335938 37.335938zm0 0"/></svg>
      Payment Methods</a>
  </div>

<div class="w-full min-h-screen bg-gray-100 pt-16 pl-64">


  <div class="bg-gray-100">
  <div class="bg-white shadow">
    <div class="container mx-auto px-6">
      <div class="flex items-center justify-between py-5">
        <div>
          <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-2">
            <i class="fas fa-angle-left"></i>
            <span>Backend Developer</span>
           </a>
          <div class="flex items-center">
            <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
            <h1 class="px-2 text-2xl text-gray-900 font-extrabold">Ricardo Cooper</h1>
          </div>
        </div>
        <div class="flex items-center">
          <button class="h-9 border border-gray-200 px-3 py-2 text-sm font-medium rounded text-gray-700 shadow">Disqualify</button>
          <button class="h-9 ml-3 px-3 py-2 text-sm font-medium rounded bg-green-600 text-white shadow">Advance to Offer</button>
        </div>
      </div>
    </div>
  </div>
  <div class="container mx-auto p-6">
    <div class="flex -mx-2">
      <div class="w-3/5 px-2">
        <div class="mb-6">
          <h2 class="text-lg font-medium text-gray-900 pb-6">Overview</h2>
          <div class="bg-white rounded shadow overflow-hidden">
            <div class="px-6 py-5">
              <dl>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Application For</dt>
                    <dd class="text-base font-normal text-gray-900">Backend Developer</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Salary Expectation</dt>
                    <dd class="text-base font-normal text-gray-900">$120,000</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Email</dt>
                    <dd class="text-base font-normal text-gray-900">richardocooper@example.com</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Phone</dt>
                    <dd class="text-base font-normal text-gray-900">+1 555-555-5555</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-full">
                    <dt class="text-sm font-medium text-gray-500">Attachments</dt>
                    <dd class="mt-2">
                      <ul class="border border-gray-200 rounded">
                        <li class="flex items-center px-4 py-3">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            resume_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                        <li class="flex items-center px-4 py-3 border-t border-gray-200">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            cover_letter_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
            </div>
            <div>
              <button class="flex items-center justify-center w-full px-6 py-2 bg-gray-100">
                <span class="text-sm font-medium text-gray-500 mr-2">Read full application</span>
                <i class="fas fa-angle-down"></i>
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2 class="text-lg font-medium text-gray-900 pb-6">Notes</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div class="flex border-b">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="flex">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="bg-gray-100 p-6">
              <label for="note" class="text-sm font-semibold block">Add a note</label>
              <textarea name="note" id="note" class="w-full rounded-lg border"></textarea>
              <div class="flex justify-end">
                <button class="text-white bg-green-600 py-2 px-3 rounded">Post Note</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="w-2/5 px-2">
        <h2 class="text-lg font-medium text-gray-900 pb-6">Profile</h2>
        <div class="bg-white rounded shadow overflow-hidden text-gray-600 mb-6">

<div class="bg-white shadow overflow-hidden sm:rounded-lg">
  <div class="px-4 py-5 sm:px-6 grid grid-cols-2">
    <h3 class="text-lg leading-6 font-medium text-gray-900">
      Applicant Information
    </h3>
            <div class="float-right flex align-top  md:justify-end">
                <Link href="/">
                  <img class="object-cover w-16 h-16 border-2 border-green-500 rounded-full dark:border-green-400" alt="Testimonial avatar" src="/lightart.jpg"/>
                </Link>
            </div>
    <p class="mt-1 max-w-2xl text-sm text-gray-500">
      Personal details and application.
    </p>
  </div>
  <div class="border-t border-gray-200">
    <dl>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Full name
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Margot Foster
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Application for
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Backend Developer
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Email address
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          margotfoster@example.com
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Salary expectation
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          $120,000
        </dd>
      </div>
      <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          About
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
        </dd>
      </div>
      <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt class="text-sm font-medium text-gray-500">
          Attachments
        </dt>
        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <ul class="border border-gray-200 rounded-md divide-y divide-gray-200">
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  resume_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
            <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
              <div class="w-0 flex-1 flex items-center">
                <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M8 4a3 3 0 00-3 3v4a5 5 0 0010 0V7a1 1 0 112 0v4a7 7 0 11-14 0V7a5 5 0 0110 0v4a3 3 0 11-6 0V7a1 1 0 012 0v4a1 1 0 102 0V7a3 3 0 00-3-3z" clip-rule="evenodd" />
                </svg>
                <span class="ml-2 flex-1 w-0 truncate">
                  coverletter_back_end_developer.pdf
                </span>
              </div>
              <div class="ml-4 flex-shrink-0">
                <a href="#" class="font-medium text-green-600 hover:text-green-500">
                  Download
                </a>
              </div>
            </li>
          </ul>
        </dd>
      </div>
    </dl>
  </div>
</div>
        </div>

        <h2 class="text-lg font-medium text-gray-900 pb-6">Timeline</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div>
              <div class="flex pt-6">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Applied</span> to <span class="font-semibold text-gray-900">Front-end Developer</span>
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to phone screening by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-check-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Completed</span> phone screening with Martha Gardner
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to interview by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4">
                  <span class="font-semibold text-gray-900">Completed</span> interview with Katherine Snyden
                </div>
                <div class="w-1/6 pb-4">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex bg-white px-6 py-4 border-t">
                <button class="w-full justify-center px-6 py-2 bg-green-600 rounded">
                  <span class="text-white">Advance to Offer</span>
                </button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
  <div class="p-8 text-sm text-gray-800">
    <h1 class="text-4xl text-gray-700 font-bold leading-none mb-8">Parkway Incentive</h1>
          <Comments/>


      <table class="border w-full text-left shadow-sm">
        <thead>
          <tr>
            <th class="p-3 text-xs text-gray-900 uppercase font-bold tracking-wide"></th>
            <th class="p-3 text-xs text-gray-900 uppercase font-bold tracking-wide">Customer</th>
            <th class="p-3 text-xs text-gray-900 uppercase font-bold tracking-wide">Member</th>
            <th class="p-3 text-xs text-gray-900 uppercase font-bold tracking-wide"></th>
          </tr>
        </thead>
        <tbody class="border rounded bg-white">
          <tr>
            <td class="p-3 border" width="50px"></td>
            <td class="p-3 border">John Smith</td>
            <td class="p-3 border">Terry Jones</td>
            <td class="p-3 border"></td>
          </tr>
          <tr>
            <td class="p-3 border"></td>
            <td class="p-3 border">John Smith</td>
            <td class="p-3 border">Terry Jones</td>
            <td class="p-3 border"></td>
          </tr>
          <tr>
            <td class="p-3 border"></td>
            <td class="p-3 border">John Smith</td>
            <td class="p-3 border">Terry Jones</td>
            <td class="p-3 border"></td>
          </tr>
          <tr>
            <td class="p-3 border"></td>
            <td class="p-3 border">John Smith</td>
            <td class="p-3 border">Terry Jones</td>
            <td class="p-3 border"></td>
          </tr>
        </tbody>
      </table>


  </div>

</div>
    </div>
         )
         }
