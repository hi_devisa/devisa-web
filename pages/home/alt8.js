export default function Alt4() {
    return (
<div class="bg-gray-100">
  <div class="bg-white shadow">
    <div class="container mx-auto px-6">
      <div class="flex items-center justify-between py-5">
        <div>
          <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-2">
            <i class="fas fa-angle-left"></i>
            <span>Backend Developer</span>
           </a>
          <div class="flex items-center">
            <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
            <h1 class="px-2 text-2xl text-gray-900 font-extrabold">Ricardo Cooper</h1>
          </div>
        </div>
        <div class="flex items-center">
          <button class="h-9 border border-gray-200 px-3 py-2 text-sm font-medium rounded text-gray-700 shadow">Disqualify</button>
          <button class="h-9 ml-3 px-3 py-2 text-sm font-medium rounded bg-indigo-600 text-white shadow">Advance to Offer</button>
        </div>
      </div>
    </div>
  </div>
  <div class="container mx-auto p-6">
    <div class="flex -mx-2">
      <div class="w-3/5 px-2">
        <div class="mb-6">
          <h2 class="text-lg font-medium text-gray-900 pb-6">Overview</h2>
          <div class="bg-white rounded shadow overflow-hidden">
            <div class="px-6 py-5">
              <dl>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Application For</dt>
                    <dd class="text-base font-normal text-gray-900">Backend Developer</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Salary Expectation</dt>
                    <dd class="text-base font-normal text-gray-900">$120,000</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Email</dt>
                    <dd class="text-base font-normal text-gray-900">richardocooper@example.com</dd>
                  </div>
                  <div class="w-1/2 mb-6">
                    <dt class="text-sm font-medium text-gray-500">Phone</dt>
                    <dd class="text-base font-normal text-gray-900">+1 555-555-5555</dd>
                  </div>
                </div>
                <div class="flex">
                  <div class="w-full">
                    <dt class="text-sm font-medium text-gray-500">Attachments</dt>
                    <dd class="mt-2">
                      <ul class="border border-gray-200 rounded">
                        <li class="flex items-center px-4 py-3">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            resume_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                        <li class="flex items-center px-4 py-3 border-t border-gray-200">
                          <i class="fas fa-paperclip"></i>
                          <a href="" class="ml-3 text-base text-gray-900">
                            cover_letter_backend_developer.pdf
                          </a>
                          <a href="" class="ml-auto text-sm font-medium text-gray-900">
                            Download
                          </a>
                        </li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
            </div>
            <div>
              <button class="flex items-center justify-center w-full px-6 py-2 bg-gray-100">
                <span class="text-sm font-medium text-gray-500 mr-2">Read full application</span>
                <i class="fas fa-angle-down"></i>
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2 class="text-lg font-medium text-gray-900 pb-6">Notes</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div class="flex border-b">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="flex">
              <div class="w-1/6 p-6">
                <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
              </div>
              <div class="w-5/6 py-5 pr-5">
                <span class="font-semibold text-gray-900 block">Emilia Gates</span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aut a neque optio provident temporibus nam rerum dolor hic vero et
                <span class="block text-sm mt-1">4 days ago &middot; <a href="">Reply</a></span>
              </div>
            </div>
            <div class="bg-gray-100 p-6">
              <label for="note" class="text-sm font-semibold block">Add a note</label>
              <textarea name="note" id="note" class="w-full rounded-lg border"></textarea>
              <div class="flex justify-end">
                <button class="text-white bg-indigo-600 py-2 px-3 rounded">Post Note</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="w-2/5 px-2">
        <h2 class="text-lg font-medium text-gray-900 pb-6">Timeline</h2>
          <div class="bg-white rounded shadow overflow-hidden text-gray-600">
            <div>
              <div class="flex pt-6">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-user-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Applied</span> to <span class="font-semibold text-gray-900">Front-end Developer</span>
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to phone screening by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Aug 26</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-check-circle"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Completed</span> phone screening with Martha Gardner
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4 mb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4 mb-4 border-b">
                  <span class="font-semibold text-gray-900">Advanced</span> to interview by Bethany Blake
                </div>
                <div class="w-1/6 pb-4 mb-4 border-b">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex">
                <div class="w-1/6 pb-4">
                  <i class="far fa-arrow-alt-circle-right"></i>
                </div>
                <div class="w-4/6 pb-4">
                  <span class="font-semibold text-gray-900">Completed</span> interview with Katherine Snyden
                </div>
                <div class="w-1/6 pb-4">
                  <span class="text-sm text-gray-500">Sep 2</span>
                </div>
              </div>
              <div class="flex bg-white px-6 py-4 border-t">
                <button class="w-full justify-center px-6 py-2 bg-indigo-600 rounded">
                  <span class="text-white">Advance to Offer</span>
                </button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
                    )
                    }
