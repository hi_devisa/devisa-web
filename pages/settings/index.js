import Card from '../../components/card'
import AltModal from '../../components/ui/modal/alt'
import Switch from '../../components/ui/switch'
import Layout from '../../components/layout'
import PageHeader from '../../components/ui/pageHeader'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'

export default function Settings() {
  const [ session, loading ] = useSession()
  const router = useRouter()
  const handleModal = (event) => {
    event.preventDefault()

  }
  return (
    <>
      <Layout session={session} title="Settings">
        <PageHeader/>
        <button onClick={handleModal}>modal</button>
        <Card/>
      </Layout>
    </>
  )
}
