import Head from 'next/head'
import { getCsrfToken } from 'next-auth/client'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab, faGoogle, faLinkedin, faFacebook, faTwitter, faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons'
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import {
  useSession, signIn, signOut, csrfToken
} from 'next-auth/client'

export default function Login({ csrfToken }) {
  const [username, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [ session, loading ] = useSession()
  const handleLogin = (e) => {
    e.preventDefault()
    if (e.target.id == 'github') {
      signIn('github')
    } else if (e.target.id == 'google') {
      signIn('google')
    } else if (e.target.id == 'twitter') {
      signIn('twitter')
    } else if (e.target.id == 'credentials') {
      signIn('credentials', {
        username,
        password,
        callbackUrl: `${window.location.origin}/account`
      })
    }
  }
  // const [showPasswordInput, setShowPasswordInput] = useState(false);
  // const [loading, setLoading] = useState(false);
  // const [message, setMessage] = useState({ type: '', content: '' });
  const router = useRouter();
  if (!session) {
    return (
        <div class="body-bg min-h-screen pt-6 md:pt-5 pb-6 px-2 md:px-0 font-sans bg-green-500">
          <Head>
            <title>Login - Devisa</title>
          </Head>
      <header class="max-w-lg mx-auto pt-5">
              <h1 class="text-4xl font-bold text-gray-50 text-center transition-opacity from-transparent"><Link href="/" class="hover:text-gray-200 transition duration-200">Devisa</Link><span class="text-green-600"> Login</span></h1>
      </header>

      <main class="bg-white max-w-lg mx-auto p-3 md:p-8 mt-10 rounded-lg shadow-2xl overflow-y-hidden">
          <section class="text-center">
              <h4 class="font-medium text-3xl text-gray-600 mb-10">Welcome back.</h4>
          </section>
              <div class="flex items-center justify-between mt-4">
                  <span class="w-1/5 border-b dark:border-gray-600 lg:w-1/4"></span>

                  <span class="text-xs text-center text-gray-500 uppercase dark:text-gray-400 hover:underline">Login with external service</span>

                  <span class="w-1/5 border-b dark:border-gray-400 lg:w-1/4"></span>
              </div>


        <div class="grid grid-cols-2 gap-2 space-y-1">

          <button
            onClick = {() => signIn("google")}
            id = "google"
            class="col-span-2 w-full bg-indigo-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faGoogle}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  Sign in with Google</span>
          </button>

          <button
            onClick = {() => signIn("facebook")}
            id = "facebook"
            class="w-full bg-blue-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faFacebook}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  Facebok</span>
              </button>

          <button
            onClick = {() => signIn("linkedin")}
            id = "linkedin"
            class="w-full bg-blue-900 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faLinkedin}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  LinkedIn</span>
              </button>
          <button
            onClick = {() => signIn("gitlab")}
            id = "gitlab"
            class="w-full bg-yellow-500 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-blue-600 hover:shadow-lg dark:hover:bg-gray-600">
                  <span class="text-white mr-25"><FontAwesomeIcon icon={faGitlab}/></span>
                  <span class="w-5/6 px-4 py-3 font-bold text-center">
                  GitLab</span>
              </button>
              <button
                onClick={() => signIn("github")}
                id = "github"
                class="w-full bg-gray-700 flex items-center justify-center mt-4 text-white rounded-lg shadow-md dark:bg-gray-700 dark:text-gray-200 hover:bg-gray-800 dark:hover:bg-gray-600 hover:shadow-lg">

                <span class="text-white mr-25"><FontAwesomeIcon icon={faGithub}/></span>
                <span class="w-5/6 px-4 py-3 font-bold text-center">GitHub</span>
              </button>

        </div>

              <div class="flex items-center justify-between mt-10">
                  <span class="w-1/5 border-b dark:border-gray-600 lg:w-1/4"></span>

                  <span class="text-xs text-center text-gray-500 uppercase dark:text-gray-400 hover:underline">or login with email</span>

                  <span class="w-1/5 border-b dark:border-gray-400 lg:w-1/4"></span>
              </div>

            <form method="POST" action="/api/auth/callback/credentials">
              <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
              <div class="2">
                  <label class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200" for="LoggingEmailAddress">Email Address</label>
                  <input
                    value={username}
                    type="text"
                    id="LoggingUsername"
                    class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-green-500 dark:focus:border-green-500 focus:outline-none focus:ring"/>
              </div>

              <div class="mt-4">
                  <div class="flex justify-between">
                      <label class="block mb-2 text-sm font-medium text-gray-600 dark:text-gray-200" for="loggingPassword">Password</label>
                      <span class="text-xs text-green-500 dark:text-gray-300 hover:underline"><Link href="#">Forgot Password?</Link></span>
                  </div>
                  <input
                    value={password}
                    id="password"
                    class="block w-full px-4 py-2 text-gray-700 bg-white border border-gray-300 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-green-500 dark:focus:border-green-500 focus:outline-none focus:ring"
                    type="password"/>
              </div>

              <div class="mt-8">
              <Link href="#">
                  <span class="flex items-center justify-center mt-4 text-white transition-colors duration-200 transform bg-green-400 rounded-lg shadow-md hover:shadow-lg hover:bg-green-500 transition duration-200 focus:outline-none focus:bg-green-500 dark:bg-gray-700 dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-600">
                  <button
                    type="submit"
                    class="w-full px-4 py-3 font-bold text-center">Login with username</button>
              </span>
              </Link>
              </div>
              </form>
      <div class="max-w-lg mx-auto text-center mt-3 mb-0">
          <p class="text-gray-600 text-sm">Don't have an account? <span class=" text-green-600 font-bold hover:underline"><Link href="/signup">Sign up</Link></span>.</p>
      </div>


      </main>


      <footer class="max-w-lg mx-auto flex justify-center text-white">
      </footer>
  </div>
      )

  } else {
    router.push('/')
    return (
      <>
      <h1>You are already signed in!</h1>
        <p>Click <Link href="/">here</Link> if you are not automatically redirected</p>
      </>
    )
  }
}

// Login.getInitialProps = async (context) => {
//   return {
//     csrfToken: await csrfToken(context)
//   }
// }

function submit(email, password) {
    // event.preventDefault()
  // const email = event.target[0].value
  // const password = event.target[1].value

  alert("Email: " + email + ", pass: " + password)
  // supabase.auth
  //   .signIn({email, password})
  //   .then((response) => {
  //     document.querySelector('#access-token').value = response.data.access_token
  //     document.querySelector('#refresh-token').value = response.data.refresh_token
  //     alert("Logged in as " + response.user.email)
  //   })
  //   .catch((err) => {
  //     alert(err.response.text)
  //   })
}


/* export async function getServerSideProps(context) {
  return {
    props: {
      csrfToken: await getCsrfToken(context)
    }
  }
}

 */
