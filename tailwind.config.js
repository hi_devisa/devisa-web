const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false,
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    plugin(function({ addComponents }) {
      const buttons = {
        '.btn': {
          padding: '.5rem 1rem',
          borderRadius: '.25rem',
          fontWeight: '600',
        },
        '.btn-green': {
          backgroundColor: '#80fe76',
          color: '#309e16',
          '&:hover': {
            backgroundColor: '#9afe8c'
          },
        },
        '.btn-red': {
          backgroundColor: '#e3342f',
          color: '#fff',
          '&:hover': {
            backgroundColor: '#cc1f1a'
          },
        },
      }
      addComponents(buttons)
    })
  ],
  theme: {
    extend: {
      maxWidth: {
        '8xl': '1920px'
      },
      boxShadow: {
        'outline-2': '0 0 0 2px var(--accents-2)',
        magical:
          'rgba(0, 0, 0, 0.02) 0px 30px 30px, rgba(0, 0, 0, 0.03) 0px 0px 8px, rgba(0, 0, 0, 0.05) 0px 1px 0px'
      },
      lineHeight: {
        'extra-loose': '2.2'
      },
      letterSpacing: {
        widest: '0.3em'
      },
      colors: {
        brand: '#71FE61',
        limegreen: {
          '50':  '#f7fbf7',
          '100': '#f1fae6',
          '200': '#ddf5b6',
          '300': '#baec78',
          '400': '#69d934',
          '500': '#2ec213',
          '600': '#1fa60b',
          '700': '#21850e',
          '800': '#1e6412',
          '900': '#194e13',
        },
        green: {
          '50':  '#e8f8f6',
          '100': '#c9f7ef',
          '200': '#95f3db',
          '300': '#53ebc3',
          '400': '#09efab',
          '500': '#30dbad',
          '600': '#10bb8d',
          '700': '#10887d',
          '800': '#10585d',
          '900': '#10382d',
        },
        seagreen: {
          '50':  '#f2f8f7',
          '100': '#e5f7ee',
          '200': '#c6f0d8',
          '300': '#a7e2bd',
          '400': '#4acf8e',
          '500': '#1fb460',
          '600': '#199646',
          '700': '#1e783e',
          '800': '#1f5c38',
          '900': '#1b4a30',
        },
        pacific: {
          '50':  '#eef9f9',
          '100': '#d7f7f6',
          '200': '#aaefeb',
          '300': '#6fe3e1',
          '400': '#2aced0',
          '500': '#0eb2ba',
          '600': '#0c949e',
          '700': '#12777f',
          '800': '#145c61',
          '900': '#134a4e',
        },
        orchid: {
          '50':  '#f6f7fa',
          '100': '#f0eef8',
          '200': '#e2d8f4',
          '300': '#d2bcf0',
          '400': '#be91eb',
          '500': '#a965e7',
          '600': '#8a44d9',
          '700': '#6a35b9',
          '800': '#522b8c',
          '900': '#41256c',
        },
        cerulean: {
          '50':  '#f1f9fb',
          '100': '#dcf7f9',
          '200': '#b0edf4',
          '300': '#7bdeef',
          '400': '#37c3e9',
          '500': '#15a1df',
          '600': '#107fcc',
          '700': '#1465a7',
          '800': '#154d7b',
          '900': '#133e5f',
        },
         hotpink: {
          '50':  '#fbf8f9',
          '100': '#fbedf6',
          '200': '#f7d0ee',
          '300': '#f4abe2',
          '400': '#f576d0',
          '500': '#f54abb',
          '600': '#e92d9b',
          '700': '#c2237a',
          '800': '#941d5a',
          '900': '#731946',
        },
        tomato: {
          '50':  '#fcf8f6',
          '100': '#fcf0ec',
          '200': '#f9d8d7',
          '300': '#f8b7b2',
          '400': '#f7857a',
          '500': '#f85a4d',
          '600': '#ee3932',
          '700': '#cc2b2e',
          '800': '#a0232b',
          '900': '#7e1d25',
        },
        denim: {
          '50':  '#f3f9fc',
          '100': '#e2f6fb',
          '200': '#bde8f7',
          '300': '#91d5f5',
          '400': '#53b3f2',
          '500': '#288bee',
          '600': '#1d67e3',
          '700': '#1e51c2',
          '800': '#1c3f92',
          '900': '#183370',
        },
        royalblue: {
          '50':  '#f4f8fb',
          '100': '#e9f1fb',
          '200': '#d0dcf9',
          '300': '#b3c3f7',
          '400': '#8d99f6',
          '500': '#656cf5',
          '600': '#4d4bee',
          '700': '#403bd6',
          '800': '#332fa8',
          '900': '#2a2884',
        },
        flamingo: {
          '50':  '#f9f6f9',
          '100': '#f6ebf8',
          '200': '#eecef4',
          '300': '#e8acef',
          '400': '#e57bea',
          '500': '#e24fe5',
          '600': '#cc32d6',
          '700': '#a027b4',
          '800': '#782187',
          '900': '#5e1d68',
        },
        cerise: {
          '50':  '#fcf8f8',
          '100': '#fceff4',
          '200': '#f9d2e8',
          '300': '#f7add6',
          '400': '#f877b8',
          '500': '#f84b97',
          '600': '#ef2e71',
          '700': '#cc235a',
          '800': '#9d1d45',
          '900': '#7b1937',
        },
        mango: {
          '50':  '#fbf7f0',
          '100': '#fbf0dd',
          '200': '#f8dfb6',
          '300': '#f6c57a',
          '400': '#f39c3b',
          '500': '#f2731b',
          '600': '#e54f11',
          '700': '#c23b15',
          '800': '#9a2f1a',
          '900': '#7b2619',
        },
        orange: {
          '50':  '#faf7ea',
          '100': '#faf2c9',
          '200': '#f6e88b',
          '300': '#f0d545',
          '400': '#e7b717',
          '500': '#de9608',
          '600': '#c97105',
          '700': '#a45509',
          '800': '#82420f',
          '900': '#683510',
        },
        sunglow: {
          '50':  '#faf8ed',
          '100': '#faf5cc',
          '200': '#f5ec8a',
          '300': '#eedc44',
          '400': '#dfbf15',
          '500': '#cfa007',
          '600': '#b27c04',
          '700': '#8d5e07',
          '800': '#6e480d',
          '900': '#56390e',
        },
      }
    }
  }
};
