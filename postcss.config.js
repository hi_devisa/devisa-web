const purgecss = [
  "@fullhuman/postcss-purgecss",
  {
    content: ["./components/**/*.js", "./pages/**/*.js"],
    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
  }
];
module.exports = {
  plugins: [
    'tailwindcss',
    'autoprefixer',
    // "postcss-import",
    'postcss-nesting',
    'postcss-flexbugs-fixes',
    ...(process.env.NODE_ENV === "production" ? [purgecss] : []),
    [
      'postcss-preset-env',
      {
        autoprefixer: {
          flexbox: 'no-2009'
        },
        stage: 3,
        features: {
          'custom-properties': false
        }
      }
    ]
  ]
};

