export default function Header() {
  return (
        <div class="mx-52 py-6 lg:py-8">
            <div class="container px-6 mx-auto flex flex-col md:flex-row items-start md:items-center justify-between">
                <div>
                    <p class="flex items-center text-green-600 text-xs">
                        <span>Home</span>
                        <span class="mx-2">&gt;</span>
                        <span>About</span>
                    </p>
                    <h4 class="text-2xl font-bold leading-tight text-gray-800 dark:text-gray-100">About</h4>
                </div>
                <div class="mt-6 md:mt-0">
                    <button class="mr-3 bg-gray-200 dark:bg-gray-700 focus:outline-none transition duration-150 ease-in-out rounded hover:bg-gray-300 text-green-700 dark:hover:bg-gray-600 dark:text-green-600 px-5 py-2 text-sm">Back</button>
                    <button class="transition focus:outline-none duration-150 ease-in-out hover:bg-green-400 bg-green-500 rounded text-white px-8 py-2 text-sm">Login</button>
                </div>
            </div>
        </div>
  )
}
