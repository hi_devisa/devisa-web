// import { Menu, Transition } from '@headlessui/react'

// export default function Dropdown() {
//   return (
//     <Menu>
//       {({ open }) => (
//         <>
//           <Menu.Button>More</Menu.Button>

//           <Transition
//             show={open}
//             enter="transition duration-100 ease-out"
//             enterFrom="transform scale-95 opacity-0"
//             enterTo="transform scale-100 opacity-100"
//             leave="transition duration-75 ease-out"
//             leaveFrom="transform scale-100 opacity-100"
//             leaveTo="transform scale-95 opacity-0"
//           >
//             <Menu.Items static>
//               <Menu.Item>Dashboard</Menu.Item>
//               <Menu.Item>Account</Menu.Item>
//               <Menu.Item>Profile</Menu.Item>
//               <Menu.Item>Preferences</Menu.Item>
//               <Menu.Item>Log out</Menu.Item>
//             </Menu.Items>
//           </Transition>
//         </>
//       )}
//     </Menu>
//   )
// }
//
//https://codesandbox.io/s/headlessuireact-menu-example-b6xje?file=/src/App.js/
import Link from 'next/link'
import * as React from "react";
import { Menu, Transition } from "@headlessui/react";

export default function Dropdown(props) {
  var menuItems = props.items.map(function(item) {
    return (
      <Menu.Item disabled={item.disabled} as="li">
        {({ active }) => (
          <Link
            href={ item.value }
          >
          <button
            onClick={ item.onClick }
            className={`${
              active
                ? "bg-green-100 text-gray-900"
                : "text-gray-700"
            } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}>
            { item.text }
          </button>
          </Link>
        )}
      </Menu.Item>
    );
  });
  return (
    <div className="flex items-center justify-center bg-white">
      <div className="relative inline-block text-left bg-white">
        <Menu>
          {({ open }) => (
            <>
              <span className="rounded-md shadow-sm">
                { props.button ?
                  <Menu.Button>
                  { props.button }
                </Menu.Button>
                :
                <Menu.Button className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 transition duration-150 ease-in-out rounded-md hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800">
                  <span>{ props.text }</span>
                  <svg
                    className="w-5 h-5 ml-2 -mr-1"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </Menu.Button>

                }
              </span>

              <Transition
                show={open}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                { props.menu?
                    <Menu.Items static className="absolute right-0 shadow-lg outline-none origin-top-right divide-y rounded-md">
                      { props.menu }
                    </Menu.Items>

                :
                <Menu.Items
                  static
                  className="absolute right-0 w-56 mt-2 bg-white border border-gray-200 shadow-lg outline-none origin-top-right divide-y divide-gray-100 rounded-md"
                >
                    { props.heading }

                  <div className="py-1">
                    <ul>
                      { menuItems }
                    </ul>
                  </div>
                </Menu.Items>

                }
              </Transition>
            </>
          )}
        </Menu>
      </div>
    </div>
  );
}


const altMenu = (
  <div class="bg-white min-w-full text-center">
          <a href="#" class="block px-10 min-w-max py-2 text-sm text-gray-700 bg-gray-50 capitalize transition-colors duration-200 transform dark:text-gray-300 hover:bg-blue-500 hover:text-white dark:hover:text-white">
              your profile
          </a>
          <a href="#" class="block px-4 min-w-max py-2 text-sm text-gray-700 bg-gray-50 capitalize transition-colors duration-200 transform dark:text-gray-300 hover:bg-blue-500 hover:text-white dark:hover:text-white">
              Your projects
          </a>
          <a href="#" class="block px-4 py-2 text-sm text-gray-700 bg-gray-50 capitalize transition-colors duration-200 transform dark:text-gray-300 hover:bg-blue-500 hover:text-white dark:hover:text-white">
              Help
          </a>
          <a href="#" class="block px-4 py-2 text-sm text-gray-700 capitalize transition-colors duration-200 transform dark:text-gray-300 hover:bg-blue-500 hover:text-white dark:hover:text-white">
              Settings
          </a>
          <a href="#" class="block px-4 py-2 text-sm text-gray-700 bg-gray-50 capitalize transition-colors duration-200 transform dark:text-gray-300 hover:bg-blue-500 hover:text-white dark:hover:text-white">
              Sign Out
          </a>
  </div>)

const dropdownWrapper = (children) => {
    <div className="flex items-center justify-center p-12">
      <div className="relative inline-block text-left">
        { children }
      </div>
    </div>
}
