import { useSession } from 'next-auth/client'

export default function Header(props) {
  const [ session, loading ] = useSession()
  return (
            <div class="px-6 py-2 mx-6 bg-white shadow rounded-b-md mt-0 pt-0 top-0 z-10 sticky">
              <div class="w-full container grid grid-cols-3 mx-auto px-6 pl-6">

                <div class="">
                  <div class="">
                    <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-2">
                      <i class="fas fa-angle-left"></i>
                      <span>{ props.breadcrumb }</span>
                     </a>
                    <div class="flex items-center">
                      <img class="h-12 w-12 rounded-full shadow ring-1 ring-black" src= { session.user.image } alt=""/>
                      <h1 class="px-4 text-2xl text-gray-900 font-medium">{ props.title }</h1>
                    </div>
                  </div>
                </div>

                <div class="inline-block  align-text-bottom align-baseline mt-10 -mb-2">
                      <nav class="flex  sm:flex-row align-bottom bottom-0 text-sm">
                          <button class=" bg-gray-50 rounded-t-lg text-gray-600 py-4 px-6 block hover:text-green-500 focus:outline-none text-green-500 border-b-4 font-medium border-green-500">
                              Overview
                          </button><button class="text-gray-600 py-4 px-6 block hover:text-green-500 focus:outline-none">
                              Records
                          </button><button class="text-gray-600 py-4 px-6 block hover:text-green-500 focus:outline-none">
                              Items
                          </button><button class="text-gray-600 py-4 px-6 block hover:text-green-500 focus:outline-none">
                              Groups
                          </button>
                      </nav>
                  </div>

                  <div class="mt-8 items-end ml-32">
                    <button class="h-9 border border-gray-200 px-3 py-2 text-sm font-medium rounded text-gray-900 shadow">{ props.secondaryBtn }</button>
                    <button class="h-9 ml-3 px-3 py-2 text-sm font-medium rounded bg-gradient-to-b from-green-400 via-green-500 to-green-500 text-white shadow">{ props.mainBtn }</button>
                  </div>



              </div>
            </div>
  )
}
