import * as fbq from '../../lib/fbPixel'
import { useEffect } from 'react'
import useSWR from 'swr'

export default function ProductCard( props ) {
  const pixelBuyNowEvent = (name, price) => {
    fbq.event('track', 'Buy Now Clicked', { name: name, price: price })
  }
    const registerClickAPI = (name, price, checkoutUrl) => {
        pixelBuyNowEvent(name, price)
        // const item = {
        //   name: name,
        //   price: price,
        //   checkout: checkoutUrl,
        // }
        // const payload = JSON.stringify(item);
        // console.log(payload)
        // const res = await fetch(`/api/shop/item/clicked`,
        //   {
        //     body: payload,
        //     headers: { 'Content-Type': 'application/json'},
        //     method: 'POST',
        //     redirect: 'follow',
        //   }
        // )
        //     .then(console.log(res))
        //     .catch(err => console.error(err))
        // return res
        }
    const handleClick = (event) => {
        // event.preventDefault();
        registerClickAPI(props.name, props.price, props.url);
    };
  return (

    <div class="flex w-full mx-auto overflow-hidden bg-white rounded-lg shadow-lg dark:bg-gray-800">
        <div class="w-1/3 bg-cover" style={{backgroundImage: `url('` + props.img + `')`}}></div>

        <div class="w-2/3 p-4 md:p-4">
            <h1 class="text-2xl font-bold text-gray-800 dark:text-white">{props.name}</h1>

            <p class="mt-2 text-sm text-gray-600 dark:text-gray-400">{ props.description }</p>

            <div class="flex mt-2 item-center">
                <svg class="w-5 h-5 text-gray-700 fill-current dark:text-gray-300" viewBox="0 0 24 24">
                    <path d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"/>
                </svg>

                <svg class="w-5 h-5 text-gray-700 fill-current dark:text-gray-300" viewBox="0 0 24 24">
                    <path d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"/>
                </svg>

                <svg class="w-5 h-5 text-gray-700 fill-current dark:text-gray-300" viewBox="0 0 24 24">
                    <path d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"/>
                </svg>

                <svg class="w-5 h-5 text-gray-500 fill-current" viewBox="0 0 24 24">
                    <path d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"/>
                </svg>

                <svg class="w-5 h-5 text-gray-500 fill-current" viewBox="0 0 24 24">
                    <path d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"/>
                </svg>
            </div>

            <div class="flex justify-between mt-3 item-center">
                <h1 class="text-lg font-bold text-gray-700 dark:text-gray-200 md:text-xl"> { props.price }</h1>
                < a href = { props.url }>
                <button
                    class="px-2 py-1 text-xs font-bold text-white uppercase transition-colors duration-200 transform bg-gray-800 rounded dark:bg-gray-700 hover:bg-gray-700 dark:hover:bg-gray-600 focus:outline-none focus:bg-gray-700 dark:focus:bg-gray-600"
                    onClick={handleClick}
                >
                    { props.button }
                </button>
                </a>
            </div>
        </div>
    </div>
  )
}
