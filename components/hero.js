import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Hero() {
  return (
      <main class="mt-10 ml-auto mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
        <div class="sm:text-center lg:text-left">
          <h1 class="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
            <span class="block xl:inline font-extralight">Welcome, friend, to</span>
            <span class="block text-green-600 xl:inline font-normal">&nbsp;devisa</span>
            <span class="block xl:inline space-x-56 font-extralight">.io&nbsp;&nbsp;</span>

          </h1>
          <p class="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
            Building something we think you'll like. Come back in just a few.
          </p>
          <div class="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
            <div class="rounded-md shadow">
            <Link href="/contact">
                <button class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white hover:bg-green-700 md:py-4 md:text-lg md:px-10 shadow-md transition-all duration-200 bg-gradient-to-br from-green-400 to-green-700">
                Contact us
              </button>
            </Link>
            </div>
            <div class="mt-3 sm:mt-0 sm:ml-3 shadow-magical">
              <Link href="/login">
                <button class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base rounded-md text-gray-500 bg-gray-50 hover:bg-gray-100 md:py-4 md:text-lg md:px-10 shadow-md border-gray-200 font-regular transition-all duration-200">
                Signup
              </button>
              </Link>
            </div>
          </div>
        </div>
      </main>
  )
}

