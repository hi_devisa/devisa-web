import Link from 'next/link'
import Head from 'next/head'
import Footer from './footer/details'
import Nav from './ui/nav'
import BigNav from './ui/nav/big'
import {useSession} from 'next-auth/client'
import { useRouter } from 'next/router'
// import { Card, Tabs, Menu, Input, IconHome, Auth } from '@supabase/ui'

export default function Layout(props) {
  const [ session, loading ] = useSession()
  const router = useRouter()
  const header = props.header ?
      <div class="mx-36 py-6 lg:py-8">
        <div class="container px-6 mx-auto flex flex-col md:flex-row items-start md:items-center justify-between">
          <div>
            <p class="flex items-center text-green-600 text-xs">
              <Link href="/"><span>Home</span></Link>
                <span class="mx-2">&gt;</span>
                <span class="subpixel-antialiased">{props.title}</span>
            </p>
            <h4 class="text-2xl font-bold leading-tight text-gray-800 dark:text-gray-100 subpixel-antialiased">{props.title}</h4>
          </div>
        </div>
      </div>
    : <></>
  return (
    <div class="min-h-screen bg-gradient-to-br from-white via-gray-50 to-pink-50">
      <Head>
        <title>{props.title} - Devisa.io</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      { props.noNav ?
        <></>
        :
      <Nav session={props.session}/>
      }
      { header }
    <div class="mx-auto container px-4 xl:px-4 mb-24">

      <br/>
      <div class="mx-8">
        {props.children}
      </div>

    </div>
        <Footer/>
    </div>
  )
}
