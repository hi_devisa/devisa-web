import Dropdown from '../dropdown'
import { useSession } from 'next-auth/client'
export default function UserDropdown(props) {
  const [session, loading] = useSession()
  const items = [
      { text: "Dashboard", value: "/dashboard", disabled: false },
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Recipes", value: "/user/chris/recipes", disabled: false },
      { text: "Items", value: "/user/chris/items", disabled: false },
      { text: "Feed", value: "/community/feed", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  const defaultButton = <UserDropdownButton name="Chris P" image = "/lightart.jpg"/>
  if (session) {
  return (
    <Dropdown
      button={<UserDropdown image = "/lightart.jpg" user={ session.user? session.user : { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io" } }/>}
      items={items}
      heading={
      <div class="px-4 py-3 hover:bg-green-100">
        <p class="text-sm leading-5">Create a new...</p>
      </div>
      }
    />
  )

  } else {
    return (
      <Dropdown
        button = { <UserDropdownButton name = "Chris P" image = "/lightart.jpg"/> }
        items={items}
        heading={
        <div class="px-4 py-3 hover:bg-green-100">
          <p class="text-sm leading-5">Create a new...</p>
        </div>
        }
      />
    );
      }
}
  export function UserDropdownButton(props) {
    return (
        <button class="flex items-center bg-white">
          <span class="relative flex-shrink-0">
            <img class="w-7 h-7 rounded-full" src={ props.image } alt="profile" />
            <span class="absolute right-0 -mb-0.5 bottom-0 w-3 h-3 shadow rounded-full bg-green-500 border border-white dark:border-gray-900"></span>
          </span>
          <span class="ml-2">{ props.name }</span>
          <svg viewBox="0 0 24 24" class="w-4 ml-1 flex-shrink-0" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <polyline points="6 9 12 15 18 9"></polyline>
          </svg>
        </button>
    )
  }
