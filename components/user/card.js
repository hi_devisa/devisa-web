import Link from 'next/link'
export default function UserCard(props) {
  return (
  <div>
    <div class="max-w-md px-8 py-4 mx-auto bg-white rounded-lg shadow-md dark:bg-gray-800">
        <div class="flex justify-center -mt-16 md:justify-end">
          <Link href={ props.imageHref }>
            <img class="object-cover w-20 h-20 border-2 border-green-500 rounded-full dark:border-green-400" alt="Testimonial avatar" src={ props.image }/>
          </Link>
        </div>

        <h2 class="mt-2 text-2xl font-semibold text-gray-800 dark:text-white md:mt-0 md:text-3xl">{ props.title }</h2>

      <p class="mt-2 text-gray-600 dark:text-gray-200">{ props.children }</p>

        <div class="flex justify-end mt-4">
            <a href={ props.href } class="text-xl font-medium text-green-500 dark:text-green-300">{ props.name }</a>
        </div>
    </div>
    </div>
  )
}
