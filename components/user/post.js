import Link from 'next/link'
export default function Post(props) {
    return (
       <div class="width-full min-width-full flex items-start px-4 py-6 shadow-md m-4 rounded-md hover:bg-gray-100 transition duration-100">
      <img class="w-12 h-12 rounded-full object-cover mr-4 shadow" src={ props.img } alt="avatar"/>
      <div class="block min-width-full">
         <div class="flex min-width-full  items-center justify-between">
            <h2 class="justify-start text-lg font-semibold text-gray-900 -mt-1"> { props.user } <span class="ml-3 text-md font-light opacity-70 font-medium text-green-600">@{ props.username }</span></h2>
            <small class="justify-end float-right text-sm text-gray-700">22h ago</small>
         </div>
         <p class="text-gray-700"> { props.subtitle } <span class="ml-4 text-gray-500"> { props.subsubtitle } </span></p>

         <p class="mt-3 text-gray-700 text-sm">
            { props.children }
         </p>
         <div class="mt-4 flex items-center">
               <Link href="/">
            <div class="flex mr-3 text-gray-700 text-sm mr-4">
               <svg fill="none" viewBox="0 0 24 24"  class="w-4 h-4 mr-1 hover:text-green-500" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
                </svg>
               <span class="hover:text-green-500"><Link href="/">12</Link></span>
            </div>
               </Link>
               <Link href="/">
            <div class="flex mr-3 text-gray-700 text-sm mr-4">
               <svg fill="none" viewBox="0 0 24 24"  class="w-4 h-4 mr-1 hover:text-green-500" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"/>
               </svg>
               <span class="hover:text-green-500"><Link href="/">8</Link></span>
            </div>
               </Link>
               <Link href="/">
            <div class="flex mr-3 text-gray-700 text-sm mr-4">
               <svg fill="none" viewBox="0 0 24 24"  class="w-4 h-4 mr-1 hover:text-green-500" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"/>
                </svg>
               <span class="hover:text-green-500"><Link href="/">Share</Link></span>
            </div>
               </Link>
         </div>
      </div>
   </div>
         )
         }
