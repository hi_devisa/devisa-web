import {
  ChevronUpIcon,
  StatusOnlineIcon, StatusOfflineIcon,
  DocumentAddIcon, DocumentIcon, DocumentTextIcon, DocumentRemoveIcon, DocumentDuplicateIcon,
  CogIcon,
  EyeIcon,
  LinkIcon,
  HomeIcon,
  CubeIcon,
  TagIcon,
  UsersIcon,
  BeakerIcon,
  StarIcon,
  InboxIcon,
  FolderIcon,
  PencilIcon,
  ClockIcon,
  MinusIcon,
  PlusIcon,
  KeyIcon,
  MapIcon,
  BellIcon,
  SaveIcon,
  ChatIcon,
  ReplyIcon,
  FolderRemoveIcon,
  ShareIcon,
  TableIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  TrashIcon,
  SearchIcon,
  CodeIcon,
  ArchiveIcon,
  LibraryIcon,
  InboxInIcon,
  FolderOpenIcon,
  FolderDownloadIcon,
  InformationCircleIcon,
  DuplicateIcon,
  DotsCircleHorizontalIcon,
  BookOpenIcon,
  BookmarkIcon,
  CashIcon,
  CheckIcon,
  CalendarIcon,
  ChartBarIcon,
  CollectionIcon,
  CalculatorIcon,
  ColorSwatchIcon, CameraIcon, FilterIcon, FolderAddIcon, UserGroupIcon, HeartIcon, HashtagIcon,
  LockClosedIcon, LocationMarkerIcon, MenuIcon, MicrophoneIcon, PaperClipIcon, PhotographIcon, RefreshIcon, UserAddIcon, UserRemoveIcon, UploadIcon, UserIcon,
  SortAscendingIcon, SortDescendingIcon, SelectorIcon, TrendingUpIcon, TrendingDownIcon, TerminalIcon, TemplateIcon, TicketIcon,
  ChevronDownIcon, DotsVerticalIcon, AtSymbolIcon,  AnnotationIcon, AdjustmentsIcon, CakeIcon, CreditCardIcon,
  PlayIcon, PauseIcon, RewindIcon, SaveAsIcon, StopIcon,
  LockOpenIcon, ExternalLinkIcon, ViewGridIcon, ViewListIcon, ViewGridAddIcon, ViewBoardsIcon
} from '@heroicons/react/solid'
import { Popover, Transition, Disclosure } from '@headlessui/react'

export function EntityBadge(props) {
  const name = props.name? props.name : "Quantity";
  const tooltip = props.tooltip? props.name : "The amount in your library";
  return (
    <div className={` flex ml-3  float-left px-2 py-1 ${ props.bg } text-md ${ props.color } ${ props.hoverBg } ${ props.hoverColor } ${ props.ridge3d? props.ridge3d : "" } items-center rounded-xl text-sm`}>
      <props.icon class="w-4 h-4"/>
      <span class="ml-1">{ props.value }</span>
  </div>
  )
}
