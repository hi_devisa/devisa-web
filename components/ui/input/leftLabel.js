
export default function Input(props) {
    return (
<div>
  { props.label ?
    <label for= { props.name } class="block -my-1 text-sm font-medium text-gray-700">{ props.label }</label>
    : <></>}
  <div class="mt-1 relative rounded-md shadow focus:outline-black focus:ring-2 focus:ring-green-500">
    { props.leftLabel ?
    <div class="bg-gray-100  px-3 absolute inset-y-0 left-0  ring-1 ring-gray-300   flex items-center pointer-events-auto rounded z-0">
      <span class="text-gray-600 text-md" >
        { props.leftLabel }
      </span>
    </div>
    : <></>
    }
    <input
      type= { props.type }
      name= { props.name }
      id= { props.id }
      class="focus:ring-0 focus:outline-none  focus:ring-2 focus:ring-gray-400 focus:ring-offset-green-50 focus:shadow-md focus:border-transparent border-transparent ring-1 ring-gray-300  block w-full pl-12 pr-12 sm:text-sm  rounded"
      placeholder= { props.placeholder }/>
</div>
</div>

    )
}
