import {
  ChevronUpIcon,
  StatusOnlineIcon, StatusOfflineIcon,
  DocumentAddIcon, DocumentIcon, DocumentTextIcon, DocumentRemoveIcon, DocumentDuplicateIcon,
  CogIcon,
  EyeIcon,
  LinkIcon,
  HomeIcon,
  CubeIcon,
  TagIcon,
  UsersIcon,
  BeakerIcon,
  StarIcon,
  InboxIcon,
  FolderIcon,
  PencilIcon,
  ClockIcon,
  MinusIcon,
  PlusIcon,
  KeyIcon,
  MapIcon,
  BellIcon,
  SaveIcon,
  ChatIcon,
  ReplyIcon,
  FolderRemoveIcon,
  ShareIcon,
  TableIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  TrashIcon,
  SearchIcon,
  CodeIcon,
  ArchiveIcon,
  LibraryIcon,
  InboxInIcon,
  FolderOpenIcon,
  FolderDownloadIcon,
  InformationCircleIcon,
  DuplicateIcon,
  DotsCircleHorizontalIcon,
  BookOpenIcon,
  BookmarkIcon,
  CashIcon,
  CheckIcon,
  CalendarIcon,
  ChartBarIcon,
  CollectionIcon,
  CalculatorIcon,
  ColorSwatchIcon, CameraIcon, FilterIcon, FolderAddIcon, UserGroupIcon, HeartIcon, HashtagIcon,
  LockClosedIcon, LocationMarkerIcon, MenuIcon, MicrophoneIcon, PaperClipIcon, PhotographIcon, RefreshIcon, UserAddIcon, UserRemoveIcon, UploadIcon, UserIcon,
  SortAscendingIcon, SortDescendingIcon, SelectorIcon, TrendingUpIcon, TrendingDownIcon, TerminalIcon, TemplateIcon, TicketIcon,
  ChevronDownIcon, DotsVerticalIcon, AtSymbolIcon,  AnnotationIcon, AdjustmentsIcon, CakeIcon, CreditCardIcon,
  PlayIcon, PauseIcon, RewindIcon, SaveAsIcon, StopIcon,
  LockOpenIcon, ExternalLinkIcon, ViewGridIcon, ViewListIcon, ViewGridAddIcon, ViewBoardsIcon
} from '@heroicons/react/solid'
import { Popover, Transition, Disclosure } from '@headlessui/react'

export function EntityPanel(props) {
  return (
      <Disclosure>
        {({ open }) => (
            <>
          <Disclosure.Button className="justify-around w-full p-2 px-8 py-2 mx-8 mx-auto my-2 font-medium bg-gray-100 border-b-2 border-gray-200 rounded-lg hover:bg-gray-200 transition duration-200">
            <div
              className={`
                 flex flex-shrink float-left items-center items-start
                ${ open? "" : "" }
              `}>
              <div class="rounded-full bg-white text-gray-400 p-1 border-b-4 border-b-gray-400">
                <props.icon class="   w-6 h-6 "/>
              </div>
              <span className={` text-gray-800 ml-3  float-left text-gray-700 text-md items-center ${ open? "text-black" : "" }`}>{ props.entity }</span>
              {  props.quantity &&
                <span className={`  ml-3  float-left text-gray-400 text-md items-center ${ open? "text-gray-400" : "" }`}>{ props.quantity  } in library</span>
              }
            </div>
            <div class="items-center place-items-end float-right justify-end flex-shrink flex mt-1">
              {  props.badge1 &&
              <EntityBadge
                value = { props.badge1 }
                icon = { CheckIcon }
                name = "" tooltip = ""
                bg = "bg-gray-200" hoverBg = "bg-gray-300"
                color = "text-gray-300" hoverColor = "text-gray-400"
              />
              }
              {  props.badge2 &&
              <EntityBadge
                value = { props.badge2 }
                icon = { BookmarkIcon }
                name = "" tooltip = ""
                bg = "bg-gray-400" hoverBg = "bg-gray-300"
                color = "text-gray-200" hoverColor = "text-gray-100"
              />
              }
              {  props.due &&
                <span class="items-center text-sm text-gray-400 ">Due by { props.due }</span>
              }
            <ChevronDownIcon
              className={` ${
                open ? "transform rotate-180" : ""
              } w-5 h-5 text-gray-500 ml-3`}/>
            </div>
          </Disclosure.Button>
          <Transition
            show={open}
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-95 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-95 opacity-0"
          >
              <Disclosure.Panel static className="px-4 pt-4 pb-2 text-sm text-gray-500 bg-white rounded-lg">
                { props.children }
                <br/><br/><hr/><br/><br/>
              </Disclosure.Panel>
          </Transition>
            </>
          )}
        </Disclosure>
  )
}
