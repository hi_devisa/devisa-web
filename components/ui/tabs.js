export default function Alt4() {
    function tabs() {
      return {
        previousTab: 0,
        activeTab: 0,
        width: 0,
        x: 0,

        setWidthAndXFromElement(element) {
          const width = element.clientWidth
          const x = element.offsetLeft

          x = x
          width = width
        },

        container: {
          ['x-on:load.window']() {
            const element = $refs.tabs.children[0]

            setWidthAndXFromElement(element)

            element.classList.add('text-indigo-600')
          },
        },

        indicator: {
          ['x-bind:style']() {
            return `width: ${width}px; transform: translateX(${x}px)`
          }
        },

        tab: {
          ['@click'](event) {
            const element = event.target

            setWidthAndXFromElement(element)

            previousTab = activeTab

            activeTab = Array
              .from($refs.tabs.children)
              .indexOf(element)

            $refs.tabs.children[previousTab]
              .classList
              .remove('text-indigo-600')

            element.classList.add('text-indigo-600')
          }
        }
      }
    }
    return (

<header x-data="tabs()" x-spread="container" class="bg-white shadow relative">
	<nav x-ref="tabs" class="flex">
		<a x-spread="tab" class="inline-flex items-center justify-center px-8 py-4 text-sm font-medium text-gray-600 transition" href="#">Info</a>
		<a x-spread="tab" class="inline-flex items-center justify-center px-8 py-4 text-sm font-medium text-gray-600 transition" href="#">Symptoms</a>
		<a x-spread="tab" class="inline-flex items-center justify-center px-8 py-4 text-sm font-medium text-gray-600 transition" href="#">Safety checklist</a>
		<a x-spread="tab" class="inline-flex items-center justify-center px-8 py-4 text-sm font-medium text-gray-600 transition" href="#">Local regulations</a>
	</nav>

	<div x-spread="indicator" class="border-t-2 border-indigo-600 absolute left-0 bottom-0 transition-all duration-500"></div>
</header>
    )
}


