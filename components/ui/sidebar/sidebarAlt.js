export default function Alt4(props) {

    return (
<div id="app" class="min-h-screen bg-gray-200">
  <aside class="relative h-screen inline-flex flex-col justify-between items-center bg-white shadow p-6">
    <nav class=" inline-flex flex-col space-y-2">
    </nav>
    <div class="flex items-center w-full bg-gray-100 p-4 rounded-lg" >
      <img src={props.img} alt={props.alt} class="w-10 h-10 object-cover rounded-full mr-4 border border-solid border-white"/>
      <div>
        <h3 class="text-gray-900 font-semibold" ></h3>
        <h4 class="text-sm text-gray-700 mt-1" ></h4>
      </div>
    </div>
        <img src={props.img} alt={props.alt} class="w-10 h-10 object-cover rounded-full"/>
          <button class="absolute right-0 bottom-0 top-0 my-auto transform translate-x-full h-10 w-10 p-1 bg-white text-gray-600 rounded-r-lg mx-auto border border-solid border-gray-200 hover:border-gray-300">
    </button>
  </aside>
</div>
      )
      }
