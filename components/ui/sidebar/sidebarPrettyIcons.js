import Link from 'next/link'
import Tooltip from '../tooltip/tooltip'
import { useRouter } from 'next/router'

export default function Sbp() {
  const router = useRouter()
  return (
<div class="bg-white    w-20 h-screen   h-full left-0 overflow-hidden">

  <div class="fixed w-18  mt-0">
    <div class="">
  <a href="#" class="block w-18 h-24  flex items-center align-middle justify-center text-xl font-medium text-gray-800">
                    <div class="rounded-full relative p-0 flex justify-end text-black text-md">
                    </div>
  </a>
    </div>


    <div class="font-bold uppercase px-3 text-xs text-gray-600 tracking-wider mt-8 mb-4"></div>
      <Link href="/dashboard">
      <button className = { router.pathname == "/dashboard" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg     class="w-12 h-5 mx-3 fill-current" viewBox="0 0 16 16">
  <path d="M6 1H1v14h5V1zm9 0h-5v5h5V1zm0 9v5h-5v-5h5zM0 1a1 1 0 0 1 1-1h5a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm9 0a1 1 0 0 1 1-1h5a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1h-5a1 1 0 0 1-1-1V1zm1 8a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h5a1 1 0 0 0 1-1v-5a1 1 0 0 0-1-1h-5z"/>
</svg>
  </button>
      </Link>

      <Link href="/dashboard/insights">
        <button className={ router.pathname == "/dashboard/insights" ? "bg-green-100" : "" }class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg class="w-12 h-5 fill-current mx-3" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" >
  <path d="M4 11H2v3h2v-3zm5-4H7v7h2V7zm5-5v12h-2V2h2zm-2-1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1h-2zM6 7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V7zm-5 4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-3z"/>
</svg>
  </button></Link>


  <Link href="/dashboard/collection"><button className = { router.pathname == "/dashboard/collection" ? "bg-green-100" : "" } class="flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 border-l-8 border-green-500 rounded-r-lg my-2  w-18">
    <svg  class="w-12 h-5 fill-current mx-3"     viewBox="0 0 16 16">
  <path d="M2.5 3.5a.5.5 0 0 1 0-1h11a.5.5 0 0 1 0 1h-11zm2-2a.5.5 0 0 1 0-1h7a.5.5 0 0 1 0 1h-7zM0 13a1.5 1.5 0 0 0 1.5 1.5h13A1.5 1.5 0 0 0 16 13V6a1.5 1.5 0 0 0-1.5-1.5h-13A1.5 1.5 0 0 0 0 6v7zm1.5.5A.5.5 0 0 1 1 13V6a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5h-13z"/>
</svg>
  </button></Link>


    { /* props.username */ }
    <Link href="/chris"><button className = { router.pathname == "/chris" ? "bg-green-100" : "" } class="flex items-center text-gray-700 tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 hover:text-black w-18">
      <svg class="w-12 h-5 fill-current mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
</svg>
  </button></Link>

    <div class="font-bold uppercase px-3 text-xs text-gray-600 tracking-wider mt-8 mb-4"></div>

      <Link href="/community/feed">
      <button className = { router.pathname == "/community/feed" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg     class="w-12 h-5 mx-3 fill-current" viewBox="0 0 16 16">
  <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
</svg>

  </button>
  </Link>

      <Link href="/community/recipes">
      <button className = { router.pathname == "/community/recipes" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg  class="w-12 h-5 fill-current mx-3"    viewBox="0 0 16 16">
  <path d="M8.235 1.559a.5.5 0 0 0-.47 0l-7.5 4a.5.5 0 0 0 0 .882L3.188 8 .264 9.559a.5.5 0 0 0 0 .882l7.5 4a.5.5 0 0 0 .47 0l7.5-4a.5.5 0 0 0 0-.882L12.813 8l2.922-1.559a.5.5 0 0 0 0-.882l-7.5-4zm3.515 7.008L14.438 10 8 13.433 1.562 10 4.25 8.567l3.515 1.874a.5.5 0 0 0 .47 0l3.515-1.874zM8 9.433L1.562 6 8 2.567 14.438 6 8 9.433z"/>
</svg>

  </button>
  </Link>


      <Link href="/community/groups">
      <button className = { router.pathname == "/community/groups" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
<svg class="w-12 h-5 fill-current mx-3" xmlns="http://www.w3.org/2000/svg"   viewBox="0 0 16 16">
  <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
</svg>
  </button></Link>

      <Link href="/community/topics">
      <button className = { router.pathname == "/community/topics" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg    class="w-12 h-5 mx-3 fill-current"   viewBox="0 0 16 16">
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
</svg>
  </button></Link>

    <div class="font-bold uppercase px-3 text-xs text-gray-600 tracking-wider mt-8 mb-4"></div>


      <Link href="/account">
      <button className = { router.pathname == "/account" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
      <svg class="w-12 h-5 fill-current mx-3" viewBox="-21 0 512 512"><path d="m389.332031 160c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m389.332031 512c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m80 336c-44.097656 0-80-35.882812-80-80s35.902344-80 80-80 80 35.882812 80 80-35.902344 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m135.703125 240.425781c-5.570313 0-10.988281-2.902343-13.910156-8.0625-4.375-7.679687-1.707031-17.453125 5.972656-21.824219l197.953125-112.855468c7.65625-4.414063 17.449219-1.726563 21.800781 5.976562 4.375 7.679688 1.707031 17.449219-5.972656 21.824219l-197.953125 112.851563c-2.496094 1.40625-5.203125 2.089843-7.890625 2.089843zm0 0"/><path d="m333.632812 416.425781c-2.6875 0-5.398437-.683593-7.894531-2.109375l-197.953125-112.855468c-7.679687-4.371094-10.34375-14.144532-5.972656-21.824219 4.351562-7.699219 14.125-10.367188 21.804688-5.972657l197.949218 112.851563c7.679688 4.375 10.347656 14.144531 5.976563 21.824219-2.945313 5.183594-8.363281 8.085937-13.910157 8.085937zm0 0"/></svg>
  </button></Link>


      <Link href="/account/prefs">
      <button className = { router.pathname == "/account/prefs" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg     class="w-12 h-5 mx-3 fill-current" viewBox="0 0 16 16">
  <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
  <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
</svg>

  </button></Link>


      <Link href="/support">
      <button className = { router.pathname == "/support" ? "bg-green-100" : "" } class="w-18 flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-gray-700 hover:text-black">
        <svg     class="w-12 h-5 mx-3 fill-current" viewBox="0 0 16 16">
  <path d="M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13a.5.5 0 0 1 0 1 .5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1 0-1 .5.5 0 0 1 0-1 .5.5 0 0 1-.46-.302l-.761-1.77a1.964 1.964 0 0 0-.453-.618A5.984 5.984 0 0 1 2 6zm6-5a5 5 0 0 0-3.479 8.592c.263.254.514.564.676.941L5.83 12h4.342l.632-1.467c.162-.377.413-.687.676-.941A5 5 0 0 0 8 1z"/>
</svg>
  </button></Link>
  </div>
  </div>
)
}




  // <Link href="/account"><button className = { router.pathname == "" ? "bg-green-100" : "" } class="flex items-center tracking-wide font-normal text-sm h-12 hover:bg-green-50 rounded-r-lg transition duration-200 text-white bg-green-500 rounded-r-lg my-2 shadow-xl w-18">
  //     <svg class="w-12 h-5 fill-current mx-3" viewBox="-21 0 512 512"><path d="m389.332031 160c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m389.332031 512c-44.09375 0-80-35.882812-80-80s35.90625-80 80-80c44.097657 0 80 35.882812 80 80s-35.902343 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m80 336c-44.097656 0-80-35.882812-80-80s35.902344-80 80-80 80 35.882812 80 80-35.902344 80-80 80zm0-128c-26.453125 0-48 21.523438-48 48s21.546875 48 48 48 48-21.523438 48-48-21.546875-48-48-48zm0 0"/><path d="m135.703125 240.425781c-5.570313 0-10.988281-2.902343-13.910156-8.0625-4.375-7.679687-1.707031-17.453125 5.972656-21.824219l197.953125-112.855468c7.65625-4.414063 17.449219-1.726563 21.800781 5.976562 4.375 7.679688 1.707031 17.449219-5.972656 21.824219l-197.953125 112.851563c-2.496094 1.40625-5.203125 2.089843-7.890625 2.089843zm0 0"/><path d="m333.632812 416.425781c-2.6875 0-5.398437-.683593-7.894531-2.109375l-197.953125-112.855468c-7.679687-4.371094-10.34375-14.144532-5.972656-21.824219 4.351562-7.699219 14.125-10.367188 21.804688-5.972657l197.949218 112.851563c7.679688 4.375 10.347656 14.144531 5.976563 21.824219-2.945313 5.183594-8.363281 8.085937-13.910157 8.085937zm0 0"/></svg>
  //     Collection
  // </button></Link>
