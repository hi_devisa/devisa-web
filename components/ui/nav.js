import Slide from '../../components/ui/slide/slide'
import Gradient from '../../components/ui/button/gradient'
import { Dialog, Popover, Transition } from '@headlessui/react'
import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  PhoneIcon,
  PlayIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,

  XIcon,
} from '@heroicons/react/outline'
import { ChevronDownIcon } from '@heroicons/react/solid'
import Dropdown from '../../components/dropdown'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { Fragment, useRef, useEffect, useState } from 'react'
import { getSession, useSession, signIn, signOut } from 'next-auth/client'

export  function NewDialog() {
  const [open, setOpen] = useState(true);
  const cancelButtonRef = useRef();

  function closeModal() {
    setOpen(false);
  }

  function openModal() {
    setOpen(true);
  }

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center">
        <button
          type="button"
          onClick={openModal}
          className="px-4 py-2 text-sm font-medium text-white bg-black rounded-md bg-opacity-20 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
        >
          Open dialog
        </button>
      </div>
      <Transition show={open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          initialFocus={cancelButtonRef}
          static
          open={open}
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle bg-white shadow-xl transition-all transform rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium text-gray-900 leading-6"
                >
                  Payment successful
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    Your payment has been successfully submitted. We’ve sent
                    your an email with all of the details of your order.
                  </p>
                </div>

                <div className="mt-4">
                  <button
                    type="button"
                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                    onClick={closeModal}
                  >
                    Got it, thanks!
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}

const callsToAction = [
  { name: 'Our blog', href: '/blog', icon: PlayIcon },
  { name: 'Contact Us', href: 'mailto:hi@devisa.io', icon: PhoneIcon },
]

const classNames = (...classes) => {
  return classes.filter(Boolean).join(' ');
}

const resources = [

  {
    name: 'Documentation',
    description: 'Read the documentation for using tools in our ecosystem',
    href: 'https://docs.devisa.io/',
    icon: SupportIcon,
  },
  {
    name: 'The Book',
    description: 'See examples of our solutions in action',
    href: 'https://book.devisa.io/',
    icon: BookmarkAltIcon,
  },
  {
    name: 'Blog',
    description: 'Read blog posts to stay up-to-date',
    href: '/blog',
    icon: CalendarIcon,
  },
  { name: 'Security', description: 'Understand how we take your privacy seriously.', href: '#', icon: ShieldCheckIcon },
]
const solutions = [
  {
    name: 'Cloud Infrastructure',
    description: 'Bring your applications and services to the cloud',
    href: '#',
    icon: ChartBarIcon,
  },
  {
    name: "Frontend Development",
    description: "Modernize your website's user experienceSpeak directly to your customers in a more meaningful way.",
    href: '#',
    icon: CursorClickIcon,
  },
  { name: 'Backend Development',
    description: "Build out a secure and robust infrastructure", href: '#', icon: ShieldCheckIcon },
  {
    name: 'Integrations',
    description: "Connect with third-party tools that you're already using.",
    href: '#',
    icon: ViewGridIcon,
  },
  {
    name: 'Mobile Applications',
    description: 'Bring your presence to mobile devices everywhere',
    href: '#',
    icon: RefreshIcon,
  },
]

export default function Nav(props) {
  const [ session, loading ] = useSession()
  const [panelOpen, setPanelOpen] = useState(true)
  const newSlide = (kind) => {
    setPanelOpen(true);


  }
    const extraBtns = (
        <>
                     <div class="text-gray-700 flex items-center mt-4 px-4 py-4 mx-2 md:mt-0 p-2 shadow-sm border border-gray-200 rounded-lg">
                         <button class="hidden text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
  <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
  <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
</svg>
                         </button>
                     </div>
                     <div class="text-gray-700 flex items-center mt-4 px-4 py-4 mx-2 md:mt-0 p-2 shadow rounded-lg">
                         <button class="hidden text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-grid" viewBox="0 0 16 16">
  <path d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"/>
</svg>
                         </button>
                     </div>
                     <div class="text-gray-700 flex items-center mt-4 px-4 py-4 mx-2 md:mt-0 p-2 shadow-sm border border-gray-200 rounded-lg">
                         <button class="hidden  text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                        </svg>
                         </button>
                     </div>
                     <div class="text-gray-700 flex items-center mt-4 px-4 py-4 mx-2 md:mt-0 p-2 shadow-sm border border-gray-200 rounded-lg">
                         <button class="hidden  text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
</svg>
                         </button>
                     </div>
    </>
    )
  const router = useRouter()
    const dropdown = () => {
        if (session) {
            return (
            <div class="flex mt-2 items-center shadow pr-2 rounded-lg justify-between mx-auto">
                <img class="rounded h-12 w-12 object-cover" src={session.user.image} alt="logo" />
                <p class="text-sm text-gray-700 pl-4"> { session.user.name } </p>
        <div class="text-gray-600 ml-2 min-h-full">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round">
                <path stroke="none" d="M0 0h24v24H0z" />
                <polyline points="6 9 12 15 18 9" />
            </svg>
        </div>
                </div>
            )}
    }
    const email = session ? session.user.email : "undefined"
    const heading = (
      <div class="px-4 py-3 hover:bg-green-100">
        <p class="text-sm leading-5">Signed in as</p>
        <p class="text-sm font-medium font-bold text-gray-900 truncate leading-5">
            { email }
        </p>
      </div>
        )
    const newItems = [
        { text: "New Post", value: "#", disabled: false, onClick: () => { slideNew("Post") } },
        { text: "New Item", value: "#", disabled: false, onClick: () => { slideNew("Item") } },
        { text: "New Record", value: "#", disabled: false, onClick: () => { slideNew("Record") } },
        { text: "New Field", value: "#", disabled: false, onClick: () => { slideNew("Field") } },
        { text: "New Group", value: "#", disabled: false, onClick: () => { slideNew("Group") } },
        { text: "New Link", value: "#", disabled: false, onClick: () => { slideNew("Link") } },
        { text: "New Message", value: "#", disabled: false, onClick: () => { slideNew("Message") } },
        { text: "New Target", value: "#", disabled: false, onClick: () => { slideNew("Target") } },
        { text: "New Field Value", value: "#", disabled: false, onClick: () => { slideNew("Field Value") } },
        { text: "New Automata", value: "#", disabled: false, onClick: () => { slideNew("Automata") } },
    ]
    const newHeading = (
      <div class="px-4 py-3 hover:bg-green-100">
        <p class="text-sm leading-5">Create a new...</p>
      </div>
    )
    const newBtn = (
        <div class="hover:bg-gray-50    active:bg-gray-100 text-gray-700 flex items-center  mx-2 px-4 md:mt-0 py-4 mr-2 shadow active:outline-none outline-none rounded-lg focus:outline-none">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="h-4 w-4 bi bi-file-plus" viewBox="0 0 16 16">
  <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
</svg>
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round">
                <path stroke="none" d="M0 0h24v24H0z" />
                <polyline points="6 9 12 15 18 9" />
            </svg>
      </div>

    )
  const items = [
      { text: "Dashboard", value: "/dashboard", disabled: false },
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Recipes", value: "/user/chris/recipes", disabled: false },
      { text: "Items", value: "/user/chris/items", disabled: false },
      { text: "Feed", value: "/community/feed", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  if (!props.session) {
    return (
    <Popover class="relative bg-white">
      {({ open }) => (
        <nav class="w-full mx-auto shadow relative z-30">
            <div class="container px-6 h-16 flex items-center justify-between mx-auto">
                <div class="flex items-center">
                    <img src="/logo/satgrn.png"
                        width = "72"
                        height = "72"/>
                    <div class="rounded-full relative p-0 flex justify-end text-black text-lg">
              <Popover.Group as="nav" class="hidden md:flex space-x-10">
                <ul class="pt-1 flex items-center justify-center h-full">
                  <li><p class="subpixel-antialiased inline-flex items-center justify-center p-2 py-3           mr-20  transition duration-200  h-full xl:flex items-center text-md font-medium  tracking-normal">
                      <Link href="/">
                      Devisa
                      </Link>
                  </p></li>
                    <Link href="/"><li class="ml-8 inline-flex items-center justify-center px-3 py-2  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">Home</li></Link>
                <Popover class="relative">
                  {({ open }) => (
                    <>
                      <Popover.Button
                        class=
                          'group   ml-6 inline-flex       '
                      >
                        <li class="inline-flex items-center justify-center px-3 py-4 bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">
                        Services
                        <ChevronDownIcon
                          class={classNames(
                            open ? 'text-gray-600' : 'text-gray-400',
                            'ml-1 h-4 w-4 group-hover:text-gray-500'
                          )}
                          aria-hidden="true"
                        />
                      </li>
                      </Popover.Button>

                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-200"
                        enterFrom="opacity-0 translate-y-1"
                        enterTo="opacity-100 translate-y-0"
                        leave="transition ease-in duration-150"
                        leaveFrom="opacity-100 translate-y-0"
                        leaveTo="opacity-0 translate-y-1"
                      >
                        <Popover.Panel
                          static
                          class="absolute z-10 w-screen max-w-md px-2 mt-3 -ml-4 transform sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2"
                        >
                          <div class="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                            <div class="relative px-5 py-6 bg-white grid gap-6 sm:gap-8 sm:p-8">
                              {solutions.map((item) => (
                                <a
                                  key={item.name}
                                  href={item.href}
                                  class="flex items-start p-3 -m-3 rounded-lg hover:bg-gray-100"
                                >
                                  <item.icon class="flex-shrink-0 w-6 h-6 text-green-600" aria-hidden="true" />
                                  <div class="ml-4">
                                    <p class="text-base font-medium text-gray-900">{item.name}</p>
                                    <p class="mt-1 text-sm text-gray-500">{item.description}</p>
                                  </div>
                                </a>
                              ))}
                            </div>
                            <div class="px-5 py-5 bg-gray-50 space-y-6 sm:flex sm:space-y-0 sm:space-x-10 sm:px-8">
                              {callsToAction.map((item) => (
                                <div key={item.name} class="flow-root">
                                  <a
                                    href={item.href}
                                    class="flex items-center p-3 -m-3 text-base font-medium text-gray-900 rounded-md hover:bg-gray-100"
                                  >
                                    <item.icon class="flex-shrink-0 w-6 h-6 text-gray-400" aria-hidden="true" />
                                    <span class="ml-3">{item.name}</span>
                                  </a>
                                </div>
                              ))}
                            </div>
                          </div>
                        </Popover.Panel>
                      </Transition>
                    </>
                  )}
                </Popover>
                          <li class="ml-1 inline-flex items-center justify-center px-3 py-3 bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">
                            <Link href="/about">About</Link></li>
                            <li class="ml-4 inline-flex items-center justify-center px-3 py-3 bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">
                            <Link href="/contact">Contact</Link></li>
                            <li class="ml-4 inline-flex items-center justify-center px-3 py-3 bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">
                            <Link href="/blog">Blog</Link></li>
                            <li class="ml-4 inline-flex items-center justify-center px-3 py-3 bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm focus:outline-none">
                            <Link href="/shop">Shop</Link></li>
                </ul>
              </Popover.Group>
                    </div>
                </div>
                <div aria-haspopup="true" class="cursor-pointer h-full xl:flex items-center justify-end hidden relative">
                    <ul class="p-2 w-40 border-r bg-gray-900 absolute rounded z-40 right-0 shadow mt-64 hidden">
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 focus:text-green-700 focus:outline-none flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <circle cx="12" cy="12" r="9" />
                                <line x1="12" y1="17" x2="12" y2="17.01" />
                                <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                            </svg>
                            <span class="ml-2">Help Center</span>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                <circle cx="12" cy="12" r="3" />
                            </svg>
                            <span class="ml-2">Account Settings</span>
                        </li>
                    </ul>
                     <div class="flex items-center mt-4 md:mt-0">
                         <Link href="/login"><button class="hidden mr-8 hover:text-green-900 hover:bg-green-100 p-4 transition duration-200 text-sm rounded-xl md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             Login
                         </button>
                         </Link>
                     </div>
                     <Gradient label="Signup" href="/signup" size="md"/>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                        </svg>
                         </button>
                     </div>
                </div>
                <div class="visible xl:hidden flex items-center">
                  <ul class="p-2 border-r bg-gray-50 absolute rounded top-0 left-0 right-0 shadow mt-16 md:mt-16 hidden">
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mt-2 py-3 hover:text-green-700 focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Dashboard</span>
                                    </div>
                                </li>
                                  <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 focus:text-green-700 focus:outline-none flex justify-center">

                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Products</span>
                                    </div>
                                    <ul class="ml-2 mt-3 hidden">
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Landing Pages
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Templates
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Components
                                        </li>
                                    </ul>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Performance</span>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mb-2 py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Deliverables</span>
                                </li>
                                <li>
                                    <hr class="border-b border-gray-300 w-full"/>
                                </li>
                      <li>

                      </li>

                            </ul>

                    <div id="menu" class="text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <line x1="4" y1="6" x2="20" y2="6" />
                            <line x1="4" y1="12" x2="20" y2="12" />
                            <line x1="4" y1="18" x2="20" y2="18" />
                        </svg>
                    </div>
                </div>
            </div>

        </nav>
)}
  </Popover>
)
} else {
    var img = props.img ? props.img : "/ICONA.png"
    return (
    <Popover class="relative bg-white">
      {({ open }) => (
        <nav class="w-full mx-auto shadow relative z-30">
            <div class="container px-6 h-16 flex items-center justify-between mx-auto">
                <div class="flex items-center">
                    <img src="/logo/satgrn.png"
                        width = "72"
                        height = "72"/>
                    <div class="rounded-full relative p-0 flex justify-end text-black text-lg">
              <Popover.Group as="nav" class="hidden md:flex space-x-10">
                <ul class="pt-1 flex items-center justify-center h-full">
                  <li><p class="subpixel-antialiased inline-flex items-center justify-center p-2 py-3           mr-20  transition duration-200  h-full xl:flex items-center text-md font-medium  tracking-normal">
                      <Link href="/">
                      Devisa
                      </Link>
                  </p></li>
                    <Link href="/"><li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">Home</li></Link>
                <Popover class="relative">
                  {({ open }) => (
                    <>
                      <Popover.Button
                        class=
                          'group   ml-10 -mr-2 inline-flex       '
                      >
                      <li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500   transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">
                        Services
                        <ChevronDownIcon
                          class={classNames(
                            open ? 'text-gray-600' : 'text-gray-400',
                            'ml-2 h-5 w-5 group-hover:text-gray-500'
                          )}
                          aria-hidden="true"
                        />
                      </li>
                      </Popover.Button>

                      <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-200"
                        enterFrom="opacity-0 translate-y-1"
                        enterTo="opacity-100 translate-y-0"
                        leave="transition ease-in duration-150"
                        leaveFrom="opacity-100 translate-y-0"
                        leaveTo="opacity-0 translate-y-1"
                      >
                        <Popover.Panel
                          static
                          class="absolute z-10 w-screen max-w-md px-2 mt-3 -ml-4 transform sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2"
                        >
                          <div class="overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
                            <div class="relative px-5 py-6 bg-white grid gap-6 sm:gap-8 sm:p-8">
                              {solutions.map((item) => (
                                <a
                                  key={item.name}
                                  href={item.href}
                                  class="flex items-start p-3 -m-3 rounded-lg hover:bg-gray-100"
                                >
                                  <item.icon class="flex-shrink-0 w-6 h-6 text-green-600" aria-hidden="true" />
                                  <div class="ml-4">
                                    <p class="text-base font-medium text-gray-900">{item.name}</p>
                                    <p class="mt-1 text-sm text-gray-500">{item.description}</p>
                                  </div>
                                </a>
                              ))}
                            </div>
                            <div class="px-5 py-5 bg-gray-50 space-y-6 sm:flex sm:space-y-0 sm:space-x-10 sm:px-8">
                              {callsToAction.map((item) => (
                                <div key={item.name} class="flow-root">
                                  <a
                                    href={item.href}
                                    class="flex items-center p-3 -m-3 text-base font-medium text-gray-900 rounded-md hover:bg-gray-100"
                                  >
                                    <item.icon class="flex-shrink-0 w-6 h-6 text-gray-400" aria-hidden="true" />
                                    <span class="ml-3">{item.name}</span>
                                  </a>
                                </div>
                              ))}
                            </div>
                          </div>
                        </Popover.Panel>
                      </Transition>
                    </>
                  )}
                </Popover>
                            <li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500 ml-5  transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">
                            <Link href="/about">About</Link></li>
                            <li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500 ml-6  transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">
                            <Link href="/contact">Contact</Link></li>
                            <li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500 ml-6  transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">
                            <Link href="/blog">Blog</Link></li>
                            <li class="inline-flex items-center justify-center p-2 py-3  bg-white rounded-md  hover:bg-gray-100  focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500 ml-6  transition duration-200 cursor-pointer h-full xl:flex items-center text-sm  tracking-normal hidden hover:shadow-sm">
                            <Link href="/shop">Shop</Link></li>
                </ul>
              </Popover.Group>
                    </div>
                </div>
                <div aria-haspopup="true" class="cursor-pointer h-full xl:flex items-center justify-end hidden relative">
                    <ul class="p-2 w-40 border-r bg-gray-900 absolute rounded z-40 right-0 shadow mt-64 hidden">
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-green-700 focus:text-green-700 focus:outline-none">
                            <div class="flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" />
                                    <circle cx="12" cy="7" r="4" />
                                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                </svg>
                                <span class="ml-2">My Profile</span>
                            </div>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 focus:text-green-700 focus:outline-none flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <circle cx="12" cy="12" r="9" />
                                <line x1="12" y1="17" x2="12" y2="17.01" />
                                <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                            </svg>
                            <span class="ml-2">Help Center</span>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                <circle cx="12" cy="12" r="3" />
                            </svg>
                            <span class="ml-2">Account Settings</span>
                        </li>
                    </ul>
                     <div class="text-gray-700 flex items-center mt-4 px-4 py-4 mx-2 md:mt-0 p-2 shadow-sm border border-gray-200 rounded-lg">
                         <button class="hidden  text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="w-4 h-4 bi bi-envelope" viewBox="0 0 16 16">
  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
</svg>
                         </button>
                     </div>
                    <Dropdown button={newBtn} items={newItems} heading={newHeading}/>
                    <Dropdown button={dropdown} items={items} heading={heading}/>
        <div class="hover:bg-gray-50    active:bg-gray-100 text-gray-700 flex items-center  mx-2 px-4 md:mt-0 py-4 mr-2 shadow active:outline-none outline-none rounded-lg focus:outline-none">
                         <button class="text-sm hidden text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications"
                             onClick={() => signOut()}>
                             Log out
                         </button>
                     </div>
                </div>
                <div class="visible xl:hidden flex items-center">
                  <ul class="p-2 border-r bg-gray-50 absolute rounded top-0 left-0 right-0 shadow-sm border border-gray-200 mt-16 md:mt-16 hidden">
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mt-2 py-3 hover:text-green-700 focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Dashboard</span>
                                    </div>
                                </li>
                                  <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 focus:text-green-700 focus:outline-none flex justify-center">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Products</span>
                                    </div>
                                    <ul class="ml-2 mt-3 hidden">
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Landing Pages
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Templates
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Components
                                        </li>
                                    </ul>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Performance</span>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mb-2 py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Deliverables</span>
                                </li>
                                <li>
                                    <hr class="border-b border-gray-300 w-full"/>
                                </li>
                      <li>

                      </li>

                            </ul>

                    <div id="menu" class="text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <line x1="4" y1="6" x2="20" y2="6" />
                            <line x1="4" y1="12" x2="20" y2="12" />
                            <line x1="4" y1="18" x2="20" y2="18" />
                        </svg>
                    </div>
                </div>
            </div>
          <Transition
            show={open}
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Popover.Panel
              focus
              static
              class="absolute inset-x-0 top-0 p-2 transition transform origin-top-right md:hidden"
            >
              <div class="bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-y-2 divide-gray-50">
                <div class="px-5 pt-5 pb-6">
                  <div class="flex items-center justify-between">
                    <div>
                      <img
                        class="w-auto h-8"
                        src="https://tailwindui.com/img/logos/workflow-mark-green-600.svg"
                        alt="Workflow"
                      />
                    </div>
                    <div class="-mr-2">
                      <Popover.Button class="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-green-500">
                        <span class="sr-only">Close menu</span>
                        <XIcon class="w-6 h-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                  <div class="mt-6">
                    <nav class="grid gap-y-8">
                      {solutions.map((item) => (
                        <a
                          key={item.name}
                          href={item.href}
                          class="flex items-center p-3 -m-3 rounded-md hover:bg-gray-50"
                        >
                          <item.icon class="flex-shrink-0 w-6 h-6 text-green-600" aria-hidden="true" />
                          <span class="ml-3 text-base font-medium text-gray-900">{item.name}</span>
                        </a>
                      ))}
                    </nav>
                  </div>
                </div>
                <div class="px-5 py-6 space-y-6">
                  <div class="grid grid-cols-2 gap-y-4 gap-x-8">
                    <a href="#" class="text-base font-medium text-gray-900 hover:text-gray-700">
                      Pricing
                    </a>

                    <a href="#" class="text-base font-medium text-gray-900 hover:text-gray-700">
                      Docs
                    </a>
                    {resources.map((item) => (
                      <a
                        key={item.name}
                        href={item.href}
                        class="text-base font-medium text-gray-900 hover:text-gray-700"
                      >
                        {item.name}
                      </a>
                    ))}
                  </div>
                  <div>
                    <a
                      href="#"
                      class="flex items-center justify-center w-full px-4 py-2 text-base font-medium text-white bg-green-600 border border-transparent rounded-md shadow-sm hover:bg-green-700"
                    >
                      Sign up
                    </a>
                    <p class="mt-6 text-base font-medium text-center text-gray-500">
                      Existing customer?{' '}
                      <a href="#" class="text-green-600 hover:text-green-500">
                        Sign in
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </nav>
)
            }
  </Popover>
    )


    }
}
