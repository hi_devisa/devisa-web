export default function PrettyCard(props) {
    if ( props.square ) {
  return (
<body class="flex items-center justify-center w-screen h-screen text-gray-800 p-10 bg-gray-200">
	<div class="grid lg:grid-cols-3 md:grid-cols-2 gap-6 w-full max-w-6xl">
		<div class="flex items-center p-4 bg-white rounded">
			<div class="flex flex-shrink-0 items-center justify-center bg-green-200 h-16 w-16 rounded">
				<svg class="w-6 h-6 fill-current text-green-700"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
				  <path fill-rule="evenodd" d="M3.293 9.707a1 1 0 010-1.414l6-6a1 1 0 011.414 0l6 6a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L4.707 9.707a1 1 0 01-1.414 0z" clip-rule="evenodd" />
				</svg>
			</div>
			<div class="flex-grow flex flex-col ml-4">
				<span class="text-xl font-bold">$8,430</span>
				<div class="flex items-center justify-between">
					<span class="text-gray-500">Revenue last 30 days</span>
					<span class="text-green-500 text-sm font-semibold ml-2">+12.6%</span>
				</div>
			</div>
		</div>
		<div class="flex items-center p-4 bg-white rounded">
			<div class="flex flex-shrink-0 items-center justify-center bg-red-200 h-16 w-16 rounded">
				<svg class="w-6 h-6 fill-current text-red-700" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
				  <path fill-rule="evenodd" d="M16.707 10.293a1 1 0 010 1.414l-6 6a1 1 0 01-1.414 0l-6-6a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l4.293-4.293a1 1 0 011.414 0z" clip-rule="evenodd" />
				</svg>
			</div>
			<div class="flex-grow flex flex-col ml-4">
				<span class="text-xl font-bold">211</span>
				<div class="flex items-center justify-between">
					<span class="text-gray-500">Sales last 30 days</span>
					<span class="text-red-500 text-sm font-semibold ml-2">-8.1%</span>
				</div>
			</div>
		</div>
		<div class="flex items-center p-4 bg-white rounded">
			<div class="flex flex-shrink-0 items-center justify-center bg-green-200 h-16 w-16 rounded">
				<svg class="w-6 h-6 fill-current text-green-700"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
				  <path fill-rule="evenodd" d="M3.293 9.707a1 1 0 010-1.414l6-6a1 1 0 011.414 0l6 6a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L4.707 9.707a1 1 0 01-1.414 0z" clip-rule="evenodd" />
				</svg>
			</div>
			<div class="flex-grow flex flex-col ml-4">
				<span class="text-xl font-bold">140</span>
				<div class="flex items-center justify-between">
					<span class="text-gray-500">Customers last 30 days</span>
					<span class="text-green-500 text-sm font-semibold ml-2">+28.4%</span>
				</div>
			</div>
		</div>

	</div>

</body>
  )

} else {
    return (
<div class="flex items-center justify-center w-screen h-screen text-black bg-gray-100">
	<div class="grid grid-cols-1 md:grid-cols-3 gap-8">
		<div class="w-48 bg-white shadow-2xl p-6 rounded-2xl">
			<div class="flex items-center">
				<span class="flex items-center justify-center w-6 h-6 rounded-full bg-pink-100">
					<svg class="w-4 h-4 stroke-current text-pink-600"  xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
					</svg>
				</span>
				<span class="ml-2 text-sm font-medium text-gray-500">Followers</span>
			</div>
			<span class="block text-4xl font-semibold mt-4">1,320</span>
			<div class="flex text-xs mt-3 font-medium">
				<span class="text-green-500">+8%</span>
				<span class="ml-1 text-gray-500">last 14 days</span>
			</div>
		</div>
		<div class="w-48 bg-white shadow-2xl p-6 rounded-2xl">
			<div class="flex items-center">
				<span class="flex items-center justify-center w-6 h-6 rounded-full bg-red-100">
					<svg class="w-4 h-4 stroke-current text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
					</svg>
				</span>
				<span class="ml-2 text-sm font-medium text-gray-500">Likes</span>
			</div>
			<span class="block text-4xl font-semibold mt-4">3,814</span>
			<div class="flex text-xs mt-3 font-medium">
				<span class="text-green-500">+12%</span>
				<span class="ml-1 text-gray-500">last 14 days</span>
			</div>
		</div>
		<div class="w-48 bg-white shadow-2xl p-6 rounded-2xl">
			<div class="flex items-center">
				<span class="flex items-center justify-center w-6 h-6 rounded-full bg-blue-100">
					<svg class="w-4 h-4 stroke-current text-blue-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z" />
					</svg>
				</span>
				<span class="ml-2 text-sm font-medium text-gray-500">Comments</span>
			</div>
			<span class="block text-4xl font-semibold mt-4">264</span>
			<div class="flex text-xs mt-3 font-medium">
				<span class="text-red-500">-2%</span>
				<span class="ml-1 text-gray-500">last 14 days</span>
			</div>
		</div>
	</div>
</div>
    )
}
}
