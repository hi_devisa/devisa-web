import { useSession } from 'next-auth/client'
import Link from 'next/link'
export default function FeatureCard(props) {
  const [ session, loading ] = useSession()
  return (
        <div class="justify-center content-center self-center bg-white dark:bg-gray-800 lg:mx-8 lg:flex w-full lg:shadow-lg lg:rounded-lg">
            <div class="lg:w-1/2">
                <div class="object-contain h-64 bg-cover rounded-lg lg:h-full" style={{backgroundImage:`url(${ props.img })`}}></div>
            </div>

            <div class="max-w-xl px-8 ml-6 py-12 lg:max-w-5xl lg:w-1/2">
                <h2 class="text-2xl font-bold text-gray-800 dark:text-white md:text-3xl">Launch your idea <span class="text-green-600 dark:text-indigo-400">into orbit</span></h2>
                <p class="mt-4 text-gray-600 dark:text-gray-400">
                    { props.children }
                </p>

                <div class="mt-8 grid grid-cols-2">
                <div class="">
                    <a href={ props.hrefMain } class="shadow-sm px-5 py-2 font-semibold hover:shadow-md text-white transition-colors duration-200 transform bg-green-500 rounded-md hover:bg-green-400">{ props.btnMain }</a>
                </div>
                <div class="">
                    <span class="ml-8 hover:shadow-md shadow-sm px-5 py-2 font-semibold text-white transition-colors duration-200 transform bg-gray-400 rounded-md hover:bg-gray-300"><Link href={ props.hrefSecondary }>{ props.btnSecondary }</Link></span>
                </div>
                </div>
            </div>
        </div>
)
}

export function getStaticProps(props) {

}
