import { useState } from 'react'
import { Transition } from '@headlessui/react'

export default function Toast(props) {
  const [ isOpen, open ] = useState(false)
  return (
    <>
      <button onClick={() => open(!isOpen)} class="px-4 text-xl py-2  text-purple-500 rounded">Toggle</button>

      <Transition show={isOpen}>
        {(ref) => (
<div ref={ref}class="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4">
  <div class="flex-shrink-0">
    <img class="h-12 w-12" src="/lightart.jpg" alt="ChitChat Logo"/>
  </div>
  <div>
    <div class="text-xl font-medium text-black">ChitChat</div>
    <p class="text-gray-500">You have a new message!</p>
  </div>
</div>
        )}
      </Transition>
    </>
  )
  }

