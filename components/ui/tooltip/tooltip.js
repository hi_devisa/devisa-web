import { useState, createRef } from 'react'
import { createPopper } from "@popperjs/core";

export default function Tooltip(props) {
  const [tooltipShow, setTooltipShow] = useState(false);
  const btnRef = createRef();
  const tooltipRef = createRef();
  const openLeftTooltip = () => {
    createPopper(btnRef.current, tooltipRef.current, {
      placement:  props.placement ,
    });
    setTooltipShow(true);
  };
  const closeLeftTooltip = () => {
    setTooltipShow(false);
  };
  return (
    <>
      <div className="z-50 flex flex-wrap">
        <div className="w-full text-center">
          <div
            onMouseEnter={openLeftTooltip}
            onMouseLeave={closeLeftTooltip}
            ref={btnRef}
          >
            { props.children }
          </div>
          <div
            className={
              (tooltipShow ? "" : "hidden ") +
              "bg-" +
              props.color +
                "-400 opacity-" +
              props.opacity +
              " border-0 mt-3 block z-50 font-normal leading-normal text-sm max-w-xs text-left no-underline break-words rounded-lg"
            }
            ref={tooltipRef}
          >
            <div class="z-50">
              { props.title?
              <div
                className={
                  "bg-" +
                  props.color +
                  "-600 text-white opacity-75 font-semibold p-3 mb-0 border-b border-solid border-blueGray-100 rounded-t-lg"
                }
              >
                { props.title }
              </div>
              : <></> }
              <div className="z-50 p-3 text-gray-500">
                { props.tooltip }
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

      {/* <Tooltip */}
      {/*   color="green" */}
      {/*   placement="bottom" */}
      {/*   title = "This is the tooltip title" */}
      {/*   tooltip="This is the tooltip text" */}
      {/*   opacity="90" > */}
      {/* <h1 class="text-4xl"> HELLO TESTING TOOLTIP</h1> */}
    {/* </Tooltip> */}
