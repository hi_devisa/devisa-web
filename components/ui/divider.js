export default function Divider(props) {
    return (
            <div class="hidden sm:block sticky top-0" aria-hidden="true">
              <div class="py-5">
                  { props.heading?
                    <p class="tex-xl"> { props.heading }</p>
                    : <></>
                  }
                <div class="border-t border-gray-200"></div>
              </div>
            </div>
    )
}
