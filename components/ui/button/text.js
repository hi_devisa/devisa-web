import {
  ChevronUpIcon,
  StatusOnlineIcon, StatusOfflineIcon,
  DocumentAddIcon, DocumentIcon, DocumentTextIcon, DocumentRemoveIcon, DocumentDuplicateIcon,
  CogIcon,
  EyeIcon,
  LinkIcon,
  HomeIcon,
  CubeIcon,
  TagIcon,
  UsersIcon,
  BeakerIcon,
  StarIcon,
  InboxIcon,
  FolderIcon,
  PencilIcon,
  ClockIcon,
  MinusIcon,
  PlusIcon,
  KeyIcon,
  MapIcon,
  BellIcon,
  SaveIcon,
  ChatIcon,
  ReplyIcon,
  FolderRemoveIcon,
  ShareIcon,
  TableIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  TrashIcon,
  SearchIcon,
  CodeIcon,
  ArchiveIcon,
  LibraryIcon,
  InboxInIcon,
  FolderOpenIcon,
  FolderDownloadIcon,
  InformationCircleIcon,
  DuplicateIcon,
  DotsCircleHorizontalIcon,
  BookOpenIcon,
  BookmarkIcon,
  CashIcon,
  CheckIcon,
  CalendarIcon,
  ChartBarIcon,
  CollectionIcon,
  CalculatorIcon,
  ColorSwatchIcon, CameraIcon, FilterIcon, FolderAddIcon, UserGroupIcon, HeartIcon, HashtagIcon,
  LockClosedIcon, LocationMarkerIcon, MenuIcon, MicrophoneIcon, PaperClipIcon, PhotographIcon, RefreshIcon, UserAddIcon, UserRemoveIcon, UploadIcon, UserIcon,
  SortAscendingIcon, SortDescendingIcon, SelectorIcon, TrendingUpIcon, TrendingDownIcon, TerminalIcon, TemplateIcon, TicketIcon,
  ChevronDownIcon, DotsVerticalIcon, AtSymbolIcon,  AnnotationIcon, AdjustmentsIcon, CakeIcon, CreditCardIcon,
  PlayIcon, PauseIcon, RewindIcon, SaveAsIcon, StopIcon,
  LockOpenIcon, ExternalLinkIcon, ViewGridIcon, ViewListIcon, ViewGridAddIcon, ViewBoardsIcon
} from '@heroicons/react/solid'
import { Popover, Transition, Disclosure } from '@headlessui/react'

export function TextBtn(props) {

  return (
    <button class="flex text-black font-medium float-right justify-end text-sm px-3 py-2 item-center mb-4 rounded-lg hover:bg-gray-100 transition duration-200 text-gray-700 focus:shadow-inner">
      <props.icon className="w-4 h-4 mr-1 item-center"/>
      <Link href="#">{ props.children }</Link>
    </button>

  )
}
