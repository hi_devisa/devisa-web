import Link from 'next/link'
const button = (props) => {

    if ( !props.size || props.size == "sm" ) {
        return (
         <div class="flex items-center mt-4 md:mt-0">
             <Link href={ props.href }><button class="p-2 shadow-md hidden mr-8 bg-gradient-to-bl from-green-500 to-pacific-500 border-b-1 border-cerulean-800 rounded-lg text-white text-sm md:block hover:bg-green-400 transition duration-200 dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                 { props.label }
             </button></Link>
         </div>
        )
    } else if ( props.size == "md" ) {
        return (
         <div class="flex items-center mt-4 md:mt-0">
             <Link href={ props.href }><button class="border-0 py-3 px-3 focus:outline-none hover:bg-green-400 transition duration-200 hidden mr-8 bg-green-600 border-b-1 border-green-800 rounded-md text-white text-sm md:block shadow hover:bg-green-400 transition duration-200 dark:text-gray-200 hover:text-green-100 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 hover:bg-gradient-to-bl hover:from-green-300 hover:to-green-500 focus:outline-none" aria-label="show notifications">
                 { props.label }
             </button></Link>
         </div>
        )
    } else if ( props.size == "lg" ) {
        return (
         <div class="flex items-center mt-4 md:mt-0">
             <Link href={ props.href }><button class="border-0 py-2 px-6 focus:outline-none hover:bg-green-400 transition duration-200 hidden mr-8 bg-green-500 border-b-1 border-green-800 rounded text-white text-lg md:block shadow-sm hover:bg-green-400 transition duration-200 dark:text-gray-200 hover:text-green-100 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                 { props.label }
             </button></Link>
         </div>
        )
    }
}

export default button;
