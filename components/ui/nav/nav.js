import Image from 'next/image'
import Gradient from '../../components/ui/button/gradient'
import Dropdown from '../../components/dropdown'
import Downshift from 'downshift'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { getSession, useSession, signIn, signOut } from 'next-auth/client'



export default function Nav(props) {
  const [ session, loading ] = useSession()
  const router = useRouter()
    const dropdown = () => {
        if (session) {
            return (
            <div class="flex items-center justify-between mx-auto">
            <div class="rounded">
                <img class="rounded h-10 w-10 object-cover" src={session.user.image} alt="logo" />
            </div>
        <div class="text-gray-600 ml-2 min-h-full">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" />
                <polyline points="6 9 12 15 18 9" />
            </svg>
        </div>
                </div>
            )}
    }
    const email = session ? session.user.email : "undefined"
    const heading = (
      <div className="px-4 py-3 hover:bg-green-100">
        <p className="text-sm leading-5">Signed in as</p>
        <p className="text-sm font-medium font-bold text-gray-900 truncate leading-5">
            { email }
        </p>
      </div>
        )
  const items = [
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  if (!props.session) {
    return (
        <nav class="w-full mx-auto shadow relative z-30">
            <div class="container px-6 h-16 flex items-center justify-between mx-auto">
                <div class="flex items-center">
                    <img src="/logo/satgrn.png"
                        width = "72"
                        height = "72"/>
                    <div class="rounded-full relative p-0 flex justify-end text-black text-lg">
                        <p class="subpixel-antialiased hover:text-limegreen-400 transition duration-200"><Link href="/">Devisa&nbsp;&nbsp;</Link></p>
                        <ul class="ml-20 pt-1 flex items-center justify-center h-full">
                            <li class="transition duration-200 cursor-pointer h-full xl:flex items-center text-sm hover:text-limegreen-400 tracking-normal hidden"><Link href="/">Home</Link></li>
                            <li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden"><Link href="/services">Services</Link></li>
                            <li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden"><Link href="/about">About</Link></li>
                            <li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden"><Link href="/contact">Contact</Link></li>
                            <li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden"><Link href="/blog">Blog</Link></li>
                            <li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden"><Link href="/shop">Shop</Link></li>
                        </ul>
                    </div>
                </div>
                <div aria-haspopup="true" class="cursor-pointer h-full xl:flex items-center justify-end hidden relative">
                    <ul class="p-2 w-40 border-r bg-gray-900 absolute rounded z-40 right-0 shadow mt-64 hidden">
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 focus:text-green-700 focus:outline-none flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <circle cx="12" cy="12" r="9" />
                                <line x1="12" y1="17" x2="12" y2="17.01" />
                                <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                            </svg>
                            <span class="ml-2">Help Center</span>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                <circle cx="12" cy="12" r="3" />
                            </svg>
                            <span class="ml-2">Account Settings</span>
                        </li>
                    </ul>
                     <div class="flex items-center mt-4 md:mt-0">
                         <Link href="/login"><button class="hidden mr-8 hover:text-green-900 hover:bg-green-100 p-4 transition duration-200 text-sm rounded-xl md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             Login
                         </button>
                         </Link>
                     </div>
                     <Gradient label="Signup" href="/signup" size="md"/>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                        </svg>
                         </button>
                     </div>
                </div>
                <div class="visible xl:hidden flex items-center">
                  <ul class="p-2 border-r bg-gray-50 absolute rounded top-0 left-0 right-0 shadow mt-16 md:mt-16 hidden">
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mt-2 py-3 hover:text-green-700 focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Dashboard</span>
                                    </div>
                                </li>
                                  <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 focus:text-green-700 focus:outline-none flex justify-center">

                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Products</span>
                                    </div>
                                    <ul class="ml-2 mt-3 hidden">
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Landing Pages
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Templates
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Components
                                        </li>
                                    </ul>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Performance</span>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mb-2 py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Deliverables</span>
                                </li>
                                <li>
                                    <hr class="border-b border-gray-300 w-full"/>
                                </li>
                      <li>

                      </li>

                            </ul>

                    <div id="menu" class="text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <line x1="4" y1="6" x2="20" y2="6" />
                            <line x1="4" y1="12" x2="20" y2="12" />
                            <line x1="4" y1="18" x2="20" y2="18" />
                        </svg>
                    </div>
                </div>
            </div>
        </nav>
)
} else {
    var img = props.img ? props.img : "/ICONA.png"
    return (
        <nav class="w-full mx-auto shadow relative z-30">
            <div class="container px-6 h-16 flex items-center justify-between mx-auto">
                <div class="flex items-center">
                    <img src="/logo/satgrn.png"
                        width = "72"
                        height = "72"/>
                    <div class="rounded-full relative p-0 flex justify-end text-black text-lg">
                        <Link href="/"><p class=" hover:text-green-400 transition duration-200">Devisa&nbsp;&nbsp;</p></Link>
                <ul class="ml-20 pt-1 flex items-center justify-center h-full">
                    <Link href="/"><li class="transition duration-200 cursor-pointer h-full xl:flex items-center text-sm hover:text-green-400 tracking-normal hidden">Home</li></Link>
                    <Link href="/services"><li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden">Services</li></Link>
                    <Link href="/about"><li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden">About</li></Link>
                    <Link href="/contact"><li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden">Contact</li></Link>
                    <Link href="/blog"><li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden">Blog</li></Link>
                    <Link href="/shop"><li class="transition duration-200 hover:text-green-400 cursor-pointer h-full xl:flex items-center text-sm text-black ml-0 xl:ml-10 tracking-normal hidden">Shop</li></Link>
                </ul>
                    </div>
                </div>
                <div aria-haspopup="true" class="cursor-pointer h-full xl:flex items-center justify-end hidden relative">
                    <ul class="p-2 w-40 border-r bg-gray-900 absolute rounded z-40 right-0 shadow mt-64 hidden">
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-green-700 focus:text-green-700 focus:outline-none">
                            <div class="flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" />
                                    <circle cx="12" cy="7" r="4" />
                                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                </svg>
                                <span class="ml-2">My Profile</span>
                            </div>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 focus:text-green-700 focus:outline-none flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <circle cx="12" cy="12" r="9" />
                                <line x1="12" y1="17" x2="12" y2="17.01" />
                                <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                            </svg>
                            <span class="ml-2">Help Center</span>
                        </li>
                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" />
                                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                <circle cx="12" cy="12" r="3" />
                            </svg>
                            <span class="ml-2">Account Settings</span>
                        </li>
                    </ul>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear" viewBox="0 0 16 16">
  <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
  <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
</svg>
                         </button>
                     </div>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-grid" viewBox="0 0 16 16">
  <path d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"/>
</svg>
                         </button>
                     </div>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                        </svg>
                         </button>
                     </div>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="hidden mr-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
</svg>
                         </button>
                     </div>
                    <Dropdown button={dropdown} items={items} heading={heading}/>
                     <div class="flex items-center mt-4 md:mt-0">
                         <button class="text-sm hidden ml-8 text-gray-900 md:block dark:text-gray-200 hover:text-gray-700 dark:hover:text-gray-400 focus:text-gray-700 dark:focus:text-gray-400 focus:outline-none" aria-label="show notifications"
                             onClick={() => signOut()}>
                             Log out
                         </button>
                     </div>
                </div>
                <div class="visible xl:hidden flex items-center">
                  <ul class="p-2 border-r bg-gray-50 absolute rounded top-0 left-0 right-0 shadow mt-16 md:mt-16 hidden">
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mt-2 py-3 hover:text-green-700 focus:text-green-700 focus:outline-none">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Dashboard</span>
                                    </div>
                                </li>
                                  <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 focus:text-green-700 focus:outline-none flex justify-center">
                                    <div class="flex items-center">
                                        <span class="leading-6 ml-2 font-bold">Products</span>
                                    </div>
                                    <ul class="ml-2 mt-3 hidden">
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Landing Pages
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Templates
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-3 hover:bg-green-700 hover:text-white px-3 font-normal">
                                            Components
                                        </li>
                                    </ul>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Performance</span>
                                </li>
                                <li class="flex xl:hidden cursor-pointer text-gray-600 text-base leading-3 tracking-normal mb-2 py-3 hover:text-green-700 flex items-center focus:text-green-700 focus:outline-none">
                                    <span class="leading-6 ml-2 font-bold">Deliverables</span>
                                </li>
                                <li>
                                    <hr class="border-b border-gray-300 w-full"/>
                                </li>
                      <li>

                      </li>

                            </ul>

                    <div id="menu" class="text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <line x1="4" y1="6" x2="20" y2="6" />
                            <line x1="4" y1="12" x2="20" y2="12" />
                            <line x1="4" y1="18" x2="20" y2="18" />
                        </svg>
                    </div>
                </div>
            </div>
        </nav>
    )
}
}
