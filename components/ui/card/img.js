export default function ImgCard(props) {
    return (
<div>
    <img class="rounded" src={ props.img } alt= { props.alt }/>
    <div class="mt-2">
      <div>
        <div class="text-xs text-gray-600 uppercase font-bold">{ props.eyebrow }</div>
        <div class="font-bold text-gray-700 leading-snug">
          <a href={ props.href } class="hover:underline">{ props.title }</a>
        </div>
        <div class="mt-2 text-sm text-gray-600">{ props.text }</div>
      </div>
    </div>
  </div>
      )
      }
