export default function SignupAlt() {
  return (
<div class="max-w-sm rounded overflow-hidden shadow-lg">
  <div class="overflow-hidden">
    <img class="w-full" src="https://scontent.fkbl4-1.fna.fbcdn.net/v/t1.0-9/107375467_2588456871416868_8947669359021370132_n.jpg?_nc_cat=105&_nc_sid=e3f864&_nc_ohc=fn4GT67yi8wAX9q5NPy&_nc_ht=scontent.fkbl4-1.fna&oh=5e829468c548939a81346daa6815f402&oe=5F46DCE0" alt="Sunset in the mountains"/>
  </div>
  <div class="float-right transform -translate-y-8 right-0 bg-blue-600 shadow-xl ml-auto mr-4 rounded-full h-16 w-16 flex items-center justify-center"><svg width="25px" fill="white" version="1.1" id="Capa_1"   x="0px" y="0px" viewBox="0 0 486.926 486.926" style="enable-background:new 0 0 486.926 486.926;" xml:space="preserve">
      <g>
        <path d="M462.8,181.564c-12.3-10.5-27.7-16.2-43.3-16.2h-15.8h-56.9h-32.4v-75.9c0-31.9-9.3-54.9-27.7-68.4
    c-29.1-21.4-69.2-9.2-70.9-8.6c-5,1.6-8.4,6.2-8.4,11.4v84.9c0,27.7-13.2,51.2-39.3,69.9c-19.5,14-39.4,20.1-41.5,20.8l-2.9,0.7
    c-4.3-7.3-12.2-12.2-21.3-12.2H24.7c-13.6,0-24.7,11.1-24.7,24.7v228.4c0,13.6,11.1,24.7,24.7,24.7h77.9c7.6,0,14.5-3.5,19-8.9
    c12.5,13.3,30.2,21.6,49.4,21.6h65.9h6.8h135.1c45.9,0,75.2-24,80.4-66l26.9-166.9C489.8,221.564,480.9,196.964,462.8,181.564z
     M103.2,441.064c0,0.4-0.3,0.7-0.7,0.7H24.7c-0.4,0-0.7-0.3-0.7-0.7v-228.4c0-0.4,0.3-0.7,0.7-0.7h77.9c0.4,0,0.7,0.3,0.7,0.7
    v228.4H103.2z M462.2,241.764l-26.8,167.2c0,0.1,0,0.3-0.1,0.5c-3.7,29.9-22.7,45.1-56.6,45.1H243.6h-6.8h-65.9
    c-21.3,0-39.8-15.9-43.1-36.9c-0.1-0.7-0.3-1.4-0.5-2.1v-191.6l5.2-1.2c0.2,0,0.3-0.1,0.5-0.1c1-0.3,24.7-7,48.6-24
    c32.7-23.2,49.9-54.3,49.9-89.9v-75.3c10.4-1.7,28.2-2.6,41.1,7c11.8,8.7,17.8,25.2,17.8,49v87.8c0,6.6,5.4,12,12,12h44.4h56.9
    h15.8c9.9,0,19.8,3.7,27.7,10.5C459,209.864,464.8,225.964,462.2,241.764z" />
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
      <g>
      </g>
    </svg></div>
  <div class="p-4">
    <small class="text-gray-600">Thursday, July 16, 2020</small>
    <h2 class="font-semibold text-2xl my-2">The standard chunk of lorem ipsum</h2>
    <p class="text-gray-600">Sod posuere consectetur est at labrotis. Aenean eu leo quam.</p>
  </div>
  <div class="flex justify-between items-center bg-gray-100 px-4 py-3">
    <span>Read more</span>
    <span><svg version="1.1" id="Ebene_1"   x="0px" y="0px" width="23px" fill="#555" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
        <g>
          <path d="M28.373,13.546c-0.803-0.758-2.068-0.722-2.827,0.081c-0.758,0.803-0.722,2.069,0.081,2.827L42.087,32l-16.46,15.546
    c-0.803,0.758-0.839,2.024-0.081,2.827C25.939,50.79,26.469,51,27,51c0.493,0,0.986-0.181,1.373-0.546l18-17
    C46.773,33.076,47,32.55,47,32s-0.227-1.076-0.627-1.454L28.373,13.546z" />
          <path d="M32,0C23.453,0,15.417,3.329,9.374,9.373C3.329,15.417,0,23.453,0,32s3.33,16.583,9.374,22.626
    C15.417,60.671,23.453,64,32,64s16.583-3.329,22.626-9.373C60.671,48.583,64,40.547,64,32s-3.33-16.583-9.374-22.626
    C48.583,3.329,40.547,0,32,0z M51.797,51.798C46.509,57.087,39.479,60,32,60s-14.509-2.913-19.798-8.202C6.913,46.51,4,39.479,4,32
    s2.913-14.51,8.203-19.798C17.491,6.913,24.521,4,32,4s14.509,2.913,19.798,8.202C57.087,17.49,60,24.521,60,32
    S57.087,46.51,51.797,51.798z" />
        </g>
      </svg></span>
  </div>
</div>
)
}
