export default function Alt4() {
    return (
<body class="antialiased bg-gray-200 font-sans">
    <div class="max-w-6xl mx-auto">
        <div class="flex items-center justify-center min-h-screen">
            <div class="max-w-lg w-full sm:w-1/2 lg:w-2/3 py-6 px-3">
                <div class="bg-white shadow-xl rounded-lg overflow-hidden">

                    <div class="bg-cover bg-center h-56 p-4" style={{backgroundImage: `url(https://images.unsplash.com/photo-1494081307357-78e3fb01df9b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80)`}}>
                        <div class="flex justify-end">
                            <svg class="h-6 w-6 text-white fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z"></path>
                            </svg>
                        </div>
                    </div>
                    <div class="p-4">
                        <p class="tracking-wide text-sm font-bold text-gray-700">Rankin County School District</p>
                        <p class="text-3xl text-gray-900">Northwest Rankin High School</p>
                      <div class="flex">
                        <div class="flex-1 inline-flex items-center py-2">
                          <div>
                      <p class="text-gray-700">5805 MS-25</p>
                      <p class="text-gray-700">Flowood, MS 39232</p>
                          </div>
                        </div>

                    </div>
                    <div class="flex p-4 border-t border-gray-300 text-gray-700">
                        <div class="flex-1 inline-flex items-center">
                                                    <svg class="h-6 w-6 text-gray-700 fill-current mr-3" viewBox="0 0 24 24">
    <path d="M12,5A3.5,3.5 0 0,0 8.5,8.5A3.5,3.5 0 0,0 12,12A3.5,3.5 0 0,0 15.5,8.5A3.5,3.5 0 0,0 12,5M12,7A1.5,1.5 0 0,1 13.5,8.5A1.5,1.5 0 0,1 12,10A1.5,1.5 0 0,1 10.5,8.5A1.5,1.5 0 0,1 12,7M5.5,8A2.5,2.5 0 0,0 3,10.5C3,11.44 3.53,12.25 4.29,12.68C4.65,12.88 5.06,13 5.5,13C5.94,13 6.35,12.88 6.71,12.68C7.08,12.47 7.39,12.17 7.62,11.81C6.89,10.86 6.5,9.7 6.5,8.5C6.5,8.41 6.5,8.31 6.5,8.22C6.2,8.08 5.86,8 5.5,8M18.5,8C18.14,8 17.8,8.08 17.5,8.22C17.5,8.31 17.5,8.41 17.5,8.5C17.5,9.7 17.11,10.86 16.38,11.81C16.5,12 16.63,12.15 16.78,12.3C16.94,12.45 17.1,12.58 17.29,12.68C17.65,12.88 18.06,13 18.5,13C18.94,13 19.35,12.88 19.71,12.68C20.47,12.25 21,11.44 21,10.5A2.5,2.5 0 0,0 18.5,8M12,14C9.66,14 5,15.17 5,17.5V19H19V17.5C19,15.17 14.34,14 12,14M4.71,14.55C2.78,14.78 0,15.76 0,17.5V19H3V17.07C3,16.06 3.69,15.22 4.71,14.55M19.29,14.55C20.31,15.22 21,16.06 21,17.07V19H24V17.5C24,15.76 21.22,14.78 19.29,14.55M12,16C13.53,16 15.24,16.5 16.23,17H7.77C8.76,16.5 10.47,16 12,16Z" />
</svg>

                            <p><span class="text-gray-900 font-bold">1,234</span> Students</p>
                        </div>
                        <div class="flex-1 inline-flex items-center">
                                              <svg class="h-6 w-6 text-gray-700 fill-current mr-3" viewBox="0 0 24 24">
    <path d="M22,5V7H17L13.53,12H16V14H14.46L18.17,22H15.97L15.04,20H6.38L5.35,22H3.1L7.23,14H7C6.55,14 6.17,13.7 6.04,13.3L2.87,3.84L3.82,3.5C4.34,3.34 4.91,3.63 5.08,4.15L7.72,12H12.1L15.57,7H12V5H22M9.5,14L7.42,18H14.11L12.26,14H9.5Z" />
</svg>

                            <p>Grades <span class="text-gray-900 font-bold">9</span> through <span class="text-gray-900 font-bold">12</span></p>
                        </div>
                    </div>
                    <div class="px-4 pt-3 pb-4 border-t border-gray-300 bg-gray-100">
                        <div class="text-xs font-bold text-gray-600 tracking-wide">Superintendent:</div>
                        <div class="flex items-center pt-2">
                            <div class="bg-cover bg-center w-10 h-10 rounded-full mr-3" style={{backgroundImage: `url(https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80)`}}>
                            </div>
                            <div>
                                <p class="font-bold text-gray-900">Tiffany Heffner</p>
                                <p class="text-sm text-gray-700">(555) 555-4321</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>
)}
