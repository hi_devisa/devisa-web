export default function PrettyCard(props) {
  return (
<div class="flex justify-center items-center h-screen">

  <div style="width:290px;height:312px" class="relative rounded shadow bg-grey-lightest">
      <i class="cursor-pointer absolute pin-r mr-4 mt-3 text-white fas fa-ellipsis-v"></i>
  <div class="h-24 bg-blue rounded-tl rounded-tr">
    <div class="flex pt-12 items-center">
      <div>
        <div  class="h-24 w-24 ml-8 border border-6 border-white rounded-full">
          <img
               src="/lightart.jpg"
               class="h-24 w-24 rounded-full border-2"
               alt=""/>
      </div>
      </div>

      <div class="ml-2">
        <h2 class="-mt-4 text-grey-lighter">JOHN DOE</h2>
        <p class="m-0 mt-4 block text-sm text-grey-dark">Lives in Ikrain</p>
      </div>
    </div>
  </div>
  <div class="mt-16">
    <div class="flex flex-col justify-between px-6 py-3">
      <div class="flex">
        <div class="flex w-1/2 justify-center items-center bg-blue-dark text-white rounded-full p-3">
          <i class="fas fa-chart-line"></i>
        </div>

        <div class="text-right ml-2">
          <h4 class="text-grey-darker">3</h4>
          <p class="text-sm text-grey-dark">Hrs/Day</p>
        </div>
        <div class="flex w-1/2 justify-center items-center bg-blue-dark text-white rounded-full p-3 ml-4">
          <i class="fas fa-calendar"></i>
        </div>

        <div class="text-right ml-2">
          <h4 class="text-grey-darker">14</h4>
          <p class="text-sm text-grey-dark">Days/Month</p>
        </div>
      </div>
      <div class="flex mt-6 w-1/2">
             <div class="flex justify-center items-center bg-blue-dark text-white rounded-full p-3">
          <i class="fas fa-wallet"></i>
        </div>

        <div class="text-right ml-2">
          <h4 class="text-grey-darker">$230</h4>
          <p class="text-sm text-grey-dark">/Month</p>
        </div>

        <div class="flex w-1/2  justify-end items-center bg-blue-dark text-white rounded-full p-3 ml-6">
          <i class="fas fa-trophy"></i>
        </div>

        <div class="text-right ml-2">
          <h4 class="text-grey-darker">10</h4>
          <p class="text-sm text-grey-dark">Awards</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
)
}
