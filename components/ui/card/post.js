export default function PrettyCard(props) {
  return (

<div class="max-w-sm rounded overflow-hidden shadow-lg">
  <div class="py-4 px-8">
    <img src="https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" class="rounded-full h-12 w-12 mb-4"/>
    <h4 class="text-lg mb-3">How to be effective at working remotely?</h4>
    <p class="mb-2 text-sm text-gray-600">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>

    <img src="https://images.pexels.com/photos/461077/pexels-photo-461077.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" class="w-100"/>

    <hr class="mt-4"/>
    <span class="text-xs">ARTICLE</span>
    &nbsp;<span class="text-xs text-gray-500">PROCESS</span>
  </div>
</div>
      )
      }
