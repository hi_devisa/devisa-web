export default function CardWithButtons(props) {
  return (
                <div class="mt-5 md:mt-0 md:col-span-2 shadow">
                  <form action="#" method="POST">
                    <div class="sm:rounded-md sm:overflow-hidden">
                      <div class="px-4 py-5 bg-white space-y-6 sm:p-6">

                        { props.children }

                      </div>
                      <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-gray-800 bg-white shadow hover:bg-gray-300 mr-4 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                          { props.secondaryBtn }
                        </button>
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-700 shadow focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                          { props.mainBtn }
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
  )
}
