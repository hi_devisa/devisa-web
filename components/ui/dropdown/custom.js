// import { Menu, Transition } from '@headlessui/react'

// export default function Dropdown() {
//   return (
//     <Menu>
//       {({ open }) => (
//         <>
//           <Menu.Button>More</Menu.Button>

//           <Transition
//             show={open}
//             enter="transition duration-100 ease-out"
//             enterFrom="transform scale-95 opacity-0"
//             enterTo="transform scale-100 opacity-100"
//             leave="transition duration-75 ease-out"
//             leaveFrom="transform scale-100 opacity-100"
//             leaveTo="transform scale-95 opacity-0"
//           >
//             <Menu.Items static>
//               <Menu.Item>Dashboard</Menu.Item>
//               <Menu.Item>Account</Menu.Item>
//               <Menu.Item>Profile</Menu.Item>
//               <Menu.Item>Preferences</Menu.Item>
//               <Menu.Item>Log out</Menu.Item>
//             </Menu.Items>
//           </Transition>
//         </>
//       )}
//     </Menu>
//   )
// }

import * as React from "react";
import { Menu, Transition } from "@headlessui/react";
import Link from 'next/link'

export default function Dropdown(props) {
  var menuItems = props.items.map(function(item) {
    return (
      <Menu.Item disabled={item.disabled}>
        {({ active }) => (
          <Link
            href={ item.link }
            className={`${
              active
                ? "bg-gray-100 text-gray-900"
                : "text-gray-700"
            } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}
          >
            { item.key }
          </Link>
        )}
      </Menu.Item>
    );
  });
  return (
    <div className="flex items-center justify-center p-12">
      <div className="relative inline-block text-left">
        <Menu>
          {({ open }) => (
            <>
              <span className="rounded-md shadow-sm">
                <Menu.Button className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 transition duration-150 ease-in-out rounded-md hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800">
                  <span>{ props.text }</span>
                  <svg
                    className="w-5 h-5 ml-2 -mr-1"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </Menu.Button>
              </span>

              <Transition
                show={open}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items
                  static
                  className="absolute right-0 w-56 mt-2 bg-white border border-gray-200 shadow-lg outline-none origin-top-right divide-y divide-gray-100 rounded-md"
                >
                  <div className="px-4 py-3">
                    <p className="text-sm leading-5">Signed in as</p>
                    <p className="text-sm font-medium text-gray-900 truncate leading-5">
                      tom@example.com
                    </p>
                  </div>

                  <div className="py-1">
                    { menuItems }
                  </div>
                </Menu.Items>
              </Transition>
            </>
          )}
        </Menu>
      </div>
    </div>
  );
}

