import { Menu } fromm '@headlessui/react'

export default function UserDropdown() {
  return (
<div class="bg-gray-100 text-gray-900 antialiased font-sans">
  <div class="px-8 py-16 flex justify-center bg-gray-100" style={{minHeight: `600px`}}>
    <div class="w-full max-w-xs mx-auto">
      <div class="space-y-1">
        <label id="assigned-to-label" class="block text-sm leading-5 font-medium text-gray-700">Assigned to</label>
        <div class="relative">
          <span class="inline-block w-full rounded-md shadow-sm">
            <button x-ref="button" type="button" aria-haspopup="listbox" aria-expanded="open" aria-labelledby="assigned-to-label" class="cursor-default relative w-full rounded-md border border-gray-300 bg-white pl-3 pr-10 py-2 text-left focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
              <div class="flex items-center space-x-3">
                <span x-text="['Wade Cooper', 'Arlene Mccoy', 'Devon Webb', 'Tom Cook', 'Tanya Fox','Hellen Schmidt','Caroline Schultz','Mason Heaney','Claudie Smitham','Emil Schaefer'][value - 1]" class="block truncate">Tom Cook</span>
              </div>
              <span class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                  <path d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path>
                </svg>
              </span>
            </button>
          </span>
          <div x-show="open"     x-description="Select popover, show/hide based on select state." x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="absolute mt-1 w-full rounded-md bg-white shadow-lg" style="display: none;">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

                    )

                    }
