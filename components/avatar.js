import { Menu } from '@headlessui/react'
import { getSession, useSession, signIn, signOut } from 'next-auth/client'

export default function Avatar() {
  return (
    <Menu as="div">
      <Menu.Button>
        <div class = "ml-3 relative">
          <button
            class="flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-green-300 focus:ring-green-400">
            <img
              class="rounded-full h-8 w-8"
              src={this.props.image}
            />
          </button>
        </div>
      </Menu.Button>
      <Menu.Items as="ul">
        <Menu.Item as="li">
          <div clasName="px-4 py-3">
            <p className="text-sm leading-5">Hi, {this.props.name}
              <span className="text-sm font-medium text-gray-900 truncate leading-5">{this.props.email}</span></p>
          </div>
        </Menu.Item>
        <Menu.Item as="li">
          {({ active }) => (
            <a className={`${active && 'bg-green-500'}`} href="/account">
              Account
            </a>
          )}
        </Menu.Item>
        <Menu.Item as="li">
          {({ active }) => (
            <a className={`${active && 'bg-green-500'}`} href="/profile">
              Profile
            </a>
          )}
        </Menu.Item>
        <Menu.Item as="li">
          {({ active }) => (
            <a className={`${active && 'bg-green-500'}`} href="/preferences">
              Preferences
            </a>
          )}
        </Menu.Item>
        <Menu.Item as="li">
          {({ active }) => (
            <a className={`${active && 'bg-green-500'}`} href="#" onClick={() => signOut()}>
              Sign out
            </a>
          )}
        </Menu.Item>
      </Menu.Items>
    </Menu>
  )
}
