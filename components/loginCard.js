import Link from 'next/link'
export default function LoginCard() {
    return (
        <form class=" mr-15 bg-white rounded shadow-xl p-8 m-10 mt-20 mb-0 ml-0 mr-40 border-2 border-gray-100" action="">
          <p class="font-light text-sm text-gray-500">Or, if you've already got a pass...</p><br/>
          <label class="font-regular text-gray-600 text-s" for="usernameField">Username or Email</label>
          <input class="flex items-center h-12 px-4 w-64 bg-gray-100 mt-2 rounded focus:outline-none focus:ring-2 shadow-magical" type="text" placeholder="Username or Email"/>
          <br/>
          <label class="font-regular text-gray-600 text-s" htmlFor="passwordField">Password</label>
          <input class="flex items-center h-12 px-4 w-64 bg-gray-100 mt-2 rounded focus:outline-none focus:ring-2 shadow-magical"type="password" placeholder="Password"/>
          <button class="flex items-center justify-center h-12 px-6 w-64 bg-green-600 mt-8 rounded font-semibold text-sm text-white hover:bg-green-700 shadow-magical">Login</button>
          <div class="flex mt-6 justify-center text-xs">
            <Link href="/signup"><button class="text-white hover:text-gray-100" href="/signup">Sign Up</button></Link>
          </div>
        </form>
    );
}
