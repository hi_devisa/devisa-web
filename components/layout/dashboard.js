import Link from 'next/link'
import UserDropdown from '../user/statusDropdown'
import Header from '../header/dashboard'
import { Transition } from '@headlessui/react'
import { useState } from 'react'
import Footer from '../footer/details'
import Head from 'next/head'
import Card from '../card'
import Dropdown from '../dropdown'
import Switch from '../ui/switch'
import Sbp from '../ui/sidebar/sidebarPretty'
import SbpIcons from '../ui/sidebar/sidebarPrettyIcons'
import Layout from '../layout'
import Nav from '../ui/nav'
import { useSession } from 'next-auth/client'
import { Router, useRouter } from 'next/router'

export default function DashLayout(props) {
  const bypassLogin = props.bypassLogin ? true : false;
  const [session, loading] = useSession()
  const [openTab, setOpenTab] = useState(0)
  const items = [
      { text: "Dashboard", value: "/dashboard", disabled: false },
      { text: "Account", value: "/account", disabled: false },
      { text: "Profile", value: "/user/chris", disabled: false },
      { text: "Records", value: "/user/chris/records", disabled: false },
      { text: "Recipes", value: "/user/chris/recipes", disabled: false },
      { text: "Items", value: "/user/chris/items", disabled: false },
      { text: "Feed", value: "/community/feed", disabled: false },
      { text: "Community", value: "/community", disabled: false },
      { text: "Settings", value: "/settings", disabled: false },
      { text: "Sign out", value: "/logout", disabled: false }
  ]
  const header = (
              <Header
                title = { props.title }
                breadcrumb = { props.breadcrumb }
                mainBtn = { props.mainBtn }
                secondaryBtn = { props.secondaryBtn }/>)
  const  router  = useRouter()
  const [ sidebarOpen, openSidebar ] = useState(true)
  const dropdown = () => {
      if (session) {
          return (
          <div class="flex items-center justify-between mx-auto">
          <div class="rounded">
              <img class="rounded h-10 w-10 object-cover" src={session.user.image} alt="logo" />
          </div>
      <div class="text-gray-600 ml-2 min-h-full">
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
              <path stroke="none" d="M0 0h24v24H0z" />
              <polyline points="6 9 12 15 18 9" />
          </svg>
      </div>
              </div>
          )}
  }
  const tabs = (
    <div class="inline-block  align-text-bottom align-baseline mt-10 -mb-3">
          <nav class="flex  sm:flex-row align-bottom bottom-0 text-sm">
            { props.tab1 ? (
              <button class={ "rounded-t-lg text-gray-600 py-3 px-6 block hover:text-green-500 focus:outline-none font-medium "
                  + ( openTab === 0 ? "text-gray-800  border-green-500 border-b-4" : "")}
                onClick={e => {
                  e.preventDefault();
                  setOpenTab(0);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist">
                { props.tab1 }

              </button>
               ) : null }
            { props.tab2 ? (
              <button class={ "rounded-t-lg text-gray-600 py-3 px-6 block hover:text-green-500 focus:outline-none font-medium "
                  + ( openTab === 1 ? "text-gray-800  border-green-500 border-b-4" : "")}
                onClick={e => {
                  e.preventDefault();
                  setOpenTab(1);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist">
                { props.tab2 }
              </button>
               ) : null }
            { props.tab3 ? (
              <button class={ "rounded-t-lg text-gray-600 py-3 px-6 block hover:text-green-500 focus:outline-none font-medium "
                  + ( openTab === 2 ? "text-gray-800  border-green-500 border-b-4" : "")}
                onClick={e => {
                  e.preventDefault();
                  setOpenTab(2);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist">
                { props.tab3 }
              </button>
               ) : null }
            { props.tab4 ? (
              <button class={ "rounded-t-lg text-gray-600 py-3 px-6 block hover:text-green-500 focus:outline-none font-medium "
                  + ( openTab === 3 ? "text-gray-800  border-green-500 border-b-4" : "")}
                onClick={e => {
                  e.preventDefault();
                  setOpenTab(3);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist">
                { props.tab4 }
              </button>
            ) : null }
          </nav>
      </div>
  )
  if ( loading ) {
    return (
      <div class="w-full">
        <div class="flex flex-row absolute w-full">
          <SbpIcons/>
          <div class="flex flex-col  relative w-full">
            <div class="border-l bg-gradient-to-br from-gray-50  to-gray-100 shadow-inner relative w-full pb-24 z-10">
            <div class="mt-6 px-6 py-0 mx-6 bg-white shadow rounded-lg mt-0  top-0 z-10 sticky pb-4 pt-2">
              <div class="w-full container grid grid-cols-3 mx-auto px-6 pl-6">
                <div class="">
                  <div class="">
                    <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-4">
                      <i class="fas fa-angle-left"></i>
                      <span>{ props.breadcrumb }</span>
                     </a>
                    <div class="flex items-center">
                      <img class="h-12 w-12 rounded-full shadow-md ring-1 border-2 border-gray-300 ring-transparent" src="/ICONA.png"/>
                      <h1 class="px-4 text-2xl text-gray-900 font-medium">Loading: { props.title }</h1>
                    </div>
                  </div>
                </div>

                <div class="mt-6 -mb-2">
                </div>

                  <div class="mt-8 items-end ml-32">
                    <button class="h-9 border border-gray-200 px-3 py-2 text-sm font-medium rounded text-gray-900 shadow">{ props.secondaryBtn }</button>
                    <button class="h-9 ml-3 px-3 py-2 text-sm font-medium rounded bg-gradient-to-b from-green-500 via-green-600 to-green-600 text-white shadow" style={{textShadow: "0px -1px 0px rgba(0,0,0,0.4)"}}>{ props.mainBtn }</button>
                  </div>



              </div>
            </div>

            <div class="px-18 pt-4">
              { props.child1 ?
              <div className="tab-content tab-space">
                <div className={openTab === 0 ? "block" : "hidden"} id="link1">
                </div>
                <div className={openTab === 1 ? "block" : "hidden"} id="link2">
                </div>
                <div className={openTab === 2 ? "block" : "hidden"} id="link3">
                </div>
                <div className={openTab === 3 ? "block" : "hidden"} id="link4">
                </div>
              </div>
              : <></>
              }
            </div>
          </div>
            <button class="  border-green-900 bottom-0 left-18 my-5 mb-5  ml-4 z-20 pb-2 bg-gradient-to-b from-white to-gray-300 ring-0 ring-gray-400 opacity-70 focus:outline-none rounded-full fixed p-2 shadow-xl focus:outline-none rounded-full hover:from-gray-50 hover:to-gray-200 text-gray-800 hover:text-gray-900 transition-all duration-200 hover:shadow-2xl" onClick={() => openSidebar(!sidebarOpen)}>
              <svg xmlns="http://www.w3.org/2000/svg"   fill="currentColor" class="w-8 h-8 text-gray-400 bi bi-arrow-bar-right"       viewBox="0 0 16 16">
  <path d="M0 3a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3zm5-1v12h9a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H5zM4 2H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2V2z"/>
</svg>
              </button>
            <div class=" border-l">
        <Footer/>
            </div>
        </div>
      </div>
      </div>
)
  }
  if ( props.bypassLogin ) {
    return (
      <div class="w-full">
        <div class="flex flex-row absolute w-full">
          { sidebarOpen ?
          <Sbp/>
          :
          <SbpIcons/>}
          <div class="flex flex-col  relative w-full">
            <Nav />


            <div class="border-l bg-gradient-to-br from-gray-50  to-gray-100 shadow-inner relative w-full pb-24 z-10">

            {/* <div class="px-6 py-2 mx-6 bg-white shadow rounded-b-md mt-0 pt-0 top-0 z-10 sticky pb-6 pt-4"> */}
            <div class="mt-6 px-6 py-0 mx-6 bg-white shadow rounded-lg mt-0  top-0 z-10 sticky pb-4 pt-2">
              <div class="w-full container grid grid-cols-3 mx-auto px-6 pl-6">

                <div class="grid grid-cols-2">
                  <div class="">
                    <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-4">
                      <i class="fas fa-angle-left"></i>
                      <span>{ props.breadcrumb }</span>
                     </a>
                    <div class="flex items-center min-w-full">
                      <img class="h-12 w-12 rounded-full shadow-md ring-1 border-2 border-gray-300 ring-transparent" src="/ICONA.png"/>
                      <h1 class="px-4 text-xl text-gray-900 font-medium w-full">{ props.title }</h1>
                    </div>
                  </div>

                </div>

                <div class="mt-6 -mb-2">
                { props.child1? tabs : null }
                </div>

                  <div class="place-self-end self-center  float-right flex py-1 px-2  bg-gray-100  border-b-2 border-gray-300 rounded-xl items-center pt-2 pb-0">
                    { session ?
                    <Dropdown
                      button={<UserDropdown user={ session.user? session.user : { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>}
                      items={items}
                      heading={
                      <div class="px-4 py-3 ">
                        <p class="text-sm leading-5">Create a new...</p>
                      </div>
                      }
                    /> :
                    <Dropdown
                      button={<UserDropdown user={{ username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>}
                      items={items}
                      heading={
                      <div class="px-4 py-3 ">
                        <p class="text-sm leading-5">Create a new...</p>
                      </div>
                      }
                    />
                    }
                  </div>




              </div>
            </div>

            <div class="px-18 pt-4">
              { props.child1 ?
              <div className="tab-content tab-space">
                <div className={openTab === 0 ? "block" : "hidden"} id="link1">
                  {props.child1}
                </div>
                <div className={openTab === 1 ? "block" : "hidden"} id="link2">
                  {props.child2}
                </div>
                <div className={openTab === 2 ? "block" : "hidden"} id="link3">
                  {props.child3}
                </div>
                <div className={openTab === 3 ? "block" : "hidden"} id="link4">
                  {props.child4}
                </div>
              </div>
              :
               props.children
              }
            </div>
          </div>
            <button class="  border-green-900 bottom-0 left-18 my-5 mb-5  ml-4 z-20 pb-2 bg-gradient-to-b from-white to-gray-300 ring-0 ring-gray-400 opacity-70 focus:outline-none rounded-full fixed p-2 shadow-xl focus:outline-none rounded-full hover:from-gray-50 hover:to-gray-200 text-gray-800 hover:text-gray-900 transition-all duration-200 hover:shadow-2xl" onClick={() => openSidebar(!sidebarOpen)}>
              <svg xmlns="http://www.w3.org/2000/svg"   fill="currentColor" class="w-8 h-8 text-gray-400 bi bi-arrow-bar-right"       viewBox="0 0 16 16">
  <path d="M0 3a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3zm5-1v12h9a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H5zM4 2H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2V2z"/>
</svg>
              </button>
            <div class=" border-l">
        <Footer/>
            </div>
        </div>
      </div>
      </div>

    )
  }
  if ( session ) {
    return (
      <div class="w-full">
        <div class="flex flex-row absolute w-full">
          { sidebarOpen ?
          <Sbp/>
          :
          <SbpIcons/>}
          <div class="flex flex-col  relative w-full">
            <Nav session = { session }/>

            <div class="border-l bg-gradient-to-br from-gray-50  to-gray-100 shadow-inner relative w-full pb-24 z-10">

            {/* <div class="px-6 py-2 mx-6 bg-white shadow rounded-b-md mt-0 pt-0 top-0 z-10 sticky pb-6 pt-4"> */}
            <div class="mt-6 px-6 py-0 mx-6 bg-white shadow rounded-lg mt-0  top-0 z-10 sticky pb-4 pt-2">
              <div class="w-full container grid grid-cols-3 mx-auto px-6 pl-6">

                <div class="flex flex-row min-w-full">
                  <div class=" min-w-full">
                    <a href="#" class="inline-flex items-center text-sm text-gray-500 leading-none pb-4">
                      <i class="fas fa-angle-left"></i>
                      <span>{ props.breadcrumb }</span>
                     </a>
                    <div class="flex items-center min-w-full group">
                      <img class="h-12 w-12 rounded-full shadow ring-1 ring-black" src= { session.user.image } alt=""/>
                      <h1 class="px-4 text-2xl text-gray-900 font-medium">{ props.title }</h1>
                    </div>
                  </div>
                </div>

                <div class="mt-6 -mb-4">
                { props.child1? tabs : null }
                </div>

                <div class="place-self-end self-center  float-right flex py-1 px-2  bg-gray-100  border-b-2 border-gray-300 rounded-xl items-center pt-2 pb-0">
                  { session ?
                  <Dropdown
                    button={<UserDropdown user={ session.user? session.user : { username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>}
                    items={items}
                    heading={
                    <div class="px-4 py-3 ">
                      <p class="text-sm leading-5">Create a new...</p>
                    </div>
                    }
                  /> :
                  <Dropdown
                    button={<UserDropdown user={{ username: "chris", image: "/lightart.jpg", email: "c@dvsa.io"} }/>}
                    items={items}
                    heading={
                    <div class="px-4 py-3 ">
                      <p class="text-sm leading-5">Create a new...</p>
                    </div>
                    }
                  />
                  }
                </div>




              </div>
            </div>

            <div class="px-18 pt-4">
              { props.child1 ?
              <div className="tab-content tab-space">
                <div className={openTab === 0 ? "block" : "hidden"} id="link1">
                  {props.child1}
                </div>
                <div className={openTab === 1 ? "block" : "hidden"} id="link2">
                  {props.child2}
                </div>
                <div className={openTab === 2 ? "block" : "hidden"} id="link3">
                  {props.child3}
                </div>
                <div className={openTab === 3 ? "block" : "hidden"} id="link4">
                  {props.child4}
                </div>
              </div>
              :
               props.children
              }
            </div>
          </div>
            <button class="  border-green-900 bottom-0 left-18 my-5 mb-5  ml-4 z-20 pb-2 bg-gradient-to-b from-white to-gray-300 ring-0 ring-gray-400 opacity-70 focus:outline-none rounded-full fixed p-2 shadow-xl focus:outline-none rounded-full hover:from-gray-50 hover:to-gray-200 text-gray-800 hover:text-gray-900 transition-all duration-200 hover:shadow-2xl" onClick={() => openSidebar(!sidebarOpen)}>
              <svg xmlns="http://www.w3.org/2000/svg"   fill="currentColor" class="w-8 h-8 text-gray-400 bi bi-arrow-bar-right"       viewBox="0 0 16 16">
  <path d="M0 3a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3zm5-1v12h9a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H5zM4 2H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2V2z"/>
</svg>
              </button>
            <div class=" border-l">
        <Footer/>
            </div>
        </div>
      </div>
      </div>
  )
  } else {
    return (
      <Layout session={session} title="Settings">
        <h1 class="text-2xl"> You are not logged in!</h1>
      </Layout>

    )
  }
}
// const Profile = ({ user }) => {
//   // Show the user. No loading state is required
//   return ( <Layout>
//       <h1>Your Profile</h1>
//       <pre>{JSON.stringify(user, null, 2)}</pre>
//     </Layout>
//   )
// }

// export default Profile = ({ session }) => {
//   return <p>{session.user.name}</p>;
// };

// export async function getServerSideProps({ req, res }) {
//   // Get the user's session based on the request
//   const session = await getSession(req);

//   if (!session) {
//     // If no user, redirect to login
//     return {
//       props: {},
//       redirect: {
//         destination: '/login',
//         permanent: false
//       }
//     };
//   }

//   // If there is a user, return the current session
//   return { props: { session } };
// }ult Profile
// }
